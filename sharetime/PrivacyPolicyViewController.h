//
//  PrivacyPolicyViewController.h
//  sharetime
//
//  Created by Pardhu G on 08/05/16.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "BaseViewController.h"

@interface PrivacyPolicyViewController : BaseViewController

@property (nonatomic,strong) NSString* screenTitle;
@property (nonatomic,strong) NSString* htmlFile;

@end
