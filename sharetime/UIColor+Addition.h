//
//  UIColor+Addition.h
//  PrintKnot
//
//  Created by Sushree on 15/05/15.
//  Copyright (c) 2015 XpertsInfosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Addition)

+ (UIColor *)colorWithHexString:(NSString *)hexStr;
+ (NSString *)hexStringFromColor:(UIColor *)color;

+ (UIColor*)cellBrownColor;
+ (UIColor*)cellGreenColor;
+ (UIColor*)cellYellowColor;
+ (UIColor*)cellPinkColor;
+ (UIColor*)redDotColor;
+ (UIColor*)navigationBlueColor;

@end
