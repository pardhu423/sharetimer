//
//  ChangeUsernameViewController.m
//  sharetime
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "ChangeUsernameViewController.h"

@interface ChangeUsernameViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtOldUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtNewUsername;

@end

@implementation ChangeUsernameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_txtNewUsername addLeftPadding:12];
    [_txtOldUsername addLeftPadding:12];
    _txtOldUsername.text = self.appDelegate.user.username;
    [self.navigationController addTitle:@"Change Username"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - button click

- (IBAction)saveClicked:(id)sender
{
    NSString *username = _txtNewUsername.text;
    if (username.length == 0) {
        kAlert(nil, @"Please enter username");
        return;
    }
    [self sendDataToServerWithTask:TASK_UPDATE_USERNAME];
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@"Updating..."];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_UPDATE_USERNAME:
        {
            UserModel *user = self.appDelegate.user;
            self.appDelegate.user.username = _txtNewUsername.text;
            [postDict setObject:user.Id forKey:kUserId];
            [postDict setObject:self.appDelegate.user.username forKey:kUsername];

            [postDict setObject:user.email forKey:kEmail];
            [postDict setObject:user.phone forKey:kPhone];
            [self.requestManager callServiceWithRequestType:TASK_UPDATE_USERNAME method:METHOD_POST params:postDict urlEndPoint:@"users/updateUser"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_UPDATE_USERNAME:
            {
                self.appDelegate.user.username = _txtNewUsername.text;
                kAlert(nil, message);
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

@end
