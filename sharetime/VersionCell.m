//
//  VersionCell.m
//  sharetime
//
//  Created by iOS Developer on 10/12/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "VersionCell.h"

@interface VersionCell ()

@property (weak, nonatomic) IBOutlet UILabel *lblVersion;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@end

@implementation VersionCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
