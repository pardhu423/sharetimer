//
//  AppDelegate.m
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "AppDelegate.h"
#import "FirstViewController.h"
#import "HomeViewController.h"
#import "AcceptedFriendsViewController.h"
#import "EditViewController.h"
#import "UIImage+ImageManipulations.h"
#import "LoginViewController.h"

@interface AppDelegate ()<UINavigationControllerDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    FirstViewController *firstVc = [[FirstViewController alloc]initWithNibName:@"FirstViewController" bundle:nil];

    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:firstVc];
    nav.delegate = self;
    nav.navigationBarHidden = YES;
    
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
    
    //remote notifications
 /*   [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
    [application registerForRemoteNotifications];*/
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - navigation Controller delegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    BOOL hidden = ([viewController isKindOfClass:[FirstViewController class]] ||
                   [viewController isKindOfClass:[HomeViewController class]] ||
                   [viewController isKindOfClass:[AcceptedFriendsViewController class]] ||
                   [viewController isKindOfClass:[EditViewController class]]);
    [navigationController setNavigationBarHidden:hidden animated:YES];
}

#pragma mark - notifications

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
   /* NSString *tokenString = [deviceToken description];
    tokenString = [tokenString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    tokenString = [tokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"My token is: %@", tokenString);
    [[NSUserDefaults standardUserDefaults] setObject:tokenString forKey:kDeviceToken];
    [[NSUserDefaults standardUserDefaults] setObject:[[NSUUID UUID] UUIDString] forKey:kDeviceUuid];
    [[NSUserDefaults standardUserDefaults] synchronize];*/
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

#pragma mark - handle push

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [self handlePushWithInfo:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self handlePushWithInfo:userInfo];
}

- (void)handlePushWithInfo:(NSDictionary*)infoDict
{

}

@end
