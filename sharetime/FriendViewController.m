//
//  FriendViewController.m
//  sharetime
//
//  Created by iOS Developer on 03/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "FriendViewController.h"
#import "ScrollThemeView.h"
#import "ListViewController.h"

@interface FriendViewController ()<ScrollThemeViewDelegate,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {
    ScrollThemeView *_scroll;
    UIColor *oldColor;
}

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *lblViewed;
@property (weak, nonatomic) IBOutlet UILabel *lblScreenshot;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation FriendViewController

#pragma mark - vc life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self styleNavBar];
    [self addGesture];
    
    //IF letter is not viewed then we record the view on server
    if (!_letter.isViewed) {
        [self sendDataToServerWithTask:TASK_RECORD_VIEW];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTakeScreenshot) name:UIApplicationUserDidTakeScreenshotNotification object:nil];
    
    _btnDelete.hidden = !_isMyProfile;
    
    if (_rowId) {
        [self sendDataToServerWithTask:TASK_READ_MAILED_LETTER];
    }
    
    if (_letter.screenShots) {
        _lblScreenshot.hidden = NO;
    }else {
        _lblScreenshot.hidden = YES;
    }
    
//    _lblScreenshot.hidden = !_letter.isScreenshotCountVisible;
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"identifier"];
    
    
    [NSTimer scheduledTimerWithTimeInterval:0
                                     target:self
                                   selector:@selector(targetMethod:)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    oldColor = self.navigationController.navigationBar.barTintColor;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"#3a4347"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = oldColor;
}

-(void)targetMethod:(id)sender{
    NSLog(@"I am done");
    //[_scroll removeFromSuperview];
    
    
    LetterModel *letter = _letters[_selectedLetterIndex];
    UIImageView *imageUser = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    
    imageUser.image = letter.gender == 0 ? [UIImage imageNamed:@"your_profile"] : [UIImage imageNamed:@"your_profile_female"];
    UIBarButtonItem *leftUserImage = [[UIBarButtonItem alloc]initWithCustomView:imageUser];
    self.navigationItem.leftBarButtonItem = leftUserImage;
    
    NSString * friends = letter.views <= 1 ? @"Friend" : @"Friends";
    _lblViewed.text = [NSString stringWithFormat:@"%ld %@ viewed", (long)letter.views, friends];
    
    NSString *screenShots = letter.screenShots <=1 ? @"Friend" : @"Friends";
    _lblScreenshot.text = [NSString stringWithFormat:@"%ld %@ screenshot",(long)letter.screenShots,screenShots];

    if (_isFromLetter == YES) {
        UIImageView *imageUser = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        imageUser.image = letter.gender == 0 ? [UIImage imageNamed:@"your_profile"] : [UIImage imageNamed:@"your_profile_female"];
        UIBarButtonItem *leftUserImage = [[UIBarButtonItem alloc]initWithCustomView:imageUser];
        self.navigationItem.leftBarButtonItem = leftUserImage;
        
        [self.navigationController addRightBarButtonWithImageNamed:@"big_close"];
    } else {
        [self.navigationController addLeftBarButtonWithImageNamed:@"back_arrow"];
    }

    [self.collectionView setAllowsMultipleSelection:YES];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_selectedLetterIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
    
    
}

#pragma mark - collection view data source
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(screenSize.width, screenSize.height - 77);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _letters.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"identifier" forIndexPath:indexPath];
    for (UIView *subview in cell.contentView.subviews) {
        [subview removeFromSuperview];
    }
    
    LetterModel *letter = _letters[indexPath.row];
    NSString *nibName = letter.scrollTheme == SCROLL_THEME_CURVED ? @"ScrollThemeView" : @"FlatScrollView";

    ScrollThemeView *scroll = [[[NSBundle mainBundle]loadNibNamed:nibName owner:self options:nil] lastObject];
    scroll.delegate = self;
    scroll.viewVideoPlayArea.backgroundColor = [UIColor clearColor];
    scroll.txtDescription.editable = NO;
    scroll.txtDescription.selectable = NO;
    scroll.imageViewForVideoBlueBorder.hidden = YES;
    //if (letter.scrollTheme != SCROLL_THEME_CURVED) {
        scroll.imageViewForVideoWhiteBorder.hidden = NO;
    //}
    
    int margin = 10;
    CGSize contentSize = cell.contentView.frame.size;
    CGSize scrollSize = CGSizeMake(contentSize.width - 2 * margin, contentSize.height);
    [scroll resizeViewConstrainedTo:scrollSize animated:NO];
    scroll.center = CGPointMake(contentSize.width/2, contentSize.height/2);
    [scroll transalateResizingIntoConstraints];
    scroll.letterObject = letter;
    
    NSDate *todaysDate = [[NSDate alloc]init];
    todaysDate = [NSDate dateForString:letter.time format:@"yyyy-MM-dd HH:mm:ss"];
    scroll.lblTimePassed.text = [NSString stringWithFormat:@"%@",[todaysDate getShortTime]];
    [cell.contentView addSubview:scroll];
   
    //[self.navigationController addPlainTitle:letter.username color:[UIColor navigationBlueColor]];
    [self.navigationController addTitleColor:letter.username];

    //[self.navigationController addLeftBarButtonWithImageNamed:@"back_arrow"];
    
    
    NSString * friends = letter.views <= 1 ? @"Friend" : @"Friends";
    _lblViewed.text = [NSString stringWithFormat:@"%ld %@ viewed", (long)letter.views, friends];
    
    NSString *screenShots = letter.screenShots <=1 ? @"Friend" : @"Friends";
    _lblScreenshot.text = [NSString stringWithFormat:@"%ld %@ screenshot",(long)letter.screenShots,screenShots];
    
    if (_isFromLetter == YES) {
        UIImageView *imageUser = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        
        imageUser.image = letter.gender == 0 ? [UIImage imageNamed:@"your_profile"] : [UIImage imageNamed:@"your_profile_female"];
        UIBarButtonItem *leftUserImage = [[UIBarButtonItem alloc]initWithCustomView:imageUser];
        self.navigationItem.leftBarButtonItem = leftUserImage;
        
        [self.navigationController addRightBarButtonWithImageNamed:@"big_close"];
    } else {
        [self.navigationController addLeftBarButtonWithImageNamed:@"back_arrow"];
    }

    return cell;
}

- (void)didTakeScreenshot
{
    if (!_letter.isScreenshotTaken) {
        [self sendDataToServerWithTask:TASK_RECORD_SCREENSHOT];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - nav

- (void)styleNavBar {
    [self.navigationController addTitleColor:_letter.username];
}

- (IBAction)leftBarButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBarButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - gesture

- (void)addGesture {
    [self.lblViewed addTapRecognizerWithTarget:self selector:@selector(viewedCountTapped:)];
    [self.lblScreenshot addTapRecognizerWithTarget:self selector:@selector(screenshotTapped:)];
}

- (IBAction)viewedCountTapped:(id)sender
{
    [self showListWithType:LIST_VIEWS];
}

- (IBAction)screenshotTapped:(id)sender
{
    [self showListWithType:LIST_SCREENSHOT];
}

- (void)showListWithType:(int)listType
{
    if (_isMyProfile) {
        ListViewController *listVc = [[ListViewController alloc]initWithNibName:@"ListViewController" bundle:nil];
        listVc.listType = listType;
        listVc.letterId = _letter.Id;
        [self.navigationController pushViewController:listVc animated:YES];
    }
}

#pragma mark - button click

- (IBAction)deleteClicked:(id)sender
{
    [self sendDataToServerWithTask:TASK_DELETE_POST];
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@""];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
    
    switch (task) {
        case TASK_RECORD_VIEW:
        {
            if (!_letter.views) {
                [postDict setObject:_letter.Id forKey:@"shareId"];
                [postDict setObject:@"view" forKey:@"action"];
                [self.requestManager callServiceWithRequestType:TASK_RECORD_VIEW method:METHOD_POST params:postDict urlEndPoint:@"users/saveuseraction"];
            }
            else {
                [self hideLoader];

            }
            
            break;
        }
            
        case TASK_RECORD_SCREENSHOT:
        {
            
            if (!_letter.screenShots) {
                [postDict setObject:_letter.Id forKey:@"shareId"];
                [postDict setObject:@"screenshot" forKey:@"action"];
                [self.requestManager callServiceWithRequestType:TASK_RECORD_SCREENSHOT method:METHOD_POST params:postDict urlEndPoint:@"users/saveuseraction"];
            }
            else {
                [self hideLoader];
            }
            break;
        }
            
        case TASK_DELETE_POST:
        {
            [postDict setObject:@"gallery" forKey:@"source"];
            [postDict setObject:_letter.Id forKey:@"shareId"];
            [self.requestManager callServiceWithRequestType:TASK_DELETE_POST method:METHOD_POST params:postDict urlEndPoint:@"deleteLetter"];
            break;
        }
            
        case TASK_READ_MAILED_LETTER:
        {
            [postDict setObject:_rowId forKey:kRowId];
            [self.requestManager callServiceWithRequestType:TASK_READ_MAILED_LETTER method:METHOD_POST params:postDict urlEndPoint:@"inboxstatus"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_RECORD_VIEW:
            {
                _letter.views = [[responseDict objectForKey:kViews] integerValue];
                _letter.isViewed = YES;
                break;
            }
                
            case TASK_RECORD_SCREENSHOT:
            {
                _letter.screenShots = [[responseDict objectForKey:kScreenShots] integerValue];
                _letter.isScreenshotTaken = YES;
                _lblScreenshot.text = [NSString stringWithFormat:@"%ld Friends screenshot", (long)_letter.screenShots];
                break;
            }
                
            case TASK_DELETE_POST:
            {
                [_letters removeObjectAtIndex:_selectedLetterIndex];
                [self.navigationController popViewControllerAnimated:YES];
                break;
            }
                
            case TASK_READ_MAILED_LETTER:
            {
                NSLog(@"mail read: %@", message);
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}


@end
