//
//  UserModel.m
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "Header.h"

#import "UserModel.h"


@implementation UserModel

- (id)init
{
    self = [super init];
    if (self) {
        self.email = @"";
        self.password = @"";
        self.dateOfBirth = @"";
        self.gender = MALE;
        self.username = @"";
        self.country = @"";
        self.phone = @"";
        self.rowId = @"";
        self.confirmStatus = STATUS_ACCEPTED;
        self.selected = NO;
        self.mailRead = NO;
    }
    return self;
}

- (id)initWithResponse:(NSDictionary *)response
{
    self = [super initWithResponse:response];
    if (self && [response isKindOfClass:[NSMutableDictionary class]]) {
        if (self.Id.length == 0) {
            self.Id = [self checkNil:[response objectForKey:kUserId]];
        }
        if (self.Id.length == 0) {
            self.Id = [self checkNil:[response objectForKey:@"friendId"]];
        }
        self.email = [self checkNil:[response objectForKey:kEmail]];
        self.username = [self checkNil:[response objectForKey:kUsername]];
        self.password = [self checkNil:[response objectForKey:kPassword]];
        self.phone = [self checkNil:[response objectForKey:kPhone]];
        self.country = [self checkNil:[response objectForKey:kCountry]];
        self.dateOfBirth = [self checkNil:[response objectForKey:kDob]];
        self.rowId = [self checkNil:[response objectForKey:kRowId]];
        self.gender = [[self checkNil:[response objectForKey:kGender]] intValue];
        self.confirmStatus = [[response objectForKey:kConfirmStatus] integerValue];
        self.mailRead = [[response objectForKey:kStatus] boolValue];
        self.selected = NO;
    }
    return self;
}

- (BOOL)isValidSignup
{
    if (_email.length == 0) {
        kAlert(nil, @"Please enter your email");
        return NO;
    }
    if (_dateOfBirth.length == 0) {
        kAlert(nil, @"Please enter your birthday");
        return NO;
    }
    if (![_email isValidForPattern:patternForEmail]) {
        kAlert(nil, @"Please enter a valid email");
        return NO;
    }
    if (_password.length < 6) {
        kAlert(nil, @"Password too short");
        return NO;
    }
    return YES;
}

- (BOOL)isValidRegion
{
    if (_country.length == 0) {
        kAlert(nil, @"Please select a country");
        return NO;
    }
    if (_phone.length == 0) {
        kAlert(nil, @"Please enter a phone");
        return NO;
    }
    if (![_phone isValidForPattern:patternForPhoneNumber]) {
        kAlert(nil, @"Please enter a valid phone number");
        return NO;
    }
    return YES;
}

@end
