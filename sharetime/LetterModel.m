//
//  LetterModel.m
//  sharetime
//
//  Created by iOS Developer on 01/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "LetterModel.h"
#import "UserModel.h"
#import "NSDate+Conversions.h"
#import "AFConnectionManager.h"

@implementation LetterModel

- (id)init
{
    self = [super init];
    if (self) {
        self.text = @"";
        self.imgData = [NSData data];
        self.isTimeVisible = NO;
        self.scrollTheme = SCROLL_THEME_FLAT;
        
        NSDate *now = [NSDate date];
        self.time = [now stringWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        self.videoUrl = [NSURL URLWithString:@""];
        self.mediaType = MEDIA_TYPE_IMAGE;
        self.views = 0;
        self.screenShots = 0;
        self.filterType = FILTER_NONE;
        self.userId = @"";
        self.username = @"";
        self.isVolume = YES;
        self.isScreenshotCountVisible = YES;
        self.gender = YES;
    }
    return self;
}

- (id)initWithResponse:(NSDictionary *)response
{
    self = [super initWithResponse:response];
    if (self && [response isKindOfClass:[NSMutableDictionary class]]) {
        self.text = [self checkNil:[response objectForKey:kDescription]];
        
        self.Id = [self checkNil:[response objectForKey:@"shareId"]];

        self.userId = [self checkNil:[response objectForKey:kUserId]];
        self.scrollTheme = [[response objectForKey:kTheme] integerValue];
        
        //time di bhasoodi
        NSString *time = [self checkNil:[response objectForKey:kCreatedAt]];
        NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
        NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
        [inputDateFormatter setTimeZone:inputTimeZone];
        [inputDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *createdDate = [inputDateFormatter dateFromString:time];
        self.time = [createdDate stringWithFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *videoLink = [self checkNil:[response objectForKey:kVideoUrl]];
        self.videoUrl = [NSURL URLWithString:videoLink];
        self.imgLink = [self checkNil:[response objectForKey:kThumbUrl]];
        self.mediaType = [[response objectForKey:kMediaType] integerValue];
        
        self.views = [[response objectForKey:kViews] integerValue];
        self.screenShots = [[response objectForKey:kScreenShots] integerValue];
        
        self.isViewed = [[self checkNil:[response objectForKey:kIsViewed]] boolValue];
        self.isScreenshotTaken = [[self checkNil:[response objectForKey:kIsScreenshotTaken]] boolValue];
        
        self.isTimeVisible = [[response objectForKey:kIsTimestampShown] boolValue];
        self.isScreenshotCountVisible = [[response objectForKey:kIsScreenshotCountVisible] boolValue];
        self.isVolume = [[response objectForKey:kIsVolume] boolValue];
        
        self.userId = [self checkNil:[response objectForKey:@"friendId"]];
        self.username = [self checkNil:[response objectForKey:@"friendUsername"]];
        self.gender = [[response objectForKey:kGender] boolValue];
    }
    return self;
}

@end
