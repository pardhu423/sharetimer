//
//  FriendTableViewCell.h
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

@interface FriendTableViewCell : UITableViewCell

- (void)bindUser:(UserModel*)user;

@end
