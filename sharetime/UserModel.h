//
//  UserModel.h
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#define genders @[@"male", @"female"]

enum {
    MALE,
    FEMALE
};

enum {
    STATUS_PENDING,
    STATUS_ACCEPTED, //1
    STATUS_REJECTED,
    STATUS_BLOCKED,   //3
    STATUS_UNBLOCKED
};

#define kFriendId @"friendId"
#define kUserId @"userId"
#define kEmail @"email"
#define kUsername @"username"
#define kPassword @"password"
#define kPhone @"phone"
#define kCountry @"country"
#define kDob @"dob"
#define kGender @"gender"
#define kRowId @"rowId"
#define kConfirmStatus @"confirmStatus"

#import "BaseModel.h"

@interface UserModel : BaseModel

@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *dateOfBirth;
@property (nonatomic) int gender;
@property (strong, nonatomic) NSString *rowId;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *phone;
@property (nonatomic) NSInteger confirmStatus;
@property (nonatomic) BOOL mailRead;
@property (nonatomic) BOOL selected;

- (BOOL)isValidSignup;
- (BOOL)isValidRegion;

@end
