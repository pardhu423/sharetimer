//
//  AddFriendsViewController.m
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "AddFriendsViewController.h"
#import "AddUsernameViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

@interface AddFriendsViewController ()<UITextFieldDelegate, ABPeoplePickerNavigationControllerDelegate>
{
    UIColor *oldColor;
}

@property (weak, nonatomic) IBOutlet UITextField *txtAddByUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtAddFromAddresBook;

@end

@implementation AddFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController addLeftBarButtonWithImageNamed:@"back_arrow"];
    [self.navigationController addTitle:@"Add Friends"];
    [self styleTextField:_txtAddByUsername];
    [self styleTextField:_txtAddFromAddresBook];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    oldColor = self.navigationController.navigationBar.barTintColor;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"3a4347"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = oldColor;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_txtAddByUsername addBottomBorderWithWidth:1 color:[UIColor lightGrayColor]];
    [_txtAddFromAddresBook addBottomBorderWithWidth:1 color:[UIColor lightGrayColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - text field

- (IBAction)cancelSearchClicked:(id)sender
{
    _txtAddByUsername.text = @"";
    [self.view endEditing:YES];
}

- (void)styleTextField:(UITextField*)txtField
{
    if (txtField == _txtAddByUsername) {
        [txtField addLeftImageNamed:@"search"];
    } else{
        [txtField addLeftImageNamed:@"addressBookmark"];
    }
    [txtField addRightButtonWithImageName:@"search_close" target:self selector:@selector(cancelSearchClicked:) viewMode:UITextFieldViewModeWhileEditing];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _txtAddByUsername) {
        AddUsernameViewController *addUsernameVc = [[AddUsernameViewController alloc]initWithNibName:@"AddUsernameViewController" bundle:nil];
        [self.navigationController pushViewController:addUsernameVc animated:YES];
    } else if (textField == _txtAddFromAddresBook) {
        [self getAddressBook];
    }
    return NO;
}

#pragma mark - contacts

- (void)getAddressBook
{
    //check address book permission status
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        //1
        [self requestAdressBookAccess];
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        [self showContacts];
    } else {
        [self requestAdressBookAccess];
    }
}

- (void)requestAdressBookAccess
{
    ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
        if (!granted){
            //4
            kAlert(nil, @"Permission denied. Cannot access address book");
            return;
        }
        [self showContacts];
    });
}

- (void)showContacts
{
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
    ABPeoplePickerNavigationController *peopleVc = [[ABPeoplePickerNavigationController alloc]init];
    peopleVc.peoplePickerDelegate = self;
    peopleVc.addressBook = addressBookRef;
    [self presentViewController:peopleVc animated:YES completion:nil];
}

#pragma mark - people picker delegate

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person
{
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
    
    //get the phone numbers for selected record
    NSMutableArray *phoneNumbers = [[NSMutableArray alloc]init];
    ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
    for(CFIndex i = 0; i < ABMultiValueGetCount(multiPhones); i++) {
        CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
        CFRelease(multiPhones);
        NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
        CFRelease(phoneNumberRef);
        [phoneNumbers addObject:phoneNumber];
    }
    NSLog(@"numbers: %@", phoneNumbers);
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
}

@end
