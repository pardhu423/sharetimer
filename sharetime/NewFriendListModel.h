//
//  NewFriendListModel.h
//  sharetime
//
//  Created by Gopala Krishna Kammela on 01/02/16.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//
#define kConfirmStatus @"confirm_status"
#define kIsNewFriend @"is_newfriend"
#define kNewFriend @"newfriend"
#define kUserId @"user_id"
#define kfriendId @"friend_id"

#import "BaseModel.h"

@interface NewFriendListModel : BaseModel

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *friendId;
@property (nonatomic, strong) NSString *confirm_status;
@property (nonatomic, strong) NSString *isNewFriend;
@property (nonatomic, strong) NSMutableArray *arrayNewFriends;
@end
