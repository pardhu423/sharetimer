//
//  PopularTopicsViewController.m
//  sharetime
//
//  Created by iOS Developer on 11/12/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#define ALERT_CONFIRM_DELETE 100

#import "PopularTopicsViewController.h"
#import "ChangeUsernameViewController.h"
#import "PrivacyPolicyViewController.h"

@interface PopularTopicsViewController ()<UITextFieldDelegate, STAlertViewDelegate>

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *numberedLabels;
@property (weak, nonatomic) IBOutlet UILabel *lblLock;
@property (weak, nonatomic) IBOutlet UILabel *lblF;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginDeletedProfileView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginBlockedProfileView;
@property (weak, nonatomic) IBOutlet UILabel *lblSettings;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightChangeUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblBinIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblDownloadApp;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UILabel *lblCameraFlip;
@property (weak, nonatomic) IBOutlet UILabel *lblCameraIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblWhiteCross;
@property (weak, nonatomic) IBOutlet UILabel *lblGreenCircle;
@property (weak, nonatomic) IBOutlet UILabel *lblLetterBin;
@property (weak, nonatomic) IBOutlet UILabel *lblFilters;
@property (weak, nonatomic) IBOutlet UIView *lastView;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@end

@implementation PopularTopicsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (_txtPassword)
        [self.navigationController addTitle:@""];//Deleting the title if it was a delete a/c screen
    else
        [self.navigationController addTitle:@"Support"];

    
    [self setWhiteNumbers];
    [self addImageToLabels];
    [self setConstraints];
    [self showScreenImages];
    if (_lblDownloadApp) {
        [self addLinks];
    }
    _scroller.frame = CGRectMake(0, 0, screenSize.width, screenSize.height);
    [self.view addSubview:_scroller];
    
    [_txtPassword addLeftPadding:10];
    _txtUsername.text = self.appDelegate.user.username;
    [_txtUsername addLeftPadding:10];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    UILabel *lastLabel = _numberedLabels[_numberedLabels.count - 1];
    int contentHeight = lastLabel.frame.origin.y + 120;
    if (_lastView) {
        CGRect lastFrame = _lastView.frame;
        contentHeight = lastFrame.origin.y + lastFrame.size.height + 80;
    }
    
    _scroller.contentSize = CGSizeMake(screenSize.width, contentHeight);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - style labels

- (void)setWhiteNumbers
{
    for (UILabel *lbl in _numberedLabels) {
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:lbl.text];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, 1)];
        lbl.attributedText = attributedString;
    }
}

- (void)addImageToLabels
{
    //lock label
    NSMutableAttributedString *lockString = [[NSMutableAttributedString alloc]initWithAttributedString:_lblLock.attributedText];
    NSTextAttachment *lockAttachment = [[NSTextAttachment alloc]init];
    lockAttachment.image = [UIImage imageNamed:@"lock_small"];
    NSAttributedString *lockAttachmentString = [NSAttributedString attributedStringWithAttachment:lockAttachment];
    [lockString appendAttributedString:lockAttachmentString];
    _lblLock.attributedText = lockString;
    
    //F label
    NSMutableAttributedString *fString = [[NSMutableAttributedString alloc]initWithAttributedString:_lblF.attributedText];
    NSTextAttachment *fAttachment = [[NSTextAttachment alloc]init];
    fAttachment.image = [UIImage imageNamed:@"green_f_small"];
//    NSMutableAttributedString *fAttachmentString = [[NSMutableAttributedString alloc]initWithAttributedString:[NSAttributedString attributedStringWithAttachment:fAttachment]];
//    [fAttachmentString appendAttributedString:[[NSAttributedString alloc]initWithString:@" "]];
//    NSInteger index = _lblF.text.length;
//    [fString insertAttributedString:fAttachmentString atIndex:index];
//    _lblF.attributedText = fString;
    
    
    NSAttributedString *fAttachmentString = [NSAttributedString attributedStringWithAttachment:fAttachment];
    [fString appendAttributedString:fAttachmentString];
    _lblF.attributedText = fString;
    
    //setting label
    NSMutableAttributedString *settingsString = [[NSMutableAttributedString alloc]initWithAttributedString:_lblSettings.attributedText];
    NSTextAttachment *settingsAttachment = [[NSTextAttachment alloc]init];
    settingsAttachment.image = [UIImage imageNamed:@"setting_small"];
    NSAttributedString *settingsAttachmentString = [NSAttributedString attributedStringWithAttachment:settingsAttachment];
    [settingsString appendAttributedString:settingsAttachmentString];
    _lblSettings.attributedText = settingsString;
    
    //bin icon
    if (_lblBinIcon) {
        NSMutableAttributedString *binString = [[NSMutableAttributedString alloc]initWithAttributedString:_lblBinIcon.attributedText];
        NSTextAttachment *binAttachment = [[NSTextAttachment alloc]init];
        binAttachment.image = [UIImage imageNamed:@"delet_small"];
        NSMutableAttributedString *binAttachmentString = [[NSMutableAttributedString alloc]initWithAttributedString:[NSAttributedString attributedStringWithAttachment:binAttachment]];
        [binAttachmentString appendAttributedString:[[NSAttributedString alloc]initWithString:@" "]];
        NSInteger index = @"3 Tap the bin icon ".length;
        [binString insertAttributedString:binAttachmentString atIndex:index];
        _lblBinIcon.attributedText = binString;
    }
    
    //camera flip icon
    NSMutableAttributedString *flipString = [[NSMutableAttributedString alloc]initWithAttributedString:_lblCameraFlip.attributedText];
    NSTextAttachment *flipAttachment = [[NSTextAttachment alloc]init];
    flipAttachment.image = [UIImage imageNamed:@"camera_flip_small"];
    NSAttributedString *flipAttachmentString = [NSAttributedString attributedStringWithAttachment:flipAttachment];
    [flipString appendAttributedString:flipAttachmentString];
    _lblCameraFlip.attributedText = flipString;
    
    //camera round icon
    if (_lblCameraIcon) {
        NSMutableAttributedString *cameraString = [[NSMutableAttributedString alloc]initWithAttributedString:_lblCameraIcon.attributedText];
        NSTextAttachment *cameraAttachment = [[NSTextAttachment alloc]init];
        cameraAttachment.image = [UIImage imageNamed:@"camera_button_small"];

        NSMutableAttributedString *cameraAttachmentString = [[NSMutableAttributedString alloc]initWithAttributedString:[NSAttributedString attributedStringWithAttachment:cameraAttachment]];
        [cameraAttachmentString appendAttributedString:[[NSAttributedString alloc]initWithString:@" "]];
        NSInteger index = @"2 tpa on the ".length;
        [cameraString insertAttributedString:cameraAttachmentString atIndex:index];
        _lblCameraIcon.attributedText = cameraString;
    }
    
    //white cross
    if (_lblWhiteCross) {
        NSMutableAttributedString *crossString = [[NSMutableAttributedString alloc]initWithAttributedString:_lblWhiteCross.attributedText];
        NSTextAttachment *crossAttachment = [[NSTextAttachment alloc]init];
        crossAttachment.image = [UIImage imageNamed:@"closes_small"];
        NSMutableAttributedString *crossAttachmentString = [[NSMutableAttributedString alloc]initWithAttributedString:[NSAttributedString attributedStringWithAttachment:crossAttachment]];
        [crossAttachmentString appendAttributedString:[[NSAttributedString alloc]initWithString:@" "]];
        NSInteger index = @"3 on the edit page you can add a caption by tapping on the photo, you can tap the ".length;
        [crossString insertAttributedString:crossAttachmentString atIndex:index];
        _lblWhiteCross.attributedText = crossString;
    }
    
    //sharing letters
    if (_lblGreenCircle) {
        NSMutableAttributedString *circleString = [[NSMutableAttributedString alloc]initWithAttributedString:_lblGreenCircle.attributedText];
        NSTextAttachment *circleAttachment = [[NSTextAttachment alloc]init];
        circleAttachment.image = [UIImage imageNamed:@"letter_box_brown_small"];
        
         NSMutableAttributedString *circleAttachmentString = [[NSMutableAttributedString alloc]initWithAttributedString:[NSAttributedString attributedStringWithAttachment:circleAttachment]];
        [circleAttachmentString appendAttributedString:[[NSAttributedString alloc]initWithString:@" "]];
        
        NSInteger index = @"2 You can share your letter with all of your friends at once by tapping ".length;
        [circleString insertAttributedString:circleAttachmentString atIndex:index];
        _lblGreenCircle.attributedText = circleString;
    }
    
    //viewing letters
    if (_lblLetterBin) {
        NSMutableAttributedString *binString = [[NSMutableAttributedString alloc]initWithAttributedString:_lblLetterBin.attributedText];
        NSTextAttachment *binAttachment = [[NSTextAttachment alloc]init];
        binAttachment.image = [UIImage imageNamed:@"delet_small"];
        NSMutableAttributedString *binAttachmentString = [[NSMutableAttributedString alloc]initWithAttributedString:[NSAttributedString attributedStringWithAttachment:binAttachment]];
        [binAttachmentString appendAttributedString:[[NSAttributedString alloc]initWithString:@" "]];
        NSInteger index = @"3 go to your own profile, select your letter and then tap the ".length;
        [binString insertAttributedString:binAttachmentString atIndex:index];
        _lblLetterBin.attributedText = binString;
    }
    
    //filters
    if (_lblFilters) {
        NSMutableAttributedString *filterString = [[NSMutableAttributedString alloc]initWithAttributedString:_lblFilters.attributedText];
        NSTextAttachment *filterAttachment = [[NSTextAttachment alloc]init];
        filterAttachment.image = [UIImage imageNamed:@"yinyang_brown_small"];
        NSMutableAttributedString *filterAttachmentString = [[NSMutableAttributedString alloc]initWithAttributedString:[NSAttributedString attributedStringWithAttachment:filterAttachment]];
        [filterAttachmentString appendAttributedString:[[NSAttributedString alloc]initWithString:@" "]];
        NSInteger index = @"1 after you take a selfie or video you can add filters by tapping ".length;
        [filterString insertAttributedString:filterAttachmentString atIndex:index];
        _lblFilters.attributedText = filterString;
    }
}

- (void)addLinks
{
    NSMutableAttributedString *downloadString = [[NSMutableAttributedString alloc]initWithString:_lblDownloadApp.text];
    NSString *partDownload = @"Download the Sharetimer app from the ";
    NSString *partItunes = @"itunes app store ";
    NSString *partForIos = @"for ios or from the ";
    NSString *partGoogle = @"google play store";
    NSRange rangeIos = NSMakeRange(partDownload.length, partItunes.length);
    NSRange rangeGoogle = NSMakeRange(partDownload.length + partItunes.length + partForIos.length, partGoogle.length);
    
    [downloadString addAttribute:NSLinkAttributeName value:[NSURL URLWithString:@"https://itunes.apple.com"] range:rangeIos];
    [downloadString addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:rangeIos];
    [downloadString addAttribute:NSLinkAttributeName value:[NSURL URLWithString:@"https://play.google.com/store/apps"] range:rangeGoogle];
    [downloadString addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:rangeGoogle];
    _lblDownloadApp.attributedText = downloadString;
}

#pragma mark - button click

- (IBAction)privacyPolicyClicked:(id)sender
{
//    kAlert(nil, @"Privacy Policy clicked");
    
    PrivacyPolicyViewController *blockListVc = [[PrivacyPolicyViewController alloc]initWithNibName:@"PrivacyPolicyViewController" bundle:nil];
    blockListVc.screenTitle = @"Privacy Policy";
    blockListVc.htmlFile = @"Privacy Policy";
    [self.navigationController pushViewController:blockListVc animated:YES];

    
}

- (IBAction)deleteClicked:(id)sender
{
    NSString *password = _txtPassword.text;
    if (!password.length) {
        kAlert(nil, @"Please enter your password");
        return;
    }
    if (password.length < 3) {
        kAlert(nil, @"Password too short");
        return;
    }
    
    STAlertview *alertConfirm = [[STAlertview alloc]initWithTitle:nil message:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"OK"]];
    alertConfirm.backgroundColor = [UIColor colorWithHexString:@"FF0000"];
    alertConfirm.tag = ALERT_CONFIRM_DELETE;
    [alertConfirm show];
}

#pragma mark - alert delegate

- (void)stAlertView:(STAlertview *)stAlertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (stAlertView.tag == ALERT_CONFIRM_DELETE) {
        [self sendDataToServerWithTask:TASK_DELETE_PROFILE];
    }
}

- (void)stAlertViewDidCancel
{
    
}

#pragma mark - text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - constraints

- (void)setConstraints
{
    int margin = (screenSize.width - 60)/3;
    _marginBlockedProfileView.constant = margin;
    _marginDeletedProfileView.constant = margin;
}

#pragma mark - screen images

- (void)showScreenImages
{
    //change username screen
    UILabel *lastLabel = _numberedLabels[_numberedLabels.count - 1];
    _heightChangeUsername.constant = screenSize.height - lastLabel.frame.origin.y - 180;
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@""];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_DELETE_PROFILE:
        {
            [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
            [postDict setObject:_txtPassword.text forKey:kPassword];
            [self.requestManager callServiceWithRequestType:TASK_DELETE_PROFILE method:METHOD_POST params:postDict urlEndPoint:@"users/deleteUser"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_DELETE_PROFILE:
            {
                //logout and go to home
                self.appDelegate.user = nil;
                [self.navigationController popToRootViewControllerAnimated:YES];
                kAlert(nil, @"Your profile has been deleted");
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}


@end
