//
//  EditViewController.h
//  sharetime
//
//  Created by iOS Developer on 01/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "BaseViewController.h"
#import "ScrollThemeView.h"

@interface EditViewController : BaseViewController

@property (strong, nonatomic) LetterModel *letterObject;
@property (nonatomic) BOOL isCameraFront;
@property (nonatomic) BOOL isFlashOn;
@property (nonatomic) BOOL willShowCamera;

@end
