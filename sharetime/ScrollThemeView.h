//
//  ScrollThemeView.h
//  sharetime
//
//  Created by iOS Developer on 29/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#define imgMaskFlat @"flat_mask"
#define imgCurvedMask @"curved_mask"

#import <UIKit/UIKit.h>
#import "LetterModel.h"
#import "SZTextView.h"
#import <AVKit/AVKit.h>

@protocol ScrollThemeViewDelegate <NSObject>

@end


@interface ScrollThemeView : UIView


@property (weak, nonatomic) IBOutlet UIImageView *profileTop;
@property (weak, nonatomic) IBOutlet UIButton *btnVolume;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) NSURL *videoUrl;
@property (nonatomic) NSInteger mediaType;
@property (weak, nonatomic) IBOutlet SZTextView *txtDescription;
@property (strong, nonatomic) LetterModel *letterObject;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
@property (nonatomic) BOOL imageOrVideoPicked, muteButtonClicked;
@property (nonatomic) NSInteger filterType;
@property (strong, nonatomic) NSData *imgData;
@property (strong, nonatomic) NSURL *filteredMovieUrl;
@property (weak, nonatomic) UIViewController<ScrollThemeViewDelegate>* delegate;
@property (weak, nonatomic) IBOutlet UIView *timePassedView;
@property (weak, nonatomic) IBOutlet UILabel *lblTimePassed;
@property (strong, nonatomic) AVPlayerViewController *playerVc;
@property (weak, nonatomic) IBOutlet UIView *viewVideoPlayArea;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewForVideoBlueBorder;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewForVideoWhiteBorder;

- (void)transalateResizingIntoConstraints;

- (void)showFlatImage:(UIImage*)image;

- (void)showCurvedImage:(UIImage*)image;

- (void)showImageFromVideoUrlWithMaskName:(NSString*)maskName;

- (void)applyFilter;

@end
