//
//  LoginViewController.m
//  sharetime
//
//  Created by iOS Developer on 26/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "LoginViewController.h"
#import "ResetPasswordViewController.h"
#import "HomeViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "SignupViewController.h"


@interface LoginViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton    *loginButton;
@property (weak, nonatomic) IBOutlet UIButton    *signupButton;


@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
   // [[UIApplication sharedApplication] setStatusBarHidden:NO];

    
    [_txtUsername addLeftPadding:12];
    [_txtPassword addLeftPadding:12];
    [self.navigationController addTitle:@"Log In" color:[UIColor colorWithHexString:@"#038c00"]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithHexString:@"#3a4347"]];
    [[UINavigationBar appearance] setTranslucent:NO];
    [self.navigationController addLeftBarButtonWithImageNamed:@"back_arrow"];
    [self.txtUsername becomeFirstResponder];
    
    
    //self.txtUsername.text = @"test";
    //self.txtPassword.text = @"123456";
//    self.txtUsername.text = @"userA";
//    self.txtPassword.text = @"secret";
    
    

}

- (IBAction)leftBarButtonClicked:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    self.appDelegate.user = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithHexString:@"#3a4347"]];
    [[UINavigationBar appearance] setTranslucent:NO];
    
    UserModel *user = self.appDelegate.user;
    if (user.username.length != 0) {
        _txtUsername.text = user.username;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button clicks

- (IBAction)forgotPasswordClicked:(id)sender
{
    ResetPasswordViewController *resetVc = [[ResetPasswordViewController alloc]initWithNibName:@"ResetPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:resetVc animated:YES];
}

- (IBAction)loginClicked:(id)sender
{
    NSString *username = _txtUsername.text;
    if (username.length == 0) {
        kAlert(nil, @"Please enter username");
        return;
    }
    NSString *password = _txtPassword.text;
    if (password.length == 0) {
        kAlert(nil, @"Please enter a password");
        return;
    }
    [self sendDataToServerWithTask:TASK_LOGIN];
}

- (IBAction)signupClicked:(id)sender
{
    SignupViewController *signupVc = [[SignupViewController alloc]initWithNibName:@"SignupViewController" bundle:nil];
    [self.navigationController pushViewController:signupVc animated:YES];
}

#pragma mark - text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
  
    switch (task) {
        case TASK_LOGIN:
        {
            [self showLoaderWithTitle:@"Logging in..."];
            
            NSString *username = _txtUsername.text;
            NSString *password = _txtPassword.text;
            [postDict setObject:username forKey:kUsername];
            [postDict setObject:password forKey:kPassword];
//            NSDictionary *dict = @{@"username":@"user1",@"password": @"secret"};
//            NSString *json = [self dictToJson:dict];
//            NSLog(@"JSON:%@",json);
            
//            NSString *deviceName = [[UIDevice currentDevice]name];
//            NSString *deviceId = [[NSUserDefaults standardUserDefaults]objectForKey:kDeviceUuid];
//            NSString *pushId = [[NSUserDefaults standardUserDefaults]objectForKey:kDeviceToken];
//            
//            if (!deviceId) {
//                deviceId = @"simulator";
//            }
//            if (!pushId) {
//                pushId = @"simulator";
//            }
//            [postDict setObject:deviceName forKey:kDeviceName];
//            [postDict setObject:deviceId forKey:kDeviceId];
//            [postDict setObject:pushId forKey:kPushId];
            
            [self.requestManager callServiceWithRequestType:TASK_LOGIN method:METHOD_POST params:postDict urlEndPoint:@"signin"];
            break;
        }
            
        default:
            break;
    }
   
      
}


- (NSString *)formatURLResponse:(NSHTTPURLResponse *)response withData:(NSData *)data
{
    NSString *responsestr = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    NSMutableString *message = [NSMutableString stringWithString:@"---RESPONSE------------------\n"];
    [message appendFormat:@"URL: %@\n",[response.URL description] ];
    [message appendFormat:@"MIMEType: %@\n",response.MIMEType];
    [message appendFormat:@"Status Code: %ld\n",(long)response.statusCode];
    for (NSString *header in [[response allHeaderFields] allKeys])
    {
        [message appendFormat:@"%@: %@\n",header,[response allHeaderFields][header]];
    }
    [message appendFormat:@"Response Data: %@\n",responsestr];
    [message appendString:@"----------------------------\n"];
    return [NSString stringWithFormat:@"%@",message];
}
#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_LOGIN:
            {
                NSDictionary *userDict = [responseDict objectForKey:@"user"];
                self.appDelegate.user = [[UserModel alloc]initWithResponse:userDict];
                
                HomeViewController *homeVc = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
                [self.navigationController pushViewController:homeVc animated:YES];
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

@end
