//
//  ListViewController.m
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "ListViewController.h"
#import "FriendTableViewCell.h"

@interface ListViewController ()<UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *_friends;
}

@property (weak, nonatomic) IBOutlet UITableView *tblFriends;

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self styleNavBar];
    if (_listType == LIST_VIEWS) {
        [self sendDataToServerWithTask:TASK_VIEW_LIST];
    } else {
        [self sendDataToServerWithTask:TASK_SCREENSHOT_LIST];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - nav

- (void)styleNavBar
{
    NSArray *titles = @[@"VIEWS", @"SCREENSHOT"];
    NSString *title = titles[self.listType];
    [self.navigationController addPlainTitle:title color:[UIColor whiteColor]];
    [self.navigationController addLeftBarButtonWithImageNamed:@"back_arrow"];
}

#pragma mark - table view delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _friends.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"FriendTableViewCell";
    FriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:identifier owner:self options:nil] lastObject];
    }
    UserModel *friend = _friends[indexPath.row];
    [cell bindUser:friend];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@""];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_VIEW_LIST:
        {
            [postDict setObject:_letterId forKey:@"shareId"];
            [self.requestManager callServiceWithRequestType:TASK_VIEW_LIST method:METHOD_POST params:postDict urlEndPoint:@"viewlist"];
            break;
        }
            
        case TASK_SCREENSHOT_LIST:
        {
            [postDict setObject:_letterId forKey:@"shareId"];
            [self.requestManager callServiceWithRequestType:TASK_SCREENSHOT_LIST method:METHOD_POST params:postDict urlEndPoint:@"screenshotlist"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_VIEW_LIST:
            {
                _friends = [[NSMutableArray alloc]init];
                NSArray *viewList = [responseDict objectForKey:@"viewlist"];
                for (NSDictionary *userDict in viewList) {
                    UserModel *user = [[UserModel alloc]initWithResponse:userDict];
                    [_friends addObject:user];
                }
                [_tblFriends reloadData];
                break;
            }
                
            case TASK_SCREENSHOT_LIST:
            {
                _friends = [[NSMutableArray alloc]init];
                NSArray *viewList = [responseDict objectForKey:@"screenshotlist"];
                for (NSDictionary *userDict in viewList) {
                    UserModel *user = [[UserModel alloc]initWithResponse:userDict];
                    [_friends addObject:user];
                }
                [_tblFriends reloadData];
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}


@end
