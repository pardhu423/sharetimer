//
//  UsernameViewController.m
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "UsernameViewController.h"
#import "RegionViewController.h"

@interface UsernameViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtUsername;

@end

@implementation UsernameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_txtUsername addLeftPadding:12];
    self.navigationController.navigationItem.hidesBackButton = YES;
   [self.navigationController addTitle:@"Username"];
}

#pragma mark - back - Overriding
- (IBAction)leftBarButtonClicked:(id)sender
{
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController addLeftBarButtonWithImageNamed:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button click

- (IBAction)continueClicked:(id)sender
{
    NSString *username = _user.username;
    if (username.length == 0) {
        kAlert(nil, @"Please enter a username");
        return;
    }
    
    [self sendDataToServerWithTask:TASK_CHECK_USERNAME];

}


#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_CHECK_USERNAME:
        {
            [self showLoaderWithTitle:@""];
            [postDict setObject:_user.username forKey:kUsername];
            [self.requestManager callServiceWithRequestType:TASK_CHECK_USERNAME method:METHOD_POST params:postDict urlEndPoint:@"users/validateUsername"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_CHECK_USERNAME:
            {
                RegionViewController *regionVc = [[RegionViewController alloc]initWithNibName:@"RegionViewController" bundle:nil];
                regionVc.user = _user;
                [self.navigationController pushViewController:regionVc animated:YES];
            }
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}


#pragma mark - text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)textChanged:(id)sender
{
    _user.username = _txtUsername.text;
}


@end
