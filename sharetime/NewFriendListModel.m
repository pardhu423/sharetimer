//
//  NewFriendListModel.m
//  sharetime
//
//  Created by Gopala Krishna Kammela on 01/02/16.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "NewFriendListModel.h"

@implementation NewFriendListModel

- (id)init
{
    self = [super init];
    if (self) {
        self.userId = @"";
        self.friendId = @"";
        self.confirm_status = @"";
        self.isNewFriend = @"";
    }
    return self;
}

- (id)initWithResponse:(NSDictionary *)response
{
    self = [super initWithResponse:response];
    if (self && [response isKindOfClass:[NSDictionary class]]) {
        NSArray *friendsList = [response objectForKey:kNewFriend];
        self.arrayNewFriends = [[NSMutableArray alloc]init];
        for (NSDictionary *dict in friendsList) {
            self.userId = [self checkNil:[dict objectForKey:kUserId]];
            self.friendId = [self checkNil:[dict objectForKey:kfriendId]];
            self.confirm_status = [self checkNil:[dict objectForKey:kConfirmStatus]];
            self.isNewFriend = [self checkNil:[dict objectForKey:kIsNewFriend]];
            [self.arrayNewFriends addObject:dict];
        }
    }
    return self;
}

@end
