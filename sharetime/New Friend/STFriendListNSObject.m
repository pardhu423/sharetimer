//
//  STFriendListNSObject.m
//
//  Created by iOS Developer on 01/02/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "STFriendListNSObject.h"
#import "STFriendListNewfriend.h"


NSString *const kSTFriendListNSObjectNewfriend = @"newFriendList";
NSString *const kSTFriendListNSObjectStatus = @"status";
NSString *const kSTFriendListNSObjectMessage = @"message";


@interface STFriendListNSObject ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation STFriendListNSObject

@synthesize newfriend = _newfriend;
@synthesize status = _status;
@synthesize message = _message;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedSTFriendListNewfriend = [dict objectForKey:kSTFriendListNSObjectNewfriend];
    NSMutableArray *parsedSTFriendListNewfriend = [NSMutableArray array];
    if ([receivedSTFriendListNewfriend isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedSTFriendListNewfriend) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedSTFriendListNewfriend addObject:[STFriendListNewfriend modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedSTFriendListNewfriend isKindOfClass:[NSDictionary class]]) {
       [parsedSTFriendListNewfriend addObject:[STFriendListNewfriend modelObjectWithDictionary:(NSDictionary *)receivedSTFriendListNewfriend]];
    }

    self.newfriend = [NSArray arrayWithArray:parsedSTFriendListNewfriend];
            self.status = [[self objectOrNilForKey:kSTFriendListNSObjectStatus fromDictionary:dict] boolValue];
            self.message = [self objectOrNilForKey:kSTFriendListNSObjectMessage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForNewfriend = [NSMutableArray array];
    for (NSObject *subArrayObject in self.newfriend) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForNewfriend addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForNewfriend addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForNewfriend] forKey:kSTFriendListNSObjectNewfriend];
    [mutableDict setValue:[NSNumber numberWithBool:self.status] forKey:kSTFriendListNSObjectStatus];
    [mutableDict setValue:self.message forKey:kSTFriendListNSObjectMessage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.newfriend = [aDecoder decodeObjectForKey:kSTFriendListNSObjectNewfriend];
    self.status = [aDecoder decodeBoolForKey:kSTFriendListNSObjectStatus];
    self.message = [aDecoder decodeObjectForKey:kSTFriendListNSObjectMessage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_newfriend forKey:kSTFriendListNSObjectNewfriend];
    [aCoder encodeBool:_status forKey:kSTFriendListNSObjectStatus];
    [aCoder encodeObject:_message forKey:kSTFriendListNSObjectMessage];
}

- (id)copyWithZone:(NSZone *)zone
{
    STFriendListNSObject *copy = [[STFriendListNSObject alloc] init];
    
    if (copy) {

        copy.newfriend = [self.newfriend copyWithZone:zone];
        copy.status = self.status;
        copy.message = [self.message copyWithZone:zone];
    }
    
    return copy;
}


@end
