//
//  STFriendListNewfriend.h
//
//  Created by iOS Developer on 01/02/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface STFriendListNewfriend : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double status;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, assign) double friendId;
@property (nonatomic, assign) double newfriendIdentifier;
@property (nonatomic, assign) double isNewfriend;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, assign) double userId;
@property (nonatomic, assign) double confirmStatus;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, strong) NSString *created;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
