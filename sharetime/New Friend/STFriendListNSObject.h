//
//  STFriendListNSObject.h
//
//  Created by iOS Developer on 01/02/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface STFriendListNSObject : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *newfriend;
@property (nonatomic, assign) BOOL status;
@property (nonatomic, strong) NSString *message;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
