//
//  STFriendListNewfriend.m
//
//  Created by iOS Developer on 01/02/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "STFriendListNewfriend.h"


NSString *const kSTFriendListNewfriendStatus = @"status";
NSString *const kSTFriendListNewfriendGender = @"gender";
NSString *const kSTFriendListNewfriendFriendId = @"friend_id";
NSString *const kSTFriendListNewfriendId = @"id";
NSString *const kSTFriendListNewfriendIsNewfriend = @"is_newfriend";
NSString *const kSTFriendListNewfriendCreatedAt = @"created_at";
NSString *const kSTFriendListNewfriendUsername = @"username";
NSString *const kSTFriendListNewfriendUserId = @"user_id";
NSString *const kSTFriendListNewfriendConfirmStatus = @"confirm_status";
NSString *const kSTFriendListNewfriendUpdatedAt = @"updated_at";
NSString *const kSTFriendListNewfriendCreated = @"created";


@interface STFriendListNewfriend ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation STFriendListNewfriend

@synthesize status = _status;
@synthesize gender = _gender;
@synthesize friendId = _friendId;
@synthesize newfriendIdentifier = _newfriendIdentifier;
@synthesize isNewfriend = _isNewfriend;
@synthesize createdAt = _createdAt;
@synthesize username = _username;
@synthesize userId = _userId;
@synthesize confirmStatus = _confirmStatus;
@synthesize updatedAt = _updatedAt;
@synthesize created = _created;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.status = [[self objectOrNilForKey:kSTFriendListNewfriendStatus fromDictionary:dict] doubleValue];
            self.gender = [[self objectOrNilForKey:kSTFriendListNewfriendGender fromDictionary:dict] stringValue];
            self.friendId = [[self objectOrNilForKey:kSTFriendListNewfriendFriendId fromDictionary:dict] doubleValue];
            self.newfriendIdentifier = [[self objectOrNilForKey:kSTFriendListNewfriendId fromDictionary:dict] doubleValue];
            self.isNewfriend = [[self objectOrNilForKey:kSTFriendListNewfriendIsNewfriend fromDictionary:dict] doubleValue];
            self.createdAt = [self objectOrNilForKey:kSTFriendListNewfriendCreatedAt fromDictionary:dict];
            self.username = [self objectOrNilForKey:kSTFriendListNewfriendUsername fromDictionary:dict];
            self.userId = [[self objectOrNilForKey:kSTFriendListNewfriendUserId fromDictionary:dict] doubleValue];
            self.confirmStatus = [[self objectOrNilForKey:kSTFriendListNewfriendConfirmStatus fromDictionary:dict] doubleValue];
            self.updatedAt = [self objectOrNilForKey:kSTFriendListNewfriendUpdatedAt fromDictionary:dict];
            self.created = [self objectOrNilForKey:kSTFriendListNewfriendCreated fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.status] forKey:kSTFriendListNewfriendStatus];
    [mutableDict setValue:self.gender forKey:kSTFriendListNewfriendGender];
    [mutableDict setValue:[NSNumber numberWithDouble:self.friendId] forKey:kSTFriendListNewfriendFriendId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.newfriendIdentifier] forKey:kSTFriendListNewfriendId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isNewfriend] forKey:kSTFriendListNewfriendIsNewfriend];
    [mutableDict setValue:self.createdAt forKey:kSTFriendListNewfriendCreatedAt];
    [mutableDict setValue:self.username forKey:kSTFriendListNewfriendUsername];
    [mutableDict setValue:[NSNumber numberWithDouble:self.userId] forKey:kSTFriendListNewfriendUserId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.confirmStatus] forKey:kSTFriendListNewfriendConfirmStatus];
    [mutableDict setValue:self.updatedAt forKey:kSTFriendListNewfriendUpdatedAt];
    [mutableDict setValue:self.created forKey:kSTFriendListNewfriendCreated];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.status = [aDecoder decodeDoubleForKey:kSTFriendListNewfriendStatus];
    self.gender = [aDecoder decodeObjectForKey:kSTFriendListNewfriendGender];
    self.friendId = [aDecoder decodeDoubleForKey:kSTFriendListNewfriendFriendId];
    self.newfriendIdentifier = [aDecoder decodeDoubleForKey:kSTFriendListNewfriendId];
    self.isNewfriend = [aDecoder decodeDoubleForKey:kSTFriendListNewfriendIsNewfriend];
    self.createdAt = [aDecoder decodeObjectForKey:kSTFriendListNewfriendCreatedAt];
    self.username = [aDecoder decodeObjectForKey:kSTFriendListNewfriendUsername];
    self.userId = [aDecoder decodeDoubleForKey:kSTFriendListNewfriendUserId];
    self.confirmStatus = [aDecoder decodeDoubleForKey:kSTFriendListNewfriendConfirmStatus];
    self.updatedAt = [aDecoder decodeObjectForKey:kSTFriendListNewfriendUpdatedAt];
    self.created = [aDecoder decodeObjectForKey:kSTFriendListNewfriendCreated];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_status forKey:kSTFriendListNewfriendStatus];
    [aCoder encodeObject:_gender forKey:kSTFriendListNewfriendGender];
    [aCoder encodeDouble:_friendId forKey:kSTFriendListNewfriendFriendId];
    [aCoder encodeDouble:_newfriendIdentifier forKey:kSTFriendListNewfriendId];
    [aCoder encodeDouble:_isNewfriend forKey:kSTFriendListNewfriendIsNewfriend];
    [aCoder encodeObject:_createdAt forKey:kSTFriendListNewfriendCreatedAt];
    [aCoder encodeObject:_username forKey:kSTFriendListNewfriendUsername];
    [aCoder encodeDouble:_userId forKey:kSTFriendListNewfriendUserId];
    [aCoder encodeDouble:_confirmStatus forKey:kSTFriendListNewfriendConfirmStatus];
    [aCoder encodeObject:_updatedAt forKey:kSTFriendListNewfriendUpdatedAt];
    [aCoder encodeObject:_created forKey:kSTFriendListNewfriendCreated];
}

- (id)copyWithZone:(NSZone *)zone
{
    STFriendListNewfriend *copy = [[STFriendListNewfriend alloc] init];
    
    if (copy) {

        copy.status = self.status;
        copy.gender = [self.gender copyWithZone:zone];
        copy.friendId = self.friendId;
        copy.newfriendIdentifier = self.newfriendIdentifier;
        copy.isNewfriend = self.isNewfriend;
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.username = [self.username copyWithZone:zone];
        copy.userId = self.userId;
        copy.confirmStatus = self.confirmStatus;
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.created = [self.created copyWithZone:zone];
    }
    
    return copy;
}


@end
