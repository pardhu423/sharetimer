//
//  ProfileViewController.m
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "ProfileViewController.h"
#import "FriendViewController.h"
#import "ScrollThemeView.h"
#import "SettingsViewController.h"
#import "MailboxViewController.h"
#import "HomeViewController.h"
#import "LettersViewController.h"

@interface ProfileViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {
    NSMutableArray *_letters;
    UIColor *oldColor;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collLetters;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self styleNavBar];
    [self.collLetters registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"identifier"];
    
    if (_galleryTask) {
        [self sendDataToServerWithTask:_galleryTask];
    } else {
        if (_isMyProfile) {
            [self sendDataToServerWithTask:TASK_MY_GALLERY];
        } else {
            [self sendDataToServerWithTask:TASK_FRIEND_GALLERY];
        }
    }
    [self.view addHorizontalSwipeRecognizersWithTarget:self leftSwipeSelector:nil rightSwipeSelector:@selector(leftSwipeDetected:)];
}
- (IBAction)leftBarButtonClicked:(id)sender
{
    NSArray *viewControllers = self.navigationController.viewControllers;
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[HomeViewController class]]) {
            [self.navigationController popToViewController:vc animated:YES];
            break;
        }
    }
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_collLetters reloadData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    oldColor = self.navigationController.navigationBar.barTintColor;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"3a4347"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = oldColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - gesture

- (IBAction)leftSwipeDetected:(id)sender
{
    LettersViewController *letterVc = [[LettersViewController alloc]initWithNibName:@"LettersViewController" bundle:nil];
    //letterVc.isMyProfile = YES;
    [self.navigationController pushViewController:letterVc animated:YES];
}


#pragma mark - nav

- (void)styleNavBar
{
    if (_isMyProfile) {
        NSString *username = self.appDelegate.user.username;
        [self.navigationController addLeftBarButtonWithImageNamed:@"paper_blue"];

        [self.navigationController addTitleColor:username];
        [self.navigationController addRightBarButtonWithImageNamed:@"setting"];
    } else {
        [self.navigationController addTitleColor:_user.username];
        [self showUserImage];
        [self.navigationController addRightBarButtonWithImageNamed:@"closes"];
    }
}

- (void)showUserImage
{
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
    img.backgroundColor = [UIColor cellBrownColor];
    NSString *imgName = genders[_user.gender];
    img.image = [UIImage imageNamed:imgName];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:img];
}

- (IBAction)rightBarButtonClicked:(id)sender
{
    if (_isMyProfile) {
        SettingsViewController *settingsVc = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self.navigationController pushViewController:settingsVc animated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - collection view delegate flow layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int width = collectionView.frame.size.width/4;
    int height = width * 1.5;
    return CGSizeMake(width, height);
}

#pragma mark - collection view data source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _letters.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"identifier" forIndexPath:indexPath];
    for (UIView *subview in cell.subviews) {
        [subview removeFromSuperview];
    }
    
    int cellWidth = cell.frame.size.width;
    int leftMargin = 5;
    
    UIView *scrollContainerView = [[UIView alloc]initWithFrame:CGRectMake(leftMargin, 0, cellWidth - 2 * leftMargin, cellWidth * 1.5)];
    [cell addSubview:scrollContainerView];
    
    LetterModel *letter = _letters[indexPath.row];
    NSString *nibName = @"ScrollThemeView";
    if (letter.scrollTheme == SCROLL_THEME_FLAT) {
        nibName = @"FlatScrollView";
    }
    ScrollThemeView *scroll = [[[NSBundle mainBundle]loadNibNamed:nibName owner:self options:nil]lastObject];
    scroll.letterObject = letter;
    scroll.viewVideoPlayArea.backgroundColor = [UIColor clearColor];
    scroll.imageViewForVideoBlueBorder.hidden = YES;
    scroll.imageViewForVideoWhiteBorder.hidden = YES;
    
    [scrollContainerView addSubview:scroll];
    [scroll resizeViewConstrainedTo:scrollContainerView.frame.size animated:NO];
    [scroll transalateResizingIntoConstraints];
    
    [cell addBottomShadeWithWidth:1 color:[UIColor lightGrayColor]];
    return cell;
}

#pragma mark - collection view delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FriendViewController *friendVc = [[FriendViewController alloc]initWithNibName:@"FriendViewController" bundle:nil];
    friendVc.letter = _letters[indexPath.row];
    friendVc.isMyProfile = _isMyProfile;
    friendVc.letters = _letters;
    if (_galleryTask != TASK_LETTERS_SENT_TO_FRIEND) {
        friendVc.rowId = _user.rowId;
    }
    friendVc.selectedLetterIndex = indexPath.row;
    [self.navigationController pushViewController:friendVc animated:YES];
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@""];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_FRIEND_GALLERY:
        {
            [postDict setObject:self.user.Id forKey:kUserId];
            [self.requestManager callServiceWithRequestType:TASK_MY_GALLERY method:METHOD_POST params:postDict urlEndPoint:@"userletters"];
            break;
        }
            
        case TASK_MY_GALLERY:
        {
            [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
            [self.requestManager callServiceWithRequestType:TASK_MY_GALLERY method:METHOD_POST params:postDict urlEndPoint:@"profilegallery"];
            break;
        }
            
        case TASK_INBOX_LETTERS_BY_FRIEND:
        {
            [postDict setObject:self.user.Id forKey:kFriendId];
            [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
            [self.requestManager callServiceWithRequestType:TASK_INBOX_LETTERS_BY_FRIEND method:METHOD_POST params:postDict urlEndPoint:@"sharefrienddetails"];
            break;
        }
            
        case TASK_LETTERS_SENT_TO_FRIEND:
        {
            [postDict setObject:self.user.Id forKey:kFriendId];
            [self.requestManager callServiceWithRequestType:TASK_INBOX_LETTERS_BY_FRIEND method:METHOD_POST params:postDict urlEndPoint:@"sharefrienddetailssent"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_INBOX_LETTERS_BY_FRIEND:
                //fall through
                
            case TASK_LETTERS_SENT_TO_FRIEND:
                //fall through
                
            TASK_FRIEND_GALLERY:
                //fall through
                
            case TASK_MY_GALLERY:
            {
                NSArray *gallery = [responseDict objectForKey:@"gallerylist"];

                _letters = [[NSMutableArray alloc]init];
                for (NSDictionary *letterDict in gallery) {
                    LetterModel *letter = [[LetterModel alloc]initWithResponse:letterDict];
                    [_letters addObject:letter];
                }
                [self.collLetters reloadData];
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}


@end
