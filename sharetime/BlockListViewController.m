//
//  BlockListViewController.m
//  sharetime
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#define ALERT_UNBLOCK 100

#import "BlockListViewController.h"

@interface BlockListViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, STAlertViewDelegate> {
    NSMutableArray *_blockList;
    NSArray *_filteredBlockList;
    NSInteger _selectedFriendIndex;
}

@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UICollectionView *collFriends;

@end

@implementation BlockListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController addTitle:@"Block List"];
    float screenWidth = screenSize.width;
    float scaleX = screenWidth/320.0;
    [_txtSearch addLeftPadding:55 * scaleX];
    [_txtSearch addRightPadding:15];
    [self.collFriends registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"identifier"];
    
    //text search
    [_txtSearch addLeftImageNamed:@"search"];
    [_txtSearch addRightButtonWithImageName:@"search_close" target:self selector:@selector(cancelSearchClicked:) viewMode:UITextFieldViewModeWhileEditing];
    [self sendDataToServerWithTask:TASK_BLOCK_LIST];
    _selectedFriendIndex = -1;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_txtSearch addBottomBorderWithWidth:1 color:[UIColor lightGrayColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - text field delegate

- (IBAction)cancelSearchClicked:(id)sender
{
    _txtSearch.text = @"";
    [_txtSearch resignFirstResponder];
    [self searchTextChanged:_txtSearch];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)searchTextChanged:(id)sender
{
    NSString *newText = [_txtSearch.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (newText.length) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"SELF.username beginswith[c] %@",
                                  newText];
        _filteredBlockList = [_blockList filteredArrayUsingPredicate:predicate];
    } else {
        _filteredBlockList = _blockList;
    }
    [self.collFriends reloadData];
}

#pragma mark - collection view delegate flow layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int width = collectionView.frame.size.width/4;
    int height = width + 25;
    return CGSizeMake(width, height);
}

#pragma mark - collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _filteredBlockList.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"identifier" forIndexPath:indexPath];
    for (UIView *subview in cell.subviews) {
        [subview removeFromSuperview];
    }
    
    UserModel *user = _filteredBlockList[indexPath.row];
    
    int width = collectionView.frame.size.width/4;
    int cellMargin = 10;
    int startY = 5;
    int imgWidth = width - cellMargin;
    CGRect frame = CGRectMake(cellMargin, startY, imgWidth, imgWidth);
    
    //show name
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(cellMargin, imgWidth + startY, imgWidth, 20)];
    lbl.adjustsFontSizeToFitWidth = YES;
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.text = user.username;
    [cell addSubview:lbl];
    
    if (indexPath.row == _selectedFriendIndex) {
        //unblock label
        UILabel *lbl = [[UILabel alloc]initWithFrame:frame];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = @"Unblock";
        lbl.backgroundColor = [UIColor cellGreenColor];
        [cell addSubview:lbl];
    } else {
        //user image
        UIImageView *img = [[UIImageView alloc]initWithFrame:frame];
        [cell addSubview:img];
        
        img.backgroundColor = [UIColor cellBrownColor];
        NSString *imgName = genders[user.gender];
        img.image = [UIImage imageNamed:imgName];
        
        if (user.confirmStatus == STATUS_BLOCKED || user.confirmStatus == STATUS_REJECTED) {
            NSString *imgName = user.confirmStatus == STATUS_BLOCKED ? @"block" : @"red_cross";
            UIImageView *imgCross = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgName]];
            imgCross.frame = frame;
            [cell addSubview:imgCross];
        }
    }
    
    [cell addBottomShadeWithWidth:1 color:[UIColor lightGrayColor]];
    return cell;
}

#pragma mark - collection view delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedFriendIndex = indexPath.row;
    STAlertview *alert = [[STAlertview alloc]initWithTitle:nil message:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"OK"]];
    alert.tag = ALERT_UNBLOCK;
    [alert show];
    [collectionView reloadData];
}

- (IBAction)rightSwipeDetected:(UITapGestureRecognizer*)tap
{
    
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@""];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_BLOCK_LIST:
        {
            NSString *userId = self.appDelegate.user.Id;
            [postDict setObject:userId forKey:kUserId];
            
            [self.requestManager callServiceWithRequestType:TASK_BLOCK_LIST method:METHOD_POST params:postDict urlEndPoint:@"users/blockList"];
            break;
        }
            
        default:
            break;
    }
}

- (void)setStatus:(int)status forUser:(UserModel*)user
{
    //STATUS CAN BE pending, accepted, rejected or blocked
    [self showLoaderWithTitle:@""];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setObject:user.Id forKey:kFriendId];
    [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
    [postDict setObject:[NSString stringWithFormat:@"%d", status] forKey:@"action"];
    [self.requestManager callServiceWithRequestType:TASK_CHANGE_FRIEND_STATUS method:METHOD_POST params:postDict urlEndPoint:@"responseOnRequest"];
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_BLOCK_LIST:
            {
                _blockList = [[NSMutableArray alloc]init];
                NSArray *friends = [responseDict objectForKey:@"blockList"];
                for (NSDictionary *userDict in friends) {
                    UserModel *user = [[UserModel alloc]initWithResponse:userDict];
                    [_blockList addObject:user];
                }
                _filteredBlockList = _blockList;
                [_collFriends reloadData];
                break;
            }
                
            case TASK_CHANGE_FRIEND_STATUS:
            {
                UserModel *user = _filteredBlockList[_selectedFriendIndex];
                NSDictionary *responceDict = [responseDict objectForKey:@"responce"];
                user.confirmStatus = [[responceDict objectForKey:@"action"] integerValue];
                _selectedFriendIndex = -1;
                [self.collFriends reloadData];
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    if (tag == TASK_ADD_FRIEND) {
        kAlert(nil, error.localizedDescription);
        return;
    }
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

#pragma mark - st alert view delegate

- (void)stAlertView:(STAlertview *)stAlertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (stAlertView.tag == ALERT_UNBLOCK) {
        UserModel *user = _filteredBlockList[_selectedFriendIndex];
        [self setStatus:STATUS_UNBLOCKED forUser:user];
    }
}

- (void)stAlertViewDidCancel
{
    _selectedFriendIndex = -1;
    [_collFriends reloadData];
}

@end
