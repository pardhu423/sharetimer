//
//  UINavigationController+Utility.h
//  Paytm
//
//  Created by iOS Developer on 23/07/15.
//  Copyright (c) 2016 Henote Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Utility)

+ (void)setBackgroundColor:(UIColor*)color;
- (void)addLeftBarButtonWithImageNamed:(NSString*)imgName;
- (void)addRightBarButtonWithImageNamed:(NSString*)imgName;
- (void)addPlainTitle:(NSString*)title color:(UIColor*)color;
- (void)addTitle:(NSString*)title;
- (void)addTitleColor:(NSString *)title;
- (void)addTitle:(NSString *)title color:(UIColor*)color;
- (void)addEmptyViewToLeftBarButton;
- (void)addEmptyViewToRightBarButton;

@end
