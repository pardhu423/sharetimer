//
//  HomeViewController.m
//  sharetime
//
//  Created by iOS Developer on 29/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

enum {
    SCROLL_FLAT,
    SCROLL_CURVED
};

#import "HomeViewController.h"
#import "ScrollThemeView.h"
#import "AcceptedFriendsViewController.h"
#import "EditViewController.h"
#import "MailboxViewController.h"
#import "LettersViewController.h"
#import "ProfileViewController.h"
#import "FriendsViewController.h"
#import "AddFriendsViewController.h"

@interface HomeViewController ()<UIImagePickerControllerDelegate, ScrollThemeViewDelegate> {
    UIImagePickerController *_imagePicker;
    BOOL _didPushEditScreen;
    BOOL directCameraShown;
}

@property (weak, nonatomic) IBOutlet UIButton *btnFlash;
@property (weak, nonatomic) IBOutlet UIButton *btnFrontBack;
@property (weak, nonatomic) IBOutlet UIImageView *imgCameraButton;
@property (weak, nonatomic) IBOutlet UIButton *btnFriends;
@property (weak, nonatomic) IBOutlet UIView *scrollContainerView;
@property (weak, nonatomic) IBOutlet UIButton *btnPlus;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *blurView;
@property (weak, nonatomic) IBOutlet UIButton *btnMail;
@property (weak, nonatomic) IBOutlet UILabel *lblMailCount;
@property (weak, nonatomic) IBOutlet UIButton *btnLetterBox;
@property (weak, nonatomic) IBOutlet UIButton *btnProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblLetterCount;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_imgCameraButton addTapRecognizerWithTarget:self selector:@selector(cameraClicked:)];
    [_imgCameraButton addLongPressRecognizerWithTarget:self selector:@selector(cameraLongPressed:)];
    [self.view addHorizontalSwipeRecognizersWithTarget:self leftSwipeSelector:@selector(lettersClicked:) rightSwipeSelector:@selector(mailboxClicked:)];
    [_lblMailCount roundCornersWithRadius:_lblMailCount.frame.size.width/2 borderColor:[UIColor clearColor] borderWidth:1];
    
    [_lblLetterCount roundCornersWithRadius:_lblLetterCount.frame.size.width/2 borderColor:[UIColor clearColor] borderWidth:1];
    directCameraShown = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self blurBackgroundImage];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _didPushEditScreen = NO;
    
    if (!_flatScrollView) {
        [self initializeScrollThemes];
        [_flatScrollView resizeViewConstrainedTo:_scrollContainerView.frame.size animated:YES];
    } else {
        //run only if coming second time

    }
    [self sendDataToServerWithTask:TASK_HOMESCREEN_COUNT];
    
    _btnFrontBack.selected = ![[NSUserDefaults standardUserDefaults]boolForKey:kBackFacingFlash];
}

- (void)showLaunchImage
{
    UIImageView *launchImage = [[UIImageView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    NSString *imgName = (screenSize.height == heightSmallScreen) ? @"home_screen_small.jpg" : @"home_screen.jpg";
    launchImage.image = [UIImage imageNamed:imgName];
    [[[UIApplication sharedApplication]keyWindow] addSubview:launchImage];
    [launchImage performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:1.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initializeScrollThemes
{
    _flatScrollView = [[[NSBundle mainBundle]loadNibNamed:@"FlatScrollView" owner:self options:nil] lastObject];
    _flatScrollView.delegate = self;
    _selectedScroll = SCROLL_FLAT;
    _curvedScrollView = [[[NSBundle mainBundle] loadNibNamed:@"ScrollThemeView" owner:self options:nil] lastObject];
    _curvedScrollView.delegate = self;
    //[self.scrollContainerView addSubview:_flatScrollView];
}

#pragma mark - gesture

- (void)showEditPageForMediaType:(NSInteger)mediaType willShowCamera:(BOOL)willShowCamera
{

    EditViewController *editVc = [[EditViewController alloc]initWithNibName:@"EditViewController" bundle:nil];
    
    switch (_selectedScroll) {
        case SCROLL_FLAT:
            editVc.letterObject = _flatScrollView.letterObject;
            
//            //clear the image to prevent memory leakage
//            _flatScrollView.imgProfilePic.image = nil;
            break;
            
        case SCROLL_CURVED:
            editVc.letterObject = _curvedScrollView.letterObject;
//            //removing this line will cause memory leakage
//            _curvedScrollView.imgProfilePic.image = nil;
            break;
            
        default:
            break;
    }
    editVc.letterObject.scrollTheme = _selectedScroll;
    editVc.letterObject.mediaType = mediaType;
    editVc.isFlashOn = _btnFlash.selected;
    editVc.isCameraFront = !_btnFrontBack.selected;
    editVc.willShowCamera = willShowCamera;
    [self.navigationController pushViewController:editVc animated:YES];
}

- (IBAction)cameraClicked:(id)sender
{
    [self showEditPageForMediaType:MEDIA_TYPE_IMAGE willShowCamera:YES];
}

- (IBAction)cameraLongPressed:(id)sender
{
 /*   if (!_didPushEditScreen) {
        [self showEditPageForMediaType:MEDIA_TYPE_VIDEO willShowCamera:YES];
        _didPushEditScreen = YES;
    }*/
}

#pragma mark - button click
                    
- (void)blurBackgroundImage
{
    self.blurView.hidden = YES;
//    self.blurView.hidden =
//        !((self.selectedScroll == SCROLL_FLAT && _flatScrollView.imageOrVideoPicked) ||
//        (self.selectedScroll == SCROLL_CURVED && _curvedScrollView.imageOrVideoPicked));
}

- (IBAction)scrollBrownClicked:(id)sender
{
    _selectedScroll = SCROLL_FLAT;
    //flat
    _flatScrollView.viewVideoPlayArea.backgroundColor = [UIColor clearColor];
    _flatScrollView.imageViewForVideoBlueBorder.hidden = YES;
    _flatScrollView.imageViewForVideoWhiteBorder.hidden = YES;
    _flatScrollView.txtDescription.selectable = NO;
    _flatScrollView.txtDescription.editable = NO;
    _flatScrollView.timePassedView.hidden = YES;
    [_curvedScrollView removeFromSuperview];
    [self.scrollContainerView addSubview:_flatScrollView];
    [self blurBackgroundImage];
    //self.btnPlus.enabled = _flatScrollView.imageOrVideoPicked;
}

- (IBAction)scrollBlueClicked:(id)sender
{
    _selectedScroll = SCROLL_CURVED;
    [_flatScrollView removeFromSuperview];
    
    //curved
    _curvedScrollView.viewVideoPlayArea.backgroundColor = [UIColor clearColor];
    _curvedScrollView.imageViewForVideoBlueBorder.hidden = YES;
    _curvedScrollView.imageViewForVideoWhiteBorder.hidden = YES;
    _curvedScrollView.txtDescription.editable = NO;
    _curvedScrollView.txtDescription.selectable = NO;
    _curvedScrollView.timePassedView.hidden = YES;
    
    [self.scrollContainerView addSubview:_curvedScrollView];
    CGSize scrollSize = _scrollContainerView.frame.size;
    [_curvedScrollView resizeViewConstrainedTo:scrollSize animated:NO];
    [_curvedScrollView transalateResizingIntoConstraints];
    
    [self blurBackgroundImage];
    //self.btnPlus.enabled = _curvedScrollView.imageOrVideoPicked;
}

- (IBAction)mailboxClicked:(id)sender
{
    MailboxViewController *mailboxVc = [[MailboxViewController alloc]initWithNibName:@"MailboxViewController" bundle:nil];
    [self.navigationController pushViewController:mailboxVc animated:YES];
}

- (IBAction)lettersClicked:(id)sender
{
    LettersViewController *lettersVc = [[LettersViewController alloc]initWithNibName:@"LettersViewController" bundle:nil];
    [self.navigationController pushViewController:lettersVc animated:YES];
}

- (IBAction)userProfileClicked:(id)sender
{
    ProfileViewController *profileVc = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
    profileVc.isMyProfile = YES;
    [self.navigationController pushViewController:profileVc animated:YES];
}

- (IBAction)flipImageClicked:(UIButton*)btnFlip
{
    BOOL backFacingCamera = [[NSUserDefaults standardUserDefaults]boolForKey:kBackFacingFlash];
    if (!backFacingCamera) {
        kAlert(nil, @"camera is fixed at front facing. Please change at settings > Manage > Front facing camera");
    } else {
        btnFlip.selected = !btnFlip.selected;
        if (btnFlip.selected) {
            kAlert(nil, @"camera is now facing you");
        } else {
            kAlert(nil, @"camera is facing away from you");
        }
    }
}

- (IBAction)addFriendClicked:(id)sender
{
    if (_btnPlus.selected) {
        AcceptedFriendsViewController *acceptedVc = [[AcceptedFriendsViewController alloc]initWithNibName:@"AcceptedFriendsViewController" bundle:nil];
        [self.navigationController pushViewController:acceptedVc animated:YES];
    } else {
        AddFriendsViewController *addFriendsVc = [[AddFriendsViewController alloc]initWithNibName:@"AddFriendsViewController" bundle:nil];
        [self.navigationController pushViewController:addFriendsVc animated:YES];
    }
}

- (IBAction)friendsClicked:(id)sender
{
    FriendsViewController *friendsVc = [[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
    [self.navigationController pushViewController:friendsVc animated:YES];
}

- (IBAction)flashClicked:(UIButton*)btnFlash
{
    btnFlash.selected = !btnFlash.selected;
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@"Loading..."];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_HOMESCREEN_COUNT:
        {
            [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
            [self.requestManager callServiceWithRequestType:TASK_HOMESCREEN_COUNT method:METHOD_POST params:postDict urlEndPoint:@"homescreencount"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (!success) {
        switch (requestType) {
            case TASK_HOMESCREEN_COUNT:
            {
                NSDictionary *dataDict = [responseDict objectForKey:@"notification"];
                NSString *unreadCount = [NSString stringWithFormat:@"%@", [dataDict objectForKey:@"unread"]];
                NSInteger unreadMessageCount = [unreadCount integerValue];
                _lblMailCount.hidden = unreadMessageCount == 0;
                _lblMailCount.text = unreadCount;
                
                _lblLetterCount.hidden = unreadMessageCount == 0;
                _lblLetterCount.text = unreadCount;
                
                NSInteger pendingCount = [[dataDict objectForKey:@"pending"] integerValue];
                _btnFriends.selected = pendingCount > 0;
                
                NSInteger newLetterCount = [[dataDict objectForKey:@"newletters"] integerValue];
                _btnLetterBox.selected = newLetterCount > 0;
                
                NSInteger newFriendCount = [[dataDict objectForKey:@"newfriendcount"] integerValue];
                _btnPlus.selected = newFriendCount;
                
//                NSArray *arr = [responseDict objectForKey:@"userdetails"];
                NSDictionary *dictForGender = [responseDict objectForKey:@"userdetails"];
                NSString *gender = [[dictForGender objectForKey:@"gender"] stringValue];
                if ([gender isEqual:@"1"]) {
                    [_btnProfile setImage:[UIImage imageNamed:@"your_profile_female"] forState:UIControlStateNormal];
                } else {
                    [_btnProfile setImage:[UIImage imageNamed:@"your_profile"] forState:UIControlStateNormal];
                }
                if (directCameraShown == YES) {
                    directCameraShown = NO;
                    [self showEditPageForMediaType:MEDIA_TYPE_IMAGE willShowCamera:YES];
                }
                break;
            }
        }
    } else {
//        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}


@end
