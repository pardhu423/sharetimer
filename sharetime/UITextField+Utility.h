//
//  UITextField+Utility.h
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Utility)

- (void)addLeftImageNamed:(NSString*)imgName;

- (void)addLeftPadding:(CGFloat)padding;

- (void)addRightPadding:(CGFloat)padding;

- (void)addRightButtonWithImageName:(NSString*)imgName target:(id)target selector:(SEL)selector;

- (void)addRightButtonWithImageName:(NSString *)imgName target:(id)target selector:(SEL)selector viewMode:(UITextFieldViewMode)viewMode;

- (void)addRightImageWithImageName:(NSString*)imgName;

- (void)addToolbarWithTarget:(id)target DoneSelector:(SEL)doneSelector;

- (void)addToolbarWithTarget:(id)target DoneSelector:(SEL)doneSelector nextSelector:(SEL)nextSelector;

@end
