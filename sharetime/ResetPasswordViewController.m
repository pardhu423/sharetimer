//
//  ResetPasswordViewController.m
//  sharetime
//
//  Created by iOS Developer on 29/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "ResetPasswordViewController.h"

@interface ResetPasswordViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtCaptcha;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMargin;
@property (weak, nonatomic) IBOutlet UILabel *lblYouCanCheck;
@property (weak, nonatomic) IBOutlet UILabel *lblCaptcha;

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController addTitle:@"Reset Password"];
    [self.navigationController addEmptyViewToRightBarButton];
    [self.txtCaptcha addLeftPadding:12];
    [self.txtEmail addLeftPadding:12];
    [self.txtEmail addRightImageWithImageName:@"message"];
    if (screenSize.height == heightSmallScreen) {
        _lblYouCanCheck.font = [UIFont systemFontOfSize:15];
    }
    [self refreshCaptchaClicked:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button clicks

- (IBAction)submitClicked:(id)sender
{
    NSString *email = _txtEmail.text;
    if (email.length == 0) {
        kAlert(nil, @"Please Enter your email");
        return;
    }
    if (![email isValidForPattern:patternForEmail]) {
        kAlert(nil, @"Please enter a valid email");
        return;
    }
    if (_txtCaptcha.text.length == 0) {
        kAlert(nil, @"Please enter the captcha");
        return;
    }
    if (![_lblCaptcha.text isEqualToString:_txtCaptcha.text]) {
        kAlert(nil, @"Captcha does not match. Please try again");
        [self refreshCaptchaClicked:nil];
        return;
    }
    [self sendDataToServerWithTask:TASK_RESET_PASSWORD];
}

- (IBAction)refreshCaptchaClicked:(id)sender
{
    int length = 6;
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*";
    NSMutableString *randomString = [NSMutableString stringWithCapacity:length];
    
    for (int i = 0; i < length; i++) {
        [randomString appendFormat: @"%c", [letters characterAtIndex: arc4random() % ([letters length])]];
    }
    _lblCaptcha.text = randomString;
}

#pragma mark - text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (screenSize.height == heightSmallScreen) {
        self.topMargin.constant = (textField == _txtCaptcha) ? -95 : -45;
        [UIView animateWithDuration:0.4
                         animations:^{
                             [self.view layoutIfNeeded]; // Called on parent view
                         }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (screenSize.height == heightSmallScreen) {
        self.topMargin.constant = 0;
        [UIView animateWithDuration:0.4
                         animations:^{
                             [self.view layoutIfNeeded]; // Called on parent view
                         }];
    }
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_RESET_PASSWORD:
        {
            [self showLoaderWithTitle:@""];
            
            [postDict setObject:_txtEmail.text forKey:kUsername];
            [self.requestManager callServiceWithRequestType:TASK_RESET_PASSWORD method:METHOD_POST params:postDict urlEndPoint:@"forget"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_RESET_PASSWORD:
            {
                kAlert(nil, message);
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

@end
