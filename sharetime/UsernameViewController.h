//
//  UsernameViewController.h
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "BaseViewController.h"

@interface UsernameViewController : BaseViewController

@property (strong, nonatomic) UserModel *user;

@end
