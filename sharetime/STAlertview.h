//
//  STAlertview.h
//  sharetime
//
//  Created by iOS Developer on 24/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STAlertview : UIView

- (id)initWithTitle:(NSString*)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString * )cancelButtonTitle otherButtonTitles:(NSArray*)otherButtonTitles;
- (void)show;

@end

@protocol STAlertViewDelegate <NSObject>

- (void)stAlertView:(STAlertview*)stAlertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@optional

- (void)stAlertViewDidCancel:(STAlertview*)stAlertView;

@end

@interface STAlertview ()

@property (strong, nonatomic) id<STAlertViewDelegate> delegate;

@end
