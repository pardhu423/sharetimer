//
//  RegionViewController.m
//  sharetime
//
//  Created by iOS Developer on 24/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

enum {
    TEXT_COUNTRY,
    TEXT_PHONE
};

#import "RegionViewController.h"
#import "CountryPicker.h"
#import "HomeViewController.h"
#import "PrivacyPolicyViewController.h"

@interface RegionViewController ()<CountryPickerDelegate, UITextFieldDelegate> {
    CountryPicker *_countryPicker;
    UIImageView *imageWelcome;
}

@property (weak, nonatomic) IBOutlet UITextField *txtCountry;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
- (IBAction)privacyPolicyButtonAction:(id)sender;

@end

@implementation RegionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationItem.hidesBackButton = YES;
    [self.navigationController addTitle:@"Region"];
    [self addCountryPicker];
    [self styleTextFields];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController addLeftBarButtonWithImageNamed:nil];
}

#pragma mark - back - Overriding
- (IBAction)leftBarButtonClicked:(id)sender
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - text fields

- (void)addCountryPicker
{
    _countryPicker = [[CountryPicker alloc]init];
    _countryPicker.delegate = self;
    _txtCountry.inputView = _countryPicker;
}

- (void)styleTextFields
{
    [_txtCountry addLeftPadding:12];
    [_txtPhone addLeftPadding:12];
    [_txtCountry addToolbarWithTarget:self DoneSelector:@selector(dismissPicker:) nextSelector:@selector(nextClicked:)];
    [_txtPhone addToolbarWithTarget:self DoneSelector:@selector(dismissPicker:)];
}

#pragma mark - country picker delegate

- (void)countryPicker:(CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
{
    _txtCountry.text = name;
    _user.country = name;
}

- (IBAction)dismissPicker:(UIButton*)btnDone
{
    if (btnDone.tag == TEXT_COUNTRY) {
        [self countryPicker:_countryPicker didSelectCountryWithName:_countryPicker.selectedCountryName code:_countryPicker.selectedCountryCode];
    }
    [self.view endEditing:YES];
}

- (IBAction)nextClicked:(id)sender
{
    [_txtPhone becomeFirstResponder];
}

#pragma mark - text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)phoneChanged:(id)sender
{
    _user.phone = _txtPhone.text;
}

#pragma mark - button click

- (IBAction)completeClicked:(id)sender
{
    
    [_txtCountry resignFirstResponder];
    [_txtPhone resignFirstResponder];

    if ([_user isValidRegion]) {
        [self sendDataToServerWithTask:TASK_SIGNUP];
    }
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_SIGNUP:
        {
            [self showLoaderWithTitle:@""];
            
            [postDict setObject:_user.email forKey:kEmail];
            [postDict setObject:_user.username forKey:kUsername];
            [postDict setObject:_user.password forKey:kPassword];
            [postDict setObject:_user.dateOfBirth forKey:kDob];
            
            NSString *gender = [NSString stringWithFormat:@"%d", _user.gender];
            [postDict setObject:gender forKey:kGender];
            [postDict setObject:_user.country forKey:kCountry];
            [postDict setObject:_user.phone forKey:kPhone];
            
            NSString *deviceName = [[UIDevice currentDevice]name];
            NSString *deviceId = [[NSUserDefaults standardUserDefaults]objectForKey:kDeviceUuid];
            NSString *pushId = [[NSUserDefaults standardUserDefaults]objectForKey:kDeviceToken];
            
            if (!deviceId) {
                deviceId = @"simulator";
            }
            if (!pushId) {
                pushId = @"simulator";
            }
            [postDict setObject:deviceName forKey:@"device_name"];
            [postDict setObject:deviceId forKey:@"device_id"];
            [postDict setObject:pushId forKey:@"push_id"];
            [self.requestManager callServiceWithRequestType:TASK_SIGNUP method:METHOD_POST params:postDict urlEndPoint:@"users/register"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_SIGNUP:
            {
                
                self.navigationController.navigationBarHidden = YES;

                NSDictionary *userDict = [responseDict objectForKey:@"user"];
                self.appDelegate.user = [[UserModel alloc]initWithResponse:userDict];
                [self.navigationController setNavigationBarHidden:YES animated:YES];
                imageWelcome = [[UIImageView alloc]init];
                imageWelcome.frame = CGRectMake(0, 0, screenSize.width, screenSize.height);
                imageWelcome.image = [UIImage imageNamed:@"welcomeScreen"];
                [self.view addSubview:imageWelcome];
                [self performSelector:@selector(navigateToHomeScreen) withObject:nil afterDelay:1.0];
            }
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

- (void)navigateToHomeScreen {
    [imageWelcome removeFromSuperview];
    HomeViewController *homeVc = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    [self.navigationController pushViewController:homeVc animated:YES];

}
- (IBAction)privacyPolicyButtonAction:(id)sender {
    
    PrivacyPolicyViewController *blockListVc = [[PrivacyPolicyViewController alloc]initWithNibName:@"PrivacyPolicyViewController" bundle:nil];
    blockListVc.screenTitle = @"Privacy Policy";
    blockListVc.htmlFile = @"Privacy Policy";
    [self.navigationController pushViewController:blockListVc animated:YES];
}
@end
