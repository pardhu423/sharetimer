//
//  MailboxViewController.m
//  sharetime
//
//  Created by iOS Developer on 03/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

// we will show only sent section
enum {
    SECTION_SENT,
    SECTION_RECIEVED
};

#import "MailboxViewController.h"
#import "ProfileViewController.h"
#import "FriendsViewController.h"
#import "HomeViewController.h"

@interface MailboxViewController ()<UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegate> {
    NSMutableArray *_sentToUsers;
    NSMutableArray *_recievedFromUsers;
    NSArray *_filteredSentToUsers;
    NSArray *_filteredReceievedFromUsers;
    UIColor *oldColor;
}

@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UICollectionView *collMailbox;

@end

@implementation MailboxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController addTitle:@"Mailbox"];
    [self.navigationController addLeftBarButtonWithImageNamed:@"paper_blue"];
    //[self.navigationController addRightBarButtonWithImageNamed:@"paper_brown"];
    
    //text search
    [_txtSearch addLeftImageNamed:@"search"];
    [_txtSearch addRightButtonWithImageName:@"search_close" target:self selector:@selector(cancelSearchClicked:) viewMode:UITextFieldViewModeWhileEditing];
    
    [self.collMailbox registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"identifier"];
    [self.collMailbox registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    
    [self.view addHorizontalSwipeRecognizersWithTarget:self leftSwipeSelector:@selector(leftSwipeDetected:) rightSwipeSelector:@selector(rightSwipeDetected:)];
}

- (IBAction)leftBarButtonClicked:(id)sender
{
    NSArray *viewControllers = self.navigationController.viewControllers;
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[HomeViewController class]]) {
            [self.navigationController popToViewController:vc animated:YES];
            break;
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self sendDataToServerWithTask:TASK_MAILBOX];
    [_txtSearch addBottomBorderWithWidth:1 color:[UIColor lightGrayColor]];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    oldColor = self.navigationController.navigationBar.barTintColor;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"3a4347"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = oldColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - gesture

- (IBAction)rightSwipeDetected:(id)sender
{
    FriendsViewController *friendsVc = [[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
    [self.navigationController pushViewController:friendsVc animated:YES];
}

- (IBAction)leftSwipeDetected:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - text field delegate

- (IBAction)cancelSearchClicked:(id)sender
{
    _txtSearch.text = @"";
    [_txtSearch resignFirstResponder];
    [self searchTextChanged:_txtSearch];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)searchTextChanged:(id)sender
{
    NSString *newText = [_txtSearch.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (newText.length) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"SELF.username beginswith[c] %@",
                                  newText];
        _filteredReceievedFromUsers = [_recievedFromUsers filteredArrayUsingPredicate:predicate];
        _filteredSentToUsers = [_sentToUsers filteredArrayUsingPredicate:predicate];
    } else {
        _filteredReceievedFromUsers = _recievedFromUsers;
        _filteredSentToUsers = _sentToUsers;
    }
    [self.collMailbox reloadData];
}

#pragma mark - collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (section) {
        case SECTION_RECIEVED:
            return _filteredReceievedFromUsers.count;
        
        case SECTION_SENT:
            if (!_filteredSentToUsers.count) {
                [_collMailbox reloadData];
            }
            return _filteredSentToUsers.count;
    }
    return 0;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"identifier" forIndexPath:indexPath];
    for (UIView *subview in cell.subviews) {
        [subview removeFromSuperview];
    }
    
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
    [cell addSubview:img];
    
    UserModel *user;
    
    
    NSString *imgName = genders[user.gender];
    img.image = [UIImage imageNamed:imgName];
    
    switch (indexPath.section) {
        case SECTION_RECIEVED:
        {
            user = _filteredReceievedFromUsers[indexPath.row];
            img.backgroundColor = [UIColor cellBrownColor];
            if (!user.mailRead) {
                UIImageView *redDot = [[UIImageView alloc]initWithFrame:CGRectMake(40, 0, 20, 20)];
                redDot.backgroundColor = [UIColor redDotColor];
                [redDot roundCornersWithRadius:10 borderColor:[UIColor clearColor] borderWidth:1];
                [cell addSubview:redDot];
            }
            break;
        }
            
        case SECTION_SENT:
        {
            user = _filteredSentToUsers[indexPath.row];
            img.backgroundColor = user.mailRead ? [UIColor cellGreenColor] : [UIColor cellYellowColor];
            if (user.mailRead == YES) {                   
                img.image = [UIImage imageNamed:@"blue_envelope"];
            }
            break;
        }
    }
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 60, 60, 20)];
    lbl.adjustsFontSizeToFitWidth = YES;
    lbl.text = user.username;
    [cell addSubview:lbl];
    
    return cell;
}

//- (UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
//{
//    UICollectionReusableView *reusableview = nil;
//    
//    if (kind == UICollectionElementKindSectionHeader) {
//        reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
//        
//        for (UIView *subview in reusableview.subviews) {
//            [subview removeFromSuperview];
//        }
//        
//        UILabel *lbl = (UILabel*)[reusableview viewWithTag:1];
//        CGSize size = reusableview.frame.size;
//        lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
//        NSArray *titles = @[@"  RECIEVED", @"  SENT"];
//        lbl.text = titles[indexPath.section];
//        lbl.textColor = [UIColor blueColor];
//        lbl.tag = 1;
//        [reusableview addSubview:lbl];
//    }
//    
//    return reusableview;
//}

#pragma mark - collection view delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_RECIEVED) {
        ProfileViewController *profileVc = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
        profileVc.isMyProfile = NO;
        profileVc.galleryTask = TASK_INBOX_LETTERS_BY_FRIEND;
        profileVc.user = _filteredReceievedFromUsers[indexPath.row];
        [self.navigationController pushViewController:profileVc animated:YES];
    }
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@""];
    NSString *userId = self.appDelegate.user.Id;
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    
    switch (task) {
        case TASK_MAILBOX:
        {
            [postDict setObject:userId forKey:kUserId];
            [self.requestManager callServiceWithRequestType:TASK_MAILBOX method:METHOD_POST params:postDict urlEndPoint:@"mailbox"];
            break;
        }
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_MAILBOX:
            {
                NSDictionary *mailboxDict = [responseDict objectForKey:@"mailbox"];
                NSArray *recievedFromUsers = [mailboxDict objectForKey:@"inbox"];
                _recievedFromUsers = [[NSMutableArray alloc]init];
                for (NSDictionary *userDict in recievedFromUsers) {
                    UserModel *user = [[UserModel alloc]initWithResponse:userDict];
                    
                    //check if there is other user with same id
                    //we want only one entry for a user in this list
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                              @"SELF.Id == %@",
                                              user.Id];
                    NSArray *usersWithSameId = [_recievedFromUsers filteredArrayUsingPredicate:predicate];
                    if (usersWithSameId.count) {
                        //if even one mail from the same user is unread, we mark the user as unread
                        if (!user.mailRead) {
                            UserModel *userWithSameId = usersWithSameId[0];
                            userWithSameId.mailRead = NO;
                        }
                    } else {
                        [_recievedFromUsers addObject:user];
                    }
                }
                _filteredReceievedFromUsers = _recievedFromUsers;
                
                NSArray *sentToUsers = [mailboxDict objectForKey:@"outbox"];
                _sentToUsers = [[NSMutableArray alloc]init];
                for (NSDictionary *userDict in sentToUsers) {
                    UserModel *user = [[UserModel alloc]initWithResponse:userDict];
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                              @"SELF.Id == %@",
                                              user.Id];
                    NSArray *usersWithSameId = [_sentToUsers filteredArrayUsingPredicate:predicate];
                    if (usersWithSameId.count) {
                        //if even one mail from the same user is unread, we mark the user as unread
                        if (!user.mailRead) {
                            UserModel *userWithSameId = usersWithSameId[0];
                            userWithSameId.mailRead = NO;
                        }
                    } else {
                        [_sentToUsers addObject:user];
                    }
                }
                _filteredSentToUsers = _sentToUsers;
                [_collMailbox reloadData];
                break;
            }
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}


@end
