//
//  RegionViewController.h
//  sharetime
//
//  Created by iOS Developer on 24/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "BaseViewController.h"

@interface RegionViewController : BaseViewController

@property (strong, nonatomic) UserModel *user;

@end
