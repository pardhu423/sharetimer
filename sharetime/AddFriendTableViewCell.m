//
//  AddFriendTableViewCell.m
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "AddFriendTableViewCell.h"

@interface AddFriendTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imgFriend;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end

@implementation AddFriendTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)bindUser:(UserModel *)user
{
    NSString *imgName = user.gender == MALE ? @"male" : @"female";
    _imgFriend.image = [UIImage imageNamed:imgName];
    _lblName.text = user.username;
}

@end
