//
//  BaseModel.m
//  Paytm
//
//  Created by iOS Developer on 22/07/15.
//  Copyright (c) 2016 Henote Technologies. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel

- (id)init
{
    self = [super init];
    if (self) {
        self.Id = @"";
    }
    return self;
}

- (id)initWithResponse:(NSDictionary *)response
{
    self = [super init];
    if (self && [response isKindOfClass:[NSMutableDictionary class]]) {
        self.Id = [self checkNil:[response objectForKey:kId]];
    }
    return self;
}

- (NSString*)checkNil:(NSString *)string
{
    if (string) {
        return [NSString stringWithFormat:@"%@", string];
    } else {
        return @"";
    }
}

@end
