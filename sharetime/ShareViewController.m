//
//  ShareViewController.m
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

enum {
    SECTION_BEST_FRIENDS,
    SECTION_GENERAL
};

#import "ShareViewController.h"
#import "MailboxViewController.h"
#import "ProfileViewController.h"

@interface ShareViewController ()<UITextFieldDelegate, UITabBarControllerDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {
    NSMutableArray *_bestFriends;
    NSMutableArray *_friends;
    NSArray *_filteredBestFriends;
    NSArray *_filteredFriends;
    NSInteger countSelected;
}

@property (weak, nonatomic) IBOutlet UIView *viewMyLetter;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedCount;
@property (weak, nonatomic) IBOutlet UICollectionView *collFriends;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckbox;
@property (weak, nonatomic) IBOutlet UIButton *btnMailbox;
@property (weak, nonatomic) IBOutlet UIButton *btnArrow;

@end

@implementation ShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    float screenWidth = screenSize.width;
    float scaleX = screenWidth/320.0;
    [_txtSearch addLeftPadding:55 * scaleX];
    [_txtSearch addRightPadding:15];
    [self.collFriends registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"identifier"];
    [self.collFriends registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    [self styleNavBar];
    
    [_txtSearch addLeftImageNamed:@"search"];
    [_txtSearch addRightButtonWithImageName:@"search_close" target:self selector:@selector(searchCancelled:) viewMode:UITextFieldViewModeWhileEditing];
    
    [self sendDataToServerWithTask:TASK_GET_FRIENDS_TO_SHARE];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_txtSearch addBottomBorderWithWidth:1 color:[UIColor lightGrayColor]];
    [self.viewMyLetter addBottomBorderWithWidth:1 color:[UIColor lightGrayColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)styleNavBar
{
    [self.navigationController addTitle:@"Share With..."];
    [self.navigationController addLeftBarButtonWithImageNamed:@"back_arrow"];
}

#pragma mark - text field delegate

- (IBAction)searchCancelled:(id)sender
{
    _txtSearch.text = @"";
    [_txtSearch resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)searchTextChanged:(id)sender
{
    NSString *newText = [_txtSearch.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (newText.length) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"SELF.username beginswith[c] %@",
                                  newText];
        _filteredBestFriends = [_bestFriends filteredArrayUsingPredicate:predicate];
        _filteredFriends = [_friends filteredArrayUsingPredicate:predicate];
    } else {
        _filteredBestFriends = _bestFriends;
        _filteredFriends = _friends;
    }
    [self reloadFriends];
}

#pragma mark - collection view delegate flow layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int width = collectionView.frame.size.width/4;
    int height = width + 25;
    return CGSizeMake(width, height);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section == SECTION_BEST_FRIENDS) {
        return CGSizeMake(collectionView.frame.size.width, 50);
    } else {
        return CGSizeZero;
    }
}

#pragma mark - collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (section) {
        case SECTION_BEST_FRIENDS:
            return _filteredBestFriends.count;
            
        case SECTION_GENERAL:
            return _filteredFriends.count;
    }
    return 0;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"identifier" forIndexPath:indexPath];
    for (UIView *subview in cell.subviews) {
        [subview removeFromSuperview];
    }
    
    int width = collectionView.frame.size.width/4;
    int cellMargin = 10;
    int startY = 5;
    int imgWidth = width - cellMargin;
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(cellMargin, startY, imgWidth, imgWidth)];
    img.backgroundColor = indexPath.section == SECTION_GENERAL ? [UIColor cellBrownColor] : [UIColor cellPinkColor];
    [cell addSubview:img];
    
    UserModel *user = [self userAtIndexPath:indexPath];
    
    NSString *imgName = genders[user.gender];
    img.image = [UIImage imageNamed:imgName];
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(cellMargin, imgWidth + startY, imgWidth, 20)];
    lbl.text = user.username;
    lbl.adjustsFontSizeToFitWidth = YES;
    [cell addSubview:lbl];
    
    //add red dot
    if (user.selected) {
        int margin = 3;
        UIImageView *redDot = [[UIImageView alloc]initWithFrame:CGRectMake(width - margin - 20, startY + margin, 20, 20)];
        redDot.backgroundColor = [UIColor redDotColor];
        [redDot roundCornersWithRadius:10 borderColor:[UIColor clearColor] borderWidth:1];
        [cell addSubview:redDot];
    }
    [cell addBottomShadeWithWidth:1 color:[UIColor lightGrayColor]];
    return cell;
}

- (UserModel*)userAtIndexPath:(NSIndexPath*)indexPath
{
    return indexPath.section == SECTION_BEST_FRIENDS ? _filteredBestFriends[indexPath.row] : _filteredFriends[indexPath.row];
}

- (UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
    
    if (kind == UICollectionElementKindSectionHeader && indexPath.section == SECTION_BEST_FRIENDS) {
        
        UILabel *lbl = (UILabel*)[reusableview viewWithTag:1];
        if (!lbl) {
            CGSize size = reusableview.frame.size;
            lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
            NSArray *titles = @[@"  BEST FRIENDS"];
            lbl.text = titles[indexPath.section];
            lbl.textColor = [UIColor colorWithHexString:@"#bb3a9f"];
            lbl.tag = 1;
            [reusableview addSubview:lbl];
        }
    }
    
    return reusableview;
}

#pragma mark - collection view delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //select or deselect the item. toggle
    UserModel *user = [self userAtIndexPath:indexPath];

    user.selected = !user.selected;
    
    [self reloadFriends];
}

- (NSMutableArray*)selectedFriends
{
    NSMutableArray *selectedFriends = [[NSMutableArray alloc]init];
    for (UserModel *user in _filteredBestFriends) {
        if (user.selected) {
            [selectedFriends addObject:user];
        }
    }
    for (UserModel *user in _filteredFriends) {
        if (user.selected) {
            [selectedFriends addObject:user];
        }
    }
    return selectedFriends;
}

- (void)reloadFriends
{
    NSMutableArray *selectedFriends = [self selectedFriends];
    NSInteger selectedCount = selectedFriends.count;
    countSelected = selectedCount;
    
    if (selectedCount > 0 && _btnMailbox.selected == YES) {
        _btnArrow.hidden = NO;
        _lblSelectedCount.hidden = NO;
        _lblSelectedCount.text = [NSString stringWithFormat:@"My Letter, %lu", (long)selectedCount];
    } else if (selectedCount == 0 && _btnMailbox.selected == YES){
        _btnArrow.hidden = NO;
        _lblSelectedCount.hidden = NO;
       _lblSelectedCount.text = @"My Letter";
    } else if (selectedCount > 0 && _btnMailbox.selected == NO) {
        _btnArrow.hidden = NO;
        _lblSelectedCount.hidden = NO;
        _lblSelectedCount.text = [NSString stringWithFormat:@"%lu", (long)selectedCount];
    } else {
        _btnArrow.hidden = YES;
        _lblSelectedCount.hidden = YES;
    }
    [_collFriends reloadData];
}

#pragma mark - table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 27;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"id"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"id"];
    } else {
        for (UIView *subview in cell.subviews) {
            [subview removeFromSuperview];
        }
    }
    char c = 96 + indexPath.row;
    if (c == 96) {
        c = '#';
    }
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    lbl.text = [NSString stringWithFormat:@"%c", c];
    lbl.textAlignment = NSTextAlignmentCenter;
    [cell addSubview:lbl];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    //convert alphabet row to card section
    NSIndexPath *indexPathCardSection = [NSIndexPath indexPathForItem:0 inSection:SECTION_GENERAL];
    NSArray *friends = [self sortedFriendsForRow:indexPath.row];
    if (friends.count) {
        [self.collFriends scrollToItemAtIndexPath:indexPathCardSection atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
    }
}

#pragma mark - predicate

- (NSString*)alphabetForRow:(NSInteger)row
{
    NSString *text = @"#";
    if (row > 0) {
        char alphabet = row + 64;
        text = [NSString stringWithFormat:@"%c", alphabet];
    }
    return text;
}

- (NSArray*)sortedFriendsForRow:(NSInteger)row
{
    NSString *alphabet = [self alphabetForRow:row];
    
    if ([alphabet isEqualToString:@"#"]) {
        //names beginning with a number '#' 0-9
        NSMutableArray *numberCards = [NSMutableArray array];
        for (UserModel *user in _filteredFriends) {
            NSString *numberPattern = @"^\\d+";
            if ([user.username isValidForPattern:numberPattern]) {
                [numberCards addObject:user];
            }
        }
        return numberCards;
    }
    
    //A-Z
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"SELF.username beginswith[c] %@",
                              alphabet];
    NSArray *friends = [_filteredFriends filteredArrayUsingPredicate:predicate];
    return friends;
}

#pragma mark - button click

-(IBAction)checkboxClicked:(id)sender
{
    _btnCheckbox.selected = !_btnCheckbox.selected;
    [self selectFriends:_btnCheckbox.selected];
    [self reloadFriends];
}

- (void)selectFriends:(BOOL)selected
{
    for (UserModel *user in _bestFriends) {
        user.selected = selected;
    }
    for (UserModel *user in _friends) {
        user.selected = selected;
    }
}

- (NSMutableArray*)allIndexPaths
{
    NSMutableArray *selectedPaths = [NSMutableArray array];
    NSInteger nSections = _collFriends.numberOfSections;
    for (int j = 0; j < nSections; j++) {
        NSInteger nRows = [_collFriends numberOfItemsInSection:j];
        for (int i = 0; i < nRows; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:j];
            [selectedPaths addObject:indexPath];
        }
    }
    return selectedPaths;
}

- (IBAction)arrowClicked:(id)sender
{
    [self sendDataToServerWithTask:TASK_SHARE_LETTER];
}

- (IBAction)mailboxClicked:(id)sender
{
    _btnMailbox.selected = !_btnMailbox.selected;
    
//    if (_btnMailbox.selected == YES) {
//        _btnArrow.hidden = NO;
//        _lblSelectedCount.hidden = NO;
//    } else if (_btnMailbox.selected == NO && countSelected > 0){
//        _btnArrow.hidden = NO;
//        _lblSelectedCount.hidden = NO;
//    } else {
//        _btnArrow.hidden = YES;
//        _lblSelectedCount.hidden = YES;
//    }
    
    
    if (countSelected > 0 && _btnMailbox.selected == YES) {
        _btnArrow.hidden = NO;
        _lblSelectedCount.hidden = NO;
        _lblSelectedCount.text = [NSString stringWithFormat:@"My Letter, %lu", (long)countSelected];
    } else if (countSelected == 0 && _btnMailbox.selected == YES){
        _btnArrow.hidden = NO;
        _lblSelectedCount.hidden = NO;
        _lblSelectedCount.text = @"My Letter";
    } else if (countSelected > 0 && _btnMailbox.selected == NO) {
        _btnArrow.hidden = NO;
        _lblSelectedCount.hidden = NO;
        _lblSelectedCount.text = [NSString stringWithFormat:@"%lu", (long)countSelected];
    } else {
        _btnArrow.hidden = YES;
        _lblSelectedCount.hidden = YES;
    }
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@""];
    NSString *userId = self.appDelegate.user.Id;
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    
    switch (task) {
        case TASK_SHARE_LETTER:
        {
            NSString *shareStatus = _btnMailbox.selected ? @"PUBLIC" : @"PRIVATE";
            [postDict setObject:shareStatus forKey:@"showstatus"];
            
            [postDict setObject:_letterId forKey:@"shareId"];
            [postDict setObject:userId forKey:kUserId];
            
            NSMutableArray *friendIds = [[NSMutableArray alloc]init];
            NSMutableArray *selectedFriends = [self selectedFriends];
            for (UserModel *user in selectedFriends) {
                [friendIds addObject:user.Id];
            }
            [postDict setObject:friendIds forKey:@"friendId"];
            [self.requestManager callServiceWithRequestType:TASK_SHARE_LETTER method:METHOD_POST params:postDict urlEndPoint:@"sharewithfriend"];
            break;
        }
            
        case TASK_GET_FRIENDS_TO_SHARE:
        {
            [postDict setObject:userId forKey:kUserId];
            [self.requestManager callServiceWithRequestType:TASK_GET_FRIENDS_TO_SHARE method:METHOD_POST params:postDict urlEndPoint:@"friendlistforshare"];
            break;
        }
        
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_GET_FRIENDS_TO_SHARE:
            {
                NSDictionary *friendsDict = [responseDict objectForKey:@"friends"];
                NSArray *bestFriends = [friendsDict objectForKey:@"best_friends"];
                _bestFriends = [[NSMutableArray alloc]init];
                for (NSDictionary *userDict in bestFriends) {
                    UserModel *user = [[UserModel alloc]initWithResponse:userDict];
                    [_bestFriends addObject:user];
                }
                _filteredBestFriends = _bestFriends;
                
                NSArray *friends = [friendsDict objectForKey:@"friends"];
                _friends = [[NSMutableArray alloc]init];
                for (NSDictionary *userDict in friends) {
                    UserModel *user = [[UserModel alloc]initWithResponse:userDict];
                    [_friends addObject:user];
                }
                _filteredFriends = _friends;
                [self reloadFriends];
                break;
            }
                
            case TASK_SHARE_LETTER:
            {
                //go to mailbox screen after letter is shared
                NSMutableArray *selectedFriends = [self selectedFriends];
                if (selectedFriends.count) {
                    MailboxViewController *mailboxVc = [[MailboxViewController alloc]initWithNibName:@"MailboxViewController" bundle:nil];
                    [self.navigationController pushViewController:mailboxVc animated:YES];
                } else {
                    ProfileViewController *profileVc = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
                    profileVc.isMyProfile = YES;
                    profileVc.galleryTask = TASK_MY_GALLERY;
                    [self.navigationController pushViewController:profileVc animated:YES];
                }
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

@end
