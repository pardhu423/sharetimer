//
//  SupportBlogViewController.m
//  sharetime
//
//  Created by iOS Developer on 11/12/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

enum {
    ROW_GETTING_STARTED,
    ROW_SELFIES_AND_VIDEO,
    ROW_LETTERS,
    ROW_FILTERS,
    ROW_VIEW_LETTERS_AND_VIDEOS,
    ROW_FRIENDS,
    ROW_DELETE_AN_ACCOUNT
};

#import "SupportBlogViewController.h"
#import "FeedbackFormViewController.h"
#import "SettingsTableViewCell.h"
#import "PopularTopicsViewController.h"

@interface SupportBlogViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgArrowFeedback;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrowRequest;
@property (weak, nonatomic) IBOutlet UIView *viewFeedback;
@property (weak, nonatomic) IBOutlet UIView *viewRequests;

@end

@implementation SupportBlogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController addTitle:@"Support"];
    
    //rotate arrows
    _imgArrowFeedback.transform = CGAffineTransformMakeRotation(3.14);
    _imgArrowRequest.transform = CGAffineTransformMakeRotation(3.14);
    
    //add tap recognizer
    [_viewFeedback addTapRecognizerWithTarget:self selector:@selector(feedbackClicked:)];
    [_viewRequests addTapRecognizerWithTarget:self selector:@selector(feedbackClicked:)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - actions

- (IBAction)feedbackClicked:(id)sender
{
    FeedbackFormViewController *feedbackFormVc = [[FeedbackFormViewController alloc]initWithNibName:@"FeedbackFormViewController" bundle:nil];
    [self.navigationController pushViewController:feedbackFormVc animated:YES];
}

#pragma mark - table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"SettingsTableViewCell";
    NSArray *titles = @[@"Getting Started", @"Selfies and Video", @"Letters", @"Filters", @"View Letters and Videos", @"Friends", @"Delete an Account"];
    SettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:identifier owner:self options:nil] lastObject];
    }
    
    NSString *title = titles[indexPath.row];
    cell.lbl.text = title;
    
    return cell;
}

#pragma mark - table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *screens = @[@"GettingStartedViewController", @"TakeSelfiesViewController", @"SharingLettersViewcontroller", @"FiltersViewController", @"ViewLettersViewController", @"FriendInfoViewController", @"DeleteProfileViewController"];
    NSString *nibName = screens[indexPath.row];
    PopularTopicsViewController *popularTopicVc = [[PopularTopicsViewController alloc]initWithNibName:nibName bundle:nil];
    [self.navigationController pushViewController:popularTopicVc animated:YES];
}


@end
