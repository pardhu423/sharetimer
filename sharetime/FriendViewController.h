//
//  FriendViewController.h
//  sharetime
//
//  Created by iOS Developer on 03/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "BaseViewController.h"
#import "LetterModel.h"

@interface FriendViewController : BaseViewController

@property (strong, nonatomic) NSMutableArray *letters;
@property (nonatomic) NSInteger selectedLetterIndex;
@property (nonatomic, strong) NSString *stringUserName;
@property (strong, nonatomic) LetterModel *letter;
@property (nonatomic) BOOL isMyProfile, isFromLetter;
@property (strong, nonatomic) NSString *rowId;

@end
