//
//  SignupViewController.m
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#define strTermsOfUse @"Terms of Use"
#define strPrivacyPolicy @"Privacy policy"

#import "SignupViewController.h"
#import "UsernameViewController.h"
#import "PrivacyPolicyViewController.h"

@interface SignupViewController ()<UITextFieldDelegate> {
    UIDatePicker *_datePicker;
    UserModel *_user;
    
}

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtBirthday;
@property (weak, nonatomic) IBOutlet UIButton *btnMale;
@property (weak, nonatomic) IBOutlet UIButton *btnFemale;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;
@property (weak, nonatomic) IBOutlet UITextView *txtAgreeTos;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _user = [[UserModel alloc]init];
    [self styleNavBar];
    [self styleTextFields];
    [self colorTextInTextView];
    [self.txtAgreeTos addTapRecognizerWithTarget:self selector:@selector(byAgreeingClicked:)];
    
    self.genderLabel.font = [UIFont appFontWithSize:23];
   

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithHexString:@"#3a4347"]];
    [[UINavigationBar appearance] setTranslucent:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI

- (void)styleNavBar
{
    [self.navigationController addLeftBarButtonWithImageNamed:@"back_arrow"];
    [self.navigationController addTitle:@"Sign Up" color:[UIColor redColor]];
}

- (void)styleTextFields
{
    int padding = 12;
    [_txtEmail addLeftPadding:padding];
    [_txtNewPassword addLeftPadding:padding];
    [_txtBirthday addLeftPadding:padding];
    [_txtBirthday addRightImageWithImageName:@"gift"];
    [_txtBirthday addToolbarWithTarget:self DoneSelector:@selector(doneClicked:)];
    [self addDatePicker];
}

- (void)addDatePicker
{
    _datePicker = [[UIDatePicker alloc]init];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _datePicker.maximumDate = [[NSDate date] dateByAddingTimeInterval: -365 * 24 * 3600];
    _txtBirthday.inputView = _datePicker;
    [_datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)colorTextInTextView
{
    NSString *text = _txtAgreeTos.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:text];
    
    //blue color for terms of use and privacy policy
    UIColor *blueColor = [UIColor colorWithHexString:@"#238dff"];
    NSRange rangeTos = [text rangeOfString:strTermsOfUse];
    [attributedString addAttribute:NSForegroundColorAttributeName value:blueColor range:rangeTos];
    NSRange rangePrivacy = [text rangeOfString:strPrivacyPolicy];
    [attributedString addAttribute:NSForegroundColorAttributeName value:blueColor range:rangePrivacy];
    
    //font
    NSRange completeRange = NSMakeRange(0, _txtAgreeTos.text.length);
    [attributedString addAttribute:NSFontAttributeName value:_txtAgreeTos.font range:completeRange];
    _txtAgreeTos.attributedText = attributedString;
}

#pragma mark - button click

- (IBAction)maleClicked:(id)sender
{
    _btnFemale.selected = NO;
    _btnMale.selected = YES;
    _user.gender = MALE;
}

- (IBAction)femaleClicked:(id)sender
{
    _btnFemale.selected = YES;
    _btnMale.selected = NO;
    _user.gender = FEMALE;
}

- (IBAction)continueClicked:(id)sender
{
    if ([_user isValidSignup]) {
        [self sendDataToServerWithTask:TASK_CHECK_EMAIL];
    }
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_CHECK_EMAIL:
        {
            [self showLoaderWithTitle:@""];
            [postDict setObject:_user.email forKey:kEmail];
            [self.requestManager callServiceWithRequestType:TASK_CHECK_EMAIL method:METHOD_POST params:postDict urlEndPoint:@"users/validateEmail"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_CHECK_EMAIL:
            {
                UsernameViewController *usernameVc = [[UsernameViewController alloc]initWithNibName:@"UsernameViewController" bundle:nil];
                usernameVc.user = _user;
                [self.navigationController pushViewController:usernameVc animated:YES];
            }
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

- (IBAction)dateChanged:(id)sender
{
    [self showSelectedDate];
}

- (IBAction)doneClicked:(id)sender
{
    [self showSelectedDate];
    [_txtBirthday resignFirstResponder];
}

- (void)showSelectedDate
{
    NSDate *date = _datePicker.date;
    NSString *birthday = [date stringWithFormat:formatForHyphenDate];
    _txtBirthday.text = birthday;
    _user.dateOfBirth = [date stringWithFormat:formatForServerDate];
}

#pragma mark - text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)emailChanged:(id)sender
{
    _user.email = _txtEmail.text;
}

- (IBAction)passwordChanged:(id)sender
{
    _user.password = _txtNewPassword.text;
}

#pragma mark - gesture

- (IBAction)byAgreeingClicked:(UITapGestureRecognizer*)recognizer
{
    //get view
    UITextView *textView = (UITextView *)recognizer.view;
    //get location
    CGPoint location = [recognizer locationInView:textView];
    UITextPosition *tapPosition = [textView closestPositionToPoint:location];
    NSString *text = _txtAgreeTos.text;
    NSInteger index = [_txtAgreeTos offsetFromPosition:_txtAgreeTos.beginningOfDocument toPosition:tapPosition] - 1;
    
    NSRange rangeTos = [text rangeOfString:strTermsOfUse];
    if (NSLocationInRange(index, rangeTos)) {
//        kAlert(nil, @"terms of use clicked");
        
        
        PrivacyPolicyViewController *blockListVc = [[PrivacyPolicyViewController alloc]initWithNibName:@"PrivacyPolicyViewController" bundle:nil];
        blockListVc.screenTitle = @"Terms and conditions";
        blockListVc.htmlFile = @"Terms and conds";
        [self.navigationController pushViewController:blockListVc animated:YES];
        
    }
    
    NSRange rangePrivacy = [text rangeOfString:strPrivacyPolicy];
    if (NSLocationInRange(index, rangePrivacy)) {
//        kAlert(nil, @"privacy policy clicked");
        
        PrivacyPolicyViewController *blockListVc = [[PrivacyPolicyViewController alloc]initWithNibName:@"PrivacyPolicyViewController" bundle:nil];
        blockListVc.screenTitle = @"Privacy Policy";
        blockListVc.htmlFile = @"Privacy Policy";
        [self.navigationController pushViewController:blockListVc animated:YES];

    }
}

@end
