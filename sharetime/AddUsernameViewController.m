//
//  AddUsernameViewController.m
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "AddUsernameViewController.h"
#import "AddFriendTableViewCell.h"

@interface AddUsernameViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate> {
    NSMutableArray *_users;
    NSArray *_filteredUsers;
    NSMutableArray *_requestSentFriendIds;
    UIColor *oldColor;
}

@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UITableView *tblUsers;
@property (nonatomic,assign) NSInteger selectedIndex;

@end

@implementation AddUsernameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController addLeftBarButtonWithImageNamed:@"back_arrow"];
    [self.navigationController addTitle:@"Add Username"];
    _requestSentFriendIds = [[NSMutableArray alloc]init];
    
    //style search text field
    [_txtSearch addLeftImageNamed:@"search"];
    [_txtSearch addRightButtonWithImageName:@"search_close" target:self selector:@selector(cancelSearchClicked:) viewMode:UITextFieldViewModeWhileEditing];
    
    [self sendDataToServerWithTask:TASK_GET_USER_LIST];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_txtSearch addBottomBorderWithWidth:1 color:[UIColor lightGrayColor]];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    oldColor = self.navigationController.navigationBar.barTintColor;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"3a4347"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = oldColor;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _filteredUsers.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"AddFriendTableViewCell";
    AddFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:identifier owner:self options:nil] lastObject];
    }
    UserModel *user = _filteredUsers[indexPath.row];
    [cell bindUser:user];
    
    cell.btnPlus.selected = [_requestSentFriendIds containsObject:user.Id];
    [cell.btnPlus addTarget:self action:@selector(addFriend:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnPlus.tag = indexPath.row;
    return cell;
}

#pragma mark - text field delegate

- (IBAction)cancelSearchClicked:(id)sender
{
    _txtSearch.text = @"";
    [_txtSearch resignFirstResponder];
    [self searchTextChanged:_txtSearch];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)searchTextChanged:(id)sender
{
    NSString *newText = [_txtSearch.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (newText.length) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"SELF.username beginswith[c] %@",
                                  newText];
        _filteredUsers = [_users filteredArrayUsingPredicate:predicate];
    } else {
        _filteredUsers = _users;
    }
    [self.tblUsers reloadData];
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_GET_USER_LIST:
        {
            [self showLoaderWithTitle:@""];
            NSString *userId = self.appDelegate.user.Id;
            [postDict setObject:userId forKey:kUserId];
            
            [self.requestManager callServiceWithRequestType:TASK_GET_USER_LIST method:METHOD_POST params:postDict urlEndPoint:@"users/peoplelist"];
            break;
        }
            
        default:
            break;
    }
}

- (IBAction)addFriend:(UIButton*)btnAdd
{
    [self showLoaderWithTitle:@""];
    
    self.selectedIndex = btnAdd.tag;
    UserModel* user = [_filteredUsers objectAtIndex:btnAdd.tag];
    NSString *friendId = [NSString stringWithFormat:@"%@", user.Id];
    NSString *userId = self.appDelegate.user.Id;
    
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setObject:friendId forKey:kFriendId];
    [postDict setObject:userId forKey:kUserId];
    
    NSString *action = btnAdd.selected ? @"0" : @"1";
    [postDict setObject:action forKey:@"action"];
    [self.requestManager callServiceWithRequestType:TASK_ADD_FRIEND method:METHOD_POST params:postDict urlEndPoint:@"sendFriendRequest"];
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_GET_USER_LIST:
            {
                _users = [[NSMutableArray alloc]init];
                NSArray *users = [responseDict objectForKey:@"people"];
                for (NSDictionary *userDict in users) {
                    UserModel *user = [[UserModel alloc]initWithResponse:userDict];
                    [_users addObject:user];
                }
                _filteredUsers = _users;
                [self.tblUsers reloadData];
                break;
            }
                
            case TASK_ADD_FRIEND:
            {
                
                UserModel* user = nil;
                
                if (self.selectedIndex < _filteredUsers.count) {
                    user = [_filteredUsers objectAtIndex:self.selectedIndex];
                }
                NSString *friendId = user.Id;
                if ([_requestSentFriendIds containsObject:friendId]) {
                    [_requestSentFriendIds removeObject:friendId];
                } else {
                    [_requestSentFriendIds addObject:friendId];
                }
                
                [self.tblUsers reloadData];
                
                kAlert(nil, message);
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    if (tag == TASK_ADD_FRIEND) {
        kAlert(nil, error.localizedDescription);
        return;
    }
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

@end
