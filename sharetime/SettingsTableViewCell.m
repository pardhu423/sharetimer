//
//  SettingsTableViewCell.m
//  sharetime
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "SettingsTableViewCell.h"
#import "UIColor+Addition.h"

@interface SettingsTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;

@end

@implementation SettingsTableViewCell

- (void)awakeFromNib {
    // Initialization code
    _imgArrow.transform = CGAffineTransformMakeRotation(3.14);
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)addBlueStrip
{
    //add blue strip
    int width = self.frame.size.width;
    int height = self.frame.size.height;
    int stripWidth = 2;
    UIView *bottomBorderView = [[UIView alloc]initWithFrame:CGRectMake(0, height - stripWidth, width, stripWidth)];
    bottomBorderView.backgroundColor = [UIColor navigationBlueColor];
    [self addSubview:bottomBorderView];
}

@end
