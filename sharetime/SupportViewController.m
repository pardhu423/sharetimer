//
//  SupportViewController.m
//  sharetime
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

enum {
    SECTION_POPULAR_TOPICS,
    SECTION_GENERAL_TOPICS,
    SECTION_ISSUES
};

enum {
    ROW_RESET_PASSWORD,
    ROW_CHANGE_PASSWORD,
    ROW_BLOCK_FRIENDS,
    ROW_CHANGE_USERNAME,
    ROW_DELETE_LETTERS
};

enum {
    ROW_LEARN_ABOUT_SHARETIMER,
    ROW_SHARETIMER_COMMUNITY,
    ROW_SHARE_PHOTOS,
    ROW_REQUESTS,
    ROW_CAN_I_VIEW_LETTER
};

enum {
    ROW_CANT_RECORD,
    ROW_CANT_UPLOAD_PHOTO,
    ROW_UPLOAD_VIDEO,
    ROW_CANT_SELECT_FRIENDS
};

#import "SupportViewController.h"
#import "SettingsTableViewCell.h"
#import "SupportBlogViewController.h"
#import "PopularTopicsViewController.h"

@interface SupportViewController ()<UITableViewDataSource, UITableViewDelegate> {
    NSArray *_titles;
}

@end

@implementation SupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initializeTitles];
    [self.navigationController addTitle:@"Support"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initializeTitles
{
    NSArray *popularTopics = @[@"How do I reset my password?", @"How do I change my password?", @"How do I block/delete friends?", @"How do I change my username?", @"How do I delete my letters?"];
    NSArray *generalTopics = @[@"Learn about Sharetimer", @"Sharetimer Community", @"Share photos and videos", @"Requests and Feedback", @"Can I view my letter after one week?"];
    NSArray *issues = @[@"I can't record or upload", @"I can't upload a photo", @"I can't upload a video", @"I can't select all my friends"];
    _titles = @[popularTopics, generalTopics, issues];
}

#pragma mark - table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _titles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *list = _titles[section];
    return list.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"SettingsTableViewCell";
    SettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:identifier owner:self options:nil] lastObject];
    }
    
    NSArray *sectionTitles = _titles[indexPath.section];
    NSString *title = sectionTitles[indexPath.row];
    cell.lbl.text = title;
    
    return cell;
}

#pragma mark - table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *nibName;
    switch (indexPath.section) {
        case SECTION_POPULAR_TOPICS:
        {
            switch (indexPath.row) {
                case ROW_RESET_PASSWORD:
                    nibName = @"PopularTopicsViewController";
                    break;
                    
                case ROW_CHANGE_PASSWORD:
                    nibName = @"ChangePasswordViewController";
                    break;
                    
                case ROW_BLOCK_FRIENDS:
                    nibName = @"BlockFriendViewController";
                    break;
                    
                case ROW_CHANGE_USERNAME:
                    nibName = @"ChangeYourUsernameViewController";
                    break;
                    
                case ROW_DELETE_LETTERS:
                    nibName = @"DeleteLettersViewController";
                    break;
            }
            
            PopularTopicsViewController *popularVc = [[PopularTopicsViewController alloc]initWithNibName:nibName bundle:nil];
            [self.navigationController pushViewController:popularVc animated:YES];
            return;
        }
            
        case SECTION_GENERAL_TOPICS:
        {
            switch (indexPath.row) {
                case ROW_LEARN_ABOUT_SHARETIMER:
                    nibName = @"TipsViewController";
                    break;
                    
                case ROW_SHARETIMER_COMMUNITY:
                    nibName = @"CommunityViewController";
                    break;
                    
                case ROW_SHARE_PHOTOS:
                    nibName = @"ShareLettersViewController";
                    break;
                    
                case ROW_REQUESTS:
                    nibName = @"FeedbackViewController";
                    break;
                
                case ROW_CAN_I_VIEW_LETTER:
                    nibName = @"SupportLettersViewController";
                    break;
            }
            break;
        }
            
        case SECTION_ISSUES:
        {
            nibName = @"SupportBlogViewController";
            break;
        }
    }
    
    if (nibName) {
        SupportBlogViewController *supportVc = [[SupportBlogViewController alloc]initWithNibName:nibName bundle:nil];
        [self.navigationController pushViewController:supportVc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, screenSize.width, 44)];
    lbl.backgroundColor = [UIColor navigationBlueColor];
    NSArray *headers = @[@" POPULAR TOPICS", @" GENERAL TOPICS", @" ISSUES"];
    NSString *header = headers[section];
    lbl.text = header;
    lbl.font = [UIFont systemFontOfSize:18];
    
    return lbl;
}

@end
