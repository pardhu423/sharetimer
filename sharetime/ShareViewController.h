//
//  ShareViewController.h
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "BaseViewController.h"
#import "LetterModel.h"

@interface ShareViewController : BaseViewController

@property (strong, nonatomic) NSString *letterId;

@end
