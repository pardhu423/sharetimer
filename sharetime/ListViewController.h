//
//  ListViewController.h
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

enum {
    LIST_VIEWS,
    LIST_SCREENSHOT
};

#import "BaseViewController.h"

@interface ListViewController : BaseViewController

@property (nonatomic) int listType;
@property (strong, nonatomic) NSString *letterId;

@end
