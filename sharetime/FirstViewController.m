//
//  FirstViewController.m
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "FirstViewController.h"
#import "SignupViewController.h"
#import "LoginViewController.h"

@interface FirstViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightSignUp;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLogin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginSignupMargin;

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSString *imgName = (screenSize.height == heightSmallScreen) ? @"login_bg2" : @"login_bg";
    _imgBg.image = [UIImage imageNamed:imgName];
    
    //change click area for different screens
    int height = 155 * screenSize.width/320.0;
    _heightSignUp.constant = height;
    _heightLogin.constant = height;
    _loginSignupMargin.constant = 25 * screenSize.width/320.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button clicks

- (IBAction)loginClicked:(id)sender
{
    LoginViewController *loginVc = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
    [self.navigationController pushViewController:loginVc animated:YES];
}

- (IBAction)signupClicked:(id)sender
{
    SignupViewController *signupVc = [[SignupViewController alloc]initWithNibName:@"SignupViewController" bundle:nil];
    [self.navigationController pushViewController:signupVc animated:YES];
}

@end
