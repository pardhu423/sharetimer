//
//  ManageViewController.m
//  sharetime
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

enum {
    FRONT_FACING_FLASH,
    SOUND,
    LOOP
};

#import "ManageViewController.h"
#import "SettingsTableViewCell.h"

@interface ManageViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UISwitch *switchFilters;

@end

@implementation ManageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController addTitle:@"MANAGE"];
    self.switchFilters.on = ![[NSUserDefaults standardUserDefaults]boolForKey:kFiltersOff];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *nibName = @"SettingsSwitchCell";
    SettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nibName];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:nibName owner:self options:nil] lastObject];
    }
    
    NSArray *titles = @[@"Front Facing Flash", @"Sound", @"Loop"];
    NSString *title = titles[indexPath.row];
    cell.lbl.text = title;
    
    NSArray *userDefaultKeys = @[kBackFacingFlash, kMute, kNoLoop];
    NSString *userDefaultKey = userDefaultKeys[indexPath.row];
    cell.switchOnOff.on = ![[NSUserDefaults standardUserDefaults]boolForKey:userDefaultKey];
    cell.switchOnOff.tag = indexPath.row;
    [cell.switchOnOff addTarget:self action:@selector(switchToggled:) forControlEvents:UIControlEventValueChanged];
    
    return cell;
}

#pragma mark - switch action

- (IBAction)filtersToggled:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setBool:!_switchFilters.on forKey:kFiltersOff];
}

- (IBAction)switchToggled:(UISwitch*)onOffSwitch
{
    NSArray *userDefaultKeys = @[kBackFacingFlash, kMute, kNoLoop];
    NSString *key = userDefaultKeys[onOffSwitch.tag];
    BOOL off = !onOffSwitch.on;
    [[NSUserDefaults standardUserDefaults]setBool:off forKey:key];
}

@end
