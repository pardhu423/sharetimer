//
//  UIFont+OurFont.m
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#define appFontName @"HomespunTTBRK"

#import "UIFont+OurFont.h"

@implementation UIFont (OurFont)

+ (UIFont*)appFontWithSize:(CGFloat)size
{
    UIFont *font = [self fontWithName:appFontName size:size];
    return font;
}

+ (void)setAppFontForButton:(UIButton *)button
{
    UIFont *font = [self fontWithName:appFontName size:button.titleLabel.font.pointSize];
    button.titleLabel.font = font;
}

@end
