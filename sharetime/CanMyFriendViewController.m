//
//  CanMyFriendViewController.m
//  sharetime
//
//  Created by iOS Developer on 10/12/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "CanMyFriendViewController.h"
#import "PrivacyPolicyViewController.h"

@interface CanMyFriendViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *screenshotSwitch;

@end

@implementation CanMyFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController addTitle:@"Can My Friends..."];
    _screenshotSwitch.on = ![[NSUserDefaults standardUserDefaults] boolForKey:kScreenshotDisplayDisabled];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button click

- (IBAction)screenshotSwitchTapped:(UISwitch*)screenshotSwitch
{
    BOOL screenshotDisplayDisabled = !screenshotSwitch.on;
    [[NSUserDefaults standardUserDefaults]setBool:screenshotDisplayDisabled forKey:kScreenshotDisplayDisabled];
}

- (IBAction)privacyPolicyClicked:(id)sender
{
//    kAlert(nil, @"privacy policy clicked");
    
    PrivacyPolicyViewController *blockListVc = [[PrivacyPolicyViewController alloc]initWithNibName:@"PrivacyPolicyViewController" bundle:nil];
    blockListVc.screenTitle = @"Privacy Policy";
    blockListVc.htmlFile = @"Privacy Policy";
    [self.navigationController pushViewController:blockListVc animated:YES];
}

@end
