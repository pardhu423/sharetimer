//
//  UIColor+Addition.m
//  PrintKnot
//
//  Created by Sushree on 15/05/15.
//  Copyright (c) 2015 XpertsInfosoft. All rights reserved.
//

#import "UIColor+Addition.h"

@implementation UIColor (Addition)

+ (UIColor*)cellBrownColor
{
    return [UIColor colorWithHexString:@"#bb8266"];
}

+ (UIColor*)cellGreenColor
{
    return [UIColor colorWithHexString:@"#038c00"];
}

+ (UIColor*)cellYellowColor
{
    return [UIColor colorWithHexString:@"#e2ff3f"];
}

+ (UIColor*)cellPinkColor
{
    return [UIColor colorWithHexString:@"#bb829f"];
}

+ (UIColor*)redDotColor
{
    return [UIColor colorWithHexString:@"#f20012"];
}

+ (UIColor*)navigationBlueColor
{
    return [UIColor colorWithHexString:@"#a4e6ff"];
}

// takes @"#123456"
+ (UIColor *)colorWithHexString:(NSString *)hexStr {
    //-----------------------------------------
    // Convert hex string to an integer
    //-----------------------------------------
    unsigned int hexint = 0;
    
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet
                                       characterSetWithCharactersInString:@"#"]];
    [scanner scanHexInt:&hexint];
    
    //-----------------------------------------
    // Create color object, specifying alpha
    //-----------------------------------------
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:1.0];
    
    return color;

}

+ (NSString *)hexStringFromColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    
    return [NSString stringWithFormat:@"#%02lX%02lX%02lX",
            lroundf(r * 255),
            lroundf(g * 255),
            lroundf(b * 255)];
}
@end
