//
//  BaseViewController.h
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "AFConnectionManager.h"

@interface BaseViewController : UIViewController<AFConnectionDelegate>

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) AFConnectionManager *requestManager;

- (void)showServiceFailAlertWithMessage:(NSString *)message tag:(NSInteger)tag;
- (void)showLoaderWithTitle:(NSString *)title;
- (void)hideLoader;

@end
