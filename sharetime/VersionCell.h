//
//  VersionCell.h
//  sharetime
//
//  Created by iOS Developer on 10/12/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VersionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDetails;

@end
