//
//  LetterModel.h
//  sharetime
//
//  Created by iOS Developer on 01/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#define kDescription @"description"
#define kTheme @"theme"
#define kThumbUrl @"thumburl"
#define kVideoUrl @"video"
#define kMediaType @"type"
#define kViews @"view"
#define kScreenShots @"screenshot"
#define kCreatedAt @"createdAt"
#define kIsViewed @"isViewed"
#define kIsScreenshotTaken @"isScreenshot"
#define kIsTimestampShown @"isTimestamp"
#define kIsVolume @"isVolume"
#define kIsScreenshotCountVisible @"is_screenshot_visable"
#define kGender @"gender"

enum {
    SCROLL_THEME_FLAT,
    SCROLL_THEME_CURVED
};

enum {
    MEDIA_TYPE_IMAGE,
    MEDIA_TYPE_VIDEO
};

enum {
    FILTER_NONE,
    FILTER_BROWN,
    FILTER_PINK,
    FILTER_BLUE,
    FILTER_VIOLET,
    FILTER_GRAYSCALE
};

#import "BaseModel.h"

@interface LetterModel : BaseModel

@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *time;
@property (nonatomic) BOOL isTimeVisible;
@property (strong, nonatomic) NSString *text;
@property (nonatomic) NSInteger scrollTheme;
@property (strong, nonatomic) NSData *imgData;
@property (strong, nonatomic) NSURL *videoUrl;
@property (nonatomic) NSInteger mediaType;
@property (nonatomic) BOOL imageOrVideoPicked;
@property (strong, nonatomic) NSString *imgLink;
@property (nonatomic) NSInteger views;
@property (nonatomic) NSInteger screenShots;
@property (nonatomic) BOOL isViewed;
@property (nonatomic) BOOL isScreenshotTaken;
@property (nonatomic) NSInteger filterType;
@property (nonatomic) BOOL isVolume;
@property (nonatomic) BOOL isScreenshotCountVisible;
@property (nonatomic) BOOL gender;

@end
