//
//  FriendTableViewCell.m
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "FriendTableViewCell.h"

@interface FriendTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *img;

@end

@implementation FriendTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)bindUser:(UserModel*)user
{
    _lblName.text = user.username;
    NSString *imgName = genders[user.gender];
    _img.image = [UIImage imageNamed:imgName];
}

@end
