//
//  EditViewController.m
//  sharetime
//
//  Created by iOS Developer on 01/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

enum {
    ALERT_SELECT_IMAGE,
    ALERT_SAVE_TO_GALLERY
};

#define keyboardHeight 220

#import "EditViewController.h"
#import "HomeViewController.h"
#import "ShareViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVKit/AVKit.h>
#import <GPUImage.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "NSDate+Conversions.h"

@interface EditViewController ()<STAlertViewDelegate, UIImagePickerControllerDelegate, ScrollThemeViewDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate> {
    ScrollThemeView *_scrollTheme;
    UIImagePickerController *_imagePicker;
    BOOL _imagePickerShown;
    NSString *_savedLetterId;
    BOOL _arrowClicked;
    BOOL keyboardIsShowing,saveImageOrVideo;

}

@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *buttonWidths;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *filterButtons;
@property (weak, nonatomic) IBOutlet UIView *scrollContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerBottomMargin;
@property (weak, nonatomic) IBOutlet UICollectionView *collAlarms;
@property (weak, nonatomic) IBOutlet UIButton *btnAlarm;
@property (weak, nonatomic) IBOutlet UIButton *btnMasterFilter;
@property (weak, nonatomic) IBOutlet UILabel *lblAlarmTime;
@property (weak, nonatomic) IBOutlet UIButton *btnTime;
@property (weak, nonatomic) IBOutlet UIButton *btnMute;

@end

@implementation EditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    int width = screenSize.width/5;
    for (NSLayoutConstraint *buttonWidth in self.buttonWidths) {
        buttonWidth.constant = width;
    }
    [self initializeImagePicker];
    if (self.letterObject.scrollTheme == SCROLL_THEME_CURVED) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    _btnEdit.hidden = _willShowCamera;
    [_collAlarms registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellid"];
    
    keyboardIsShowing = NO;
    _btnMute.selected = ![[NSUserDefaults standardUserDefaults]boolForKey:kMute];
    
    _btnEdit.hidden = (_letterObject.mediaType == MEDIA_TYPE_IMAGE);
    _btnMute.hidden = (_letterObject.mediaType == MEDIA_TYPE_IMAGE);

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!_imagePickerShown && _willShowCamera) {
        [self presentViewController:_imagePicker animated:YES completion:nil];
    }
    _imagePickerShown = YES;
    
    if (!_scrollTheme) {
        NSString *nibName = _letterObject.scrollTheme == SCROLL_THEME_FLAT ? @"FlatScrollView" : @"ScrollThemeView";
        _scrollTheme = [[[NSBundle mainBundle]loadNibNamed:nibName owner:self options:nil] lastObject];
        _scrollTheme.delegate = self;
        _scrollTheme.letterObject = _letterObject;
        _scrollTheme.txtDescription.placeholder = @"Description comes here";
        _scrollTheme.txtDescription.editable = YES;
        _scrollTheme.timePassedView.hidden = YES;
        _scrollTheme.imageViewForVideoWhiteBorder.hidden = YES;
        
        [_scrollTheme resizeViewConstrainedTo:_scrollContainerView.frame.size animated:NO];
        [_scrollTheme transalateResizingIntoConstraints];
        
        UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(resignORShowKeyboard:)];
        [_scrollTheme.viewVideoPlayArea addGestureRecognizer:viewTap];
        viewTap.cancelsTouchesInView = NO;
        
        if (_willShowCamera) {
            [self.scrollContainerView addSubview:_scrollTheme];
        } else {
            [UIView transitionWithView:_scrollContainerView duration:1.0
                               options:UIViewAnimationOptionTransitionCurlDown
                            animations:^ { [self.scrollContainerView addSubview:_scrollTheme]; }
                            completion:^(BOOL finished) {
                                if (_scrollTheme.filterType != FILTER_NONE) {
                                    //[_scrollTheme applyFilter];
                                }
                            }];
        }
    }
    _btnTime.selected = !_scrollTheme.lblTime.hidden;

}

- (void)resignORShowKeyboard:(id)sender
{
    if (keyboardIsShowing == NO) {
        keyboardIsShowing = YES;
       [_scrollTheme.txtDescription becomeFirstResponder];
    }
    else {
        keyboardIsShowing = NO;
        [_scrollTheme.txtDescription resignFirstResponder];
    }
}
                              

- (void)presentImagePicker
{
    [self presentViewController:_imagePicker animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - keyboard
- (IBAction)keyboardWillShow:(id)sender
{
    [self slideBy:-keyboardHeight];
}

- (IBAction)keyboardWillHide:(id)sender
{
    [self slideBy:keyboardHeight];
}

- (void)slideBy:(int)deltaY
{
    self.topMargin.constant += deltaY;
    self.containerBottomMargin.constant -= deltaY;
    [UIView animateWithDuration:0.4 animations:^(void) {
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - image picker

- (void)initializeImagePicker
{
    _imagePicker = [[UIImagePickerController alloc]init];
    _imagePicker.delegate = self;
    _imagePicker.allowsEditing = YES;
    _imagePicker.videoMaximumDuration = 15;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        _imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    if (_letterObject.mediaType == MEDIA_TYPE_IMAGE) {
        _imagePicker.mediaTypes = @[(NSString*)kUTTypeImage];
    } else  {
        _imagePicker.mediaTypes = @[(NSString*)kUTTypeMovie];
    }
    if (_imagePicker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        if (_isFlashOn) {
            _imagePicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeOn;
        } else {
            _imagePicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
        }
        if (_isCameraFront) {
            _imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        } else {
            _imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        }
    }
}

#pragma mark - image picker delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) // Media is a video
    {
        _scrollTheme.btnPlay.hidden = NO;
        //_btnEdit.hidden = NO;
        _scrollTheme.filteredMovieUrl = nil;
        _scrollTheme.videoUrl = info[UIImagePickerControllerMediaURL];
        NSString *maskName = _scrollTheme.tag == SCROLL_THEME_CURVED ? @"curved_mask" : @"flat_mask";
        [_scrollTheme showImageFromVideoUrlWithMaskName:maskName];

    } else {
        UIImage *recievedImage = info[UIImagePickerControllerOriginalImage];
        
        if (_scrollTheme.letterObject.scrollTheme == SCROLL_THEME_CURVED) {
            [_scrollTheme showCurvedImage:recievedImage];
        } else {
            [_scrollTheme showFlatImage:recievedImage];
        }
    }
    
    
    
    _scrollTheme.imgData = UIImagePNGRepresentation(_scrollTheme.imgProfilePic.image);
    [picker dismissViewControllerAnimated:YES completion:^{
        _scrollTheme.filterType = FILTER_NONE;
        [self applyFilter];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{[self applyFilter];}];
}

- (void)applyFilter
{
    //[_scrollTheme applyFilter];
}
- (IBAction)volumeBtnClicked:(id)sender {
    if ([[NSUserDefaults standardUserDefaults]boolForKey:kMute]) {
        kAlert(nil, @"Please switch on sound from Settings > Manage > sound first");
    } else {
        _btnMute.selected = !_btnMute.selected;
    }
    _scrollTheme.muteButtonClicked = !_btnMute.selected;

}

#pragma mark - navigation

- (IBAction)leftBarButtonClicked:(id)sender
{
    //view controller at second last index is home vc
//    NSArray *viewControllers = self.navigationController.viewControllers;
//    HomeViewController *homeVc = (HomeViewController*)viewControllers[viewControllers.count - 2];
//    switch (homeVc.selectedScroll) {
//        case SCROLL_THEME_FLAT:
//            homeVc.flatScrollView.letterObject = _scrollTheme.letterObject;
//            break;
//            
//        case SCROLL_THEME_CURVED:
//            homeVc.curvedScrollView.letterObject = _scrollTheme.letterObject;
//            break;
//    }
    _imagePickerShown = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - button click

- (IBAction)timeClicked:(UIButton*)btnTime
{
    btnTime.selected = !btnTime.selected;
    _scrollTheme.lblTime.hidden = !btnTime.selected;
}

- (IBAction)masterFilterClicked:(UIButton*)btnMasterFilter
{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:kFiltersOff]) {
        kAlert(nil, @"Filters are turned off. Go to Settings > Manage > Filters");
        return;
    }
    btnMasterFilter.selected = !btnMasterFilter.selected;
    for (UIButton *btnFilter in _filterButtons) {
        [UIView animateWithDuration:0.4 animations:^(void) {
            btnFilter.hidden = !btnMasterFilter.selected;
        }];
    }
    int translateOffset = btnMasterFilter.selected ? -50 : 50;
    [self slideBy:translateOffset];
    
    _collAlarms.hidden = YES;
}

- (IBAction)saveClicked:(id)sender
{
    saveImageOrVideo = YES;
    STAlertview *alertSave = [[STAlertview alloc]initWithTitle:nil message:@"Save to gallery?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Yes"]];
    alertSave.tag = ALERT_SAVE_TO_GALLERY;
    [alertSave show];
}

- (IBAction)arrowClicked:(id)sender
{
    if (!_savedLetterId) {
        _arrowClicked = YES;
        if (_scrollTheme.mediaType == MEDIA_TYPE_VIDEO) {
            [self sendDataToServerWithTask:TASK_UPLOAD_VIDEO];
        } else {
            [self sendDataToServerWithTask:TASK_UPLOAD_IMAGE];
        }
    } else {
        ShareViewController *shareVc = [[ShareViewController alloc]initWithNibName:@"ShareViewController" bundle:nil];
        shareVc.letterId = _savedLetterId;
        [self.navigationController pushViewController:shareVc animated:YES];
    }
}


- (IBAction)alarmClicked:(id)sender
{
    _collAlarms.hidden = !_collAlarms.hidden;
    
    //hide filters if shown
    if (_btnMasterFilter.selected) {
        [self masterFilterClicked:_btnMasterFilter];
    }
}

#pragma mark - filter buttons

- (IBAction)grayscaleFilterClicked:(id)sender
{
//    saveImageOrVideo = YES;
    _scrollTheme.filterType = FILTER_GRAYSCALE;
    [_scrollTheme applyFilter];
}

- (IBAction)brownFilterClicked:(id)sender
{
    _scrollTheme.filterType = FILTER_NONE;
    [_scrollTheme applyFilter];
//    if (_scrollTheme.mediaType != MEDIA_TYPE_IMAGE) {
//        [_scrollTheme applyFilter];
//    }
    _scrollTheme.filteredMovieUrl = nil;
}

- (IBAction)pinkFilterClicked:(id)sender
{
//    saveImageOrVideo = YES;
    _scrollTheme.filterType = FILTER_PINK;
    [_scrollTheme applyFilter];
}

- (IBAction)blueFilterClicked:(id)sender
{
//    saveImageOrVideo = YES;
    _scrollTheme.filterType = FILTER_BLUE;
    [_scrollTheme applyFilter];
}

- (IBAction)violetFilterClicked:(id)sender
{
//    saveImageOrVideo = YES;
    _scrollTheme.filterType = FILTER_VIOLET;
    [_scrollTheme applyFilter];
}

#pragma mark - alert delegate

- (void)stAlertView:(STAlertview *)stAlertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (stAlertView.tag) {
        case ALERT_SAVE_TO_GALLERY:
        {
            if (_scrollTheme.imageOrVideoPicked) {
                if (_scrollTheme.mediaType == MEDIA_TYPE_VIDEO) {
                    [self sendDataToServerWithTask:TASK_UPLOAD_VIDEO];
                } else {
                    [self sendDataToServerWithTask:TASK_UPLOAD_IMAGE];
                }
            } else {
                NSString *message = @"Please pick a video first";
                if (_scrollTheme.mediaType == MEDIA_TYPE_IMAGE) {
                    message = @"Please select an image first";
                }
                STAlertview *alert = [[STAlertview alloc]initWithTitle:nil message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@[@"OK"]];
                alert.tag = ALERT_SELECT_IMAGE;
                [alert show];
            }
            break;
        }
            
        case ALERT_SELECT_IMAGE:
        {
            [self presentViewController:_imagePicker animated:YES completion:nil];
            break;
        }
            
        default:
            [self sendDataToServerWithTask:stAlertView.tag];
            break;
    }
}

- (void)stAlertViewDidCancel
{
    
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@"Saving..."];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_UPLOAD_VIDEO:
        {
            [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
            [postDict setObject:_scrollTheme.txtDescription.text forKey:kDescription];
            NSNumber *theme = [NSNumber numberWithInteger:_scrollTheme.letterObject.scrollTheme];
            [postDict setObject:theme forKey:kTheme];
            
            NSURL *videoUrl = _scrollTheme.letterObject.videoUrl;
            if (_scrollTheme.filterType != FILTER_NONE) {
                videoUrl = _scrollTheme.filteredMovieUrl;
            }
            
            //volume
            NSString *isVolume = _scrollTheme.btnVolume.selected ? @"1" : @"0";
            [postDict setObject:isVolume forKey:kIsVolume];
            
            //showtime
            NSString *isTimeShown = _scrollTheme.lblTime.hidden ? @"0" : @"1";
            [postDict setObject:isTimeShown forKey:kIsTimestampShown];
            
            //expiry
            NSString *expiry = [_lblAlarmTime.text substringToIndex:1];
            [postDict setObject:expiry forKey:@"expiry_duration"];
            
            //screenshot
            NSString *isScreenshotVisible = [[NSUserDefaults standardUserDefaults] boolForKey:kScreenshotDisplayDisabled] ? @"0" : @"1";
            [postDict setObject:isScreenshotVisible forKey:kIsScreenshotCountVisible];
            
            [self.requestManager uploadVideoWithLocalUrl:videoUrl urlEndPoint:@"videoupload" requestType:TASK_UPLOAD_VIDEO params:postDict videoKey:@"video" imageKey:kThumbUrl];
            
            if (saveImageOrVideo == YES) {
                saveImageOrVideo = NO;
                ALAssetsLibrary* library = [[ALAssetsLibrary alloc]init];
                
                if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:videoUrl])
                {
                    [library writeVideoAtPathToSavedPhotosAlbum:videoUrl completionBlock:^(NSURL *assetURL, NSError *error)
                     {
                         if (error)
                             kAlert(nil, error.localizedDescription);
                     }];
                }
            }
            break;
        }
            
        case TASK_UPLOAD_IMAGE:
        {
            [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
            NSString *description = _scrollTheme.txtDescription.text;
            description = [description stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [postDict setObject:description forKey:kDescription];
            NSNumber *theme = [NSNumber numberWithInteger:_scrollTheme.letterObject.scrollTheme];
            [postDict setObject:theme forKey:kTheme];
            
            NSData *imgData = _scrollTheme.letterObject.imgData;
            if (_scrollTheme.filterType != FILTER_NONE) {
                imgData = UIImagePNGRepresentation(_scrollTheme.imgProfilePic.image);
            }
            UIImage *image = [UIImage imageWithData:imgData];
            
            //showtime
            NSString *isTimeShown = _scrollTheme.lblTime.hidden ? @"0" : @"1";
            [postDict setObject:isTimeShown forKey:kIsTimestampShown];
            
            //expiry
            NSString *expiry = [_lblAlarmTime.text substringToIndex:1];
            [postDict setObject:expiry forKey:@"expiry_duration"];
            
            //screenshot
            NSString *isScreenshotVisible = [[NSUserDefaults standardUserDefaults] boolForKey:kScreenshotDisplayDisabled] ? @"0" : @"1";
            [postDict setObject:isScreenshotVisible forKey:kIsScreenshotCountVisible];
            
            [self.requestManager uploadPhoto:image urlEndPoint:@"media/upload" requestType:TASK_UPLOAD_IMAGE params:postDict imageKey:@"file"];
            
            //save to local device gallery
            if (saveImageOrVideo == YES) {
                saveImageOrVideo = NO;
                UIImage *screenShot = [UIImage imagewithScreenShotOfView:_scrollTheme];
                UIImageWriteToSavedPhotosAlbum(screenShot, nil, nil, nil);
            }
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_UPLOAD_VIDEO:
                //fall through
                
            case TASK_UPLOAD_IMAGE:
            {
                _savedLetterId = [responseDict objectForKey:kId];
                if (_arrowClicked) {
                    _arrowClicked = NO;
                    [self arrowClicked:nil];
                } else {
                    kAlert(nil, @"image saved");
                }
            }
                
            default:
                break;
        }
    } else {
        _arrowClicked = NO;
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    _arrowClicked = NO;
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

#pragma mark - collection view delegate flow layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int width = screenSize.width/7;
    width = width < 50 ? 50 : width;
    return CGSizeMake(width, width);
}

#pragma mark - collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 7;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellid" forIndexPath:indexPath];
    for (UIView *subview in cell.subviews) {
        [subview removeFromSuperview];
    }
    CGRect cellFrame = cell.frame;
    CGRect frame = CGRectMake(0, 0, cellFrame.size.width, cellFrame.size.height);
    UIImageView *imgAlarm = [[UIImageView alloc]initWithFrame:frame];
    imgAlarm.image = [UIImage imageNamed:@"alarm"];
    [cell addSubview:imgAlarm];
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:frame];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.text = [NSString stringWithFormat:@"%dd", (int)indexPath.row + 1];
    lbl.font = [UIFont appFontWithSize:20];
    lbl.textColor = [UIColor whiteColor];
    [cell addSubview:lbl];
    
    return cell;
}

#pragma mark - collection view delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *title = [NSString stringWithFormat:@"%dd", (int)indexPath.row + 1];
    _lblAlarmTime.text = title;
}

@end
