//
//  AcceptedFriendsViewController.m
//  sharetime
//
//  Created by iOS Developer on 30/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//
#import "AcceptedFriendsViewController.h"
#import "STFriendListNewfriend.h"
#import "STFriendListNSObject.h"

@interface AcceptedFriendsViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>
{
    STFriendListNSObject *newFriendModel;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collFriends;

@end

@implementation AcceptedFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_collFriends registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"identifier"];
    [self.navigationController addLeftBarButtonWithImageNamed:@"back_arrow"];
    [self.navigationController addTitle:@"Accepted me"];
    
    NSMutableDictionary *paramDict = [[NSMutableDictionary alloc]init];
    [paramDict setObject:self.appDelegate.user.Id forKey:@"userId"];
    [self.requestManager callServiceWithRequestType:TASK_NEW_FRIEND method:METHOD_POST params:paramDict urlEndPoint:@"newFriendList"];
}
#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_NEW_FRIEND:
            {
                newFriendModel = [[STFriendListNSObject alloc]initWithDictionary:responseDict];
                [_collFriends reloadData];
                break;
            }
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

#pragma mark - collection view data source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return newFriendModel.newfriend.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"identifier" forIndexPath:indexPath];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
    imgView.backgroundColor = [UIColor brownColor];
    //NSString *imgName = (indexPath.row % 2 == 0) ? @"male" : @"female";
    STFriendListNewfriend *accptedFriend = newFriendModel.newfriend[indexPath.row];
    if ([accptedFriend.gender isEqual:@"0"]) {
        imgView.image = [UIImage imageNamed:@"male"];
    } else {
    imgView.image = [UIImage imageNamed:@"female"];
    }
    [cell addSubview:imgView];
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 60, 60, 20)];
    lbl.font = [UIFont systemFontOfSize:13];
    lbl.textAlignment = NSTextAlignmentCenter;
    //lbl.text = @"name";
    lbl.text = accptedFriend.username;
    lbl.adjustsFontSizeToFitWidth = YES;
    [cell addSubview:lbl];
    
    return cell;
}

#pragma mark - collection view delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
