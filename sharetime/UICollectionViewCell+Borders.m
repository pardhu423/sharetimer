//
//  UICollectionViewCell+Borders.m
//  sharetime
//
//  Created by iOS Developer on 03/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "UICollectionViewCell+Borders.h"

@interface UICollectionViewCell ()

@end

@implementation UICollectionViewCell (Borders)

- (void)addBottomShadeWithWidth:(CGFloat)width color:(UIColor *)color
{
    CGRect frame = self.frame;
    UIView *viewShade = [[UIView alloc]initWithFrame:CGRectMake(0, frame.size.height - width, frame.size.width, width)];
    viewShade.backgroundColor = color;
    [self addSubview:viewShade];
}

@end
