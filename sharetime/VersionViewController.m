//
//  VersionViewController.m
//  sharetime
//
//  Created by iOS Developer on 10/12/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "VersionViewController.h"
#import "VersionCell.h"

@interface VersionViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation VersionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController addTitle:@"Version History"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"VersionCell";
    VersionCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:identifier owner:self options:nil]lastObject];
    }
    return cell;
}

#pragma mark - table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VersionCell *cell = [[[NSBundle mainBundle]loadNibNamed:@"VersionCell" owner:self options:nil]lastObject];
    CGRect detailsFrame = cell.lblDetails.frame;
    CGFloat height = detailsFrame.origin.y + detailsFrame.size.height + 20;
    return height;
}

@end
