//
//  STAlertview.m
//  sharetime
//
//  Created by iOS Developer on 24/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#define borderWidth 4

#import "STAlertview.h"
#import "UIColor+Addition.h"
#import "UIFont+OurFont.h"

@interface STAlertview () {
    UIColor *_borderColor;
}

@end

@implementation STAlertview

- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSArray *)otherButtonTitles
{
    int screenWidth = [[UIScreen mainScreen]bounds].size.width;
    int margin = 30;
    int width = screenWidth - 2 * margin;
    self = [super initWithFrame:CGRectMake(margin, 0, width, 300)];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:@"#a4e6ff"];
        _borderColor = [UIColor colorWithHexString:@"#1b2436"];
        
        int titleHeight = 40;
        //title label
        if (title) {
            UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, titleHeight)];
            lblTitle.font = [UIFont boldSystemFontOfSize:18];
            lblTitle.text = title;
            lblTitle.numberOfLines = 0;
            lblTitle.backgroundColor = [UIColor clearColor];
            lblTitle.textAlignment = NSTextAlignmentCenter;
            [self addSubview:lblTitle];
        } else {
            titleHeight = 10;
        }
        
        //message
        UILabel *lblMessage = [[UILabel alloc]initWithFrame:CGRectMake(0, titleHeight, width, 200)];
        lblMessage.text = message;
        lblMessage.numberOfLines = 0;
        lblMessage.textAlignment = NSTextAlignmentCenter;
        lblMessage.backgroundColor = [UIColor clearColor];
        lblMessage.font = [UIFont systemFontOfSize:15];
        [lblMessage sizeToFit];
        lblMessage.center = CGPointMake(width/2, lblMessage.center.y);
        [self addSubview:lblMessage];
        
        int cancelStartY = lblMessage.frame.size.height + titleHeight + 10;
        
        if (cancelButtonTitle) {
            BOOL isSingleOtherButton = (otherButtonTitles.count == 1);
            int cancelWidth = isSingleOtherButton ? width/2 : width;
            //cancel button
            UIButton *cancelButton = [self buttonWithWidth:cancelWidth text:cancelButtonTitle startY:cancelStartY startX:0];
            [cancelButton addTarget:self action:@selector(cancelClicked:) forControlEvents:UIControlEventTouchUpInside];
            [self addRightBorderWithWidth:borderWidth/2 color:_borderColor toView:cancelButton];
            [self addSubview:cancelButton];
        }
        
        //other buttons
        for (NSString *otherButtonTitle in otherButtonTitles) {
            NSInteger index = [otherButtonTitles indexOfObject:otherButtonTitle];
            int btnWidth, startX, prevButtonCount = (int)index;
            if (otherButtonTitles.count == 1 && cancelButtonTitle) {
                btnWidth = width/2;
                startX = width/2;
            } else {
                btnWidth = width;
                startX = 0;
            }
            if (cancelButtonTitle && index > 0) {
                prevButtonCount++;
            }
            UIButton *otherButton = [self buttonWithWidth:btnWidth text:otherButtonTitle startY:cancelStartY + prevButtonCount * 30 startX:startX];
            otherButton.tag = index;
            [otherButton addTarget:self action:@selector(otherButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            if (cancelButtonTitle && otherButtonTitles.count == 1) {
                [self addLeftBorderWithWidth:borderWidth/2 color:_borderColor toView:otherButton];
            }
            [self addSubview:otherButton];
        }
        self.delegate = delegate;
    }
    
    NSArray *subviews = self.subviews;
    UIView *lastView = subviews[subviews.count - 1];
    int height = lastView.frame.size.height + lastView.frame.origin.y + 5;
    self.frame = CGRectMake(0, 0, width, height);
    return self;
}

- (UIButton*)buttonWithWidth:(int)width text:(NSString*)text startY:(int)startY startX:(int)startX
{
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(startX, startY, width, 30)];
    btn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setTitle:text forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont appFontWithSize:25];
    [self addTopShadeWithWidth:borderWidth color:_borderColor toView:btn];
    return btn;
}

- (void)show
{
    UIView *tintView = [[UIView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    tintView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.1];
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    [window addSubview:tintView];
    
    CGSize size = [[UIScreen mainScreen]bounds].size;
    self.center = CGPointMake(size.width/2, size.height/2);
    [tintView addSubview:self];
}

#pragma mark - button clicks

- (IBAction)cancelClicked:(id)sender
{
    [self.superview removeFromSuperview];
    if ([self.delegate respondsToSelector:@selector(stAlertViewDidCancel:)]) {
        [self.delegate stAlertViewDidCancel:self];
    }
}

- (IBAction)otherButtonClicked:(id)sender
{
    NSInteger tag = [sender tag];
    if ([self.delegate respondsToSelector:@selector(stAlertView:clickedButtonAtIndex:)]) {
        [self.delegate stAlertView:self clickedButtonAtIndex:tag];
    }
    [self.superview removeFromSuperview];
}

#pragma mark - borders

- (void)addTopShadeWithWidth:(float)width color:(UIColor *)color toView:(UIView*)view
{
    CGRect frame = view.frame;
    UIView *viewShade = [[UIView alloc]initWithFrame:CGRectMake(frame.origin.x, frame.origin.y - width, frame.size.width, width)];
    viewShade.backgroundColor = color;
    [self addSubview:viewShade];
}

- (void)addLeftBorderWithWidth:(float)width color:(UIColor *)color toView:(UIView*)view
{
    CGRect frame = view.frame;
    UIView *viewBorder = [[UIView alloc]initWithFrame:CGRectMake(frame.origin.x - width, frame.origin.y, width, frame.size.height)];
    viewBorder.backgroundColor = color;
    [self addSubview:viewBorder];
}

- (void)addRightBorderWithWidth:(float)width color:(UIColor *)color toView:(UIView*)view
{
    CGRect frame = view.frame;
    UIView *viewBorder = [[UIView alloc]initWithFrame:CGRectMake(frame.origin.x + frame.size.width, frame.origin.y, width, frame.size.height)];
    viewBorder.backgroundColor = color;
    [self addSubview:viewBorder];
}

@end
