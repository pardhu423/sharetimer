//
//  HomeViewController.h
//  sharetime
//
//  Created by iOS Developer on 29/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "BaseViewController.h"

@class ScrollThemeView;

@interface HomeViewController : BaseViewController

@property (strong, nonatomic) ScrollThemeView *flatScrollView;
@property (strong, nonatomic) ScrollThemeView *curvedScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (nonatomic) NSInteger selectedScroll;

@end
