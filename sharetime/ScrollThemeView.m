//
//  ScrollThemeView.m
//  sharetime
//
//  Created by iOS Developer on 29/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "ScrollThemeView.h"
#import "UIImage+ImageManipulations.h"
#import "UIImageView+AFNetworking.h"
#import <AVFoundation/AVFoundation.h>
#import <GPUImage.h>
#import "MBProgressHUD.h"
#import "Header.h"

@interface ScrollThemeView ()<UITextViewDelegate, NSLayoutManagerDelegate> {
    GPUImageMovieWriter *_movieWriter;
    GPUImageMovie *_movieFile;
    MBProgressHUD *_loader;
    AVAsset *asset;
    AVPlayer *avPlayer;
}

@end

@implementation ScrollThemeView

- (void)awakeFromNib
{
    [super awakeFromNib];
    //initialize current time
    LetterModel *letter = [[LetterModel alloc]init];
    _lblTime.text = letter.time;
    _txtDescription.layoutManager.delegate = self;
    NSLog(@"Tag is %ld",_txtDescription.tag);
}
- (CGFloat)layoutManager:(NSLayoutManager *)layoutManager lineSpacingAfterGlyphAtIndex:(NSUInteger)glyphIndex withProposedLineFragmentRect:(CGRect)rect
{
    if (_txtDescription.tag == 0)

        return 3.5f; // For really wide spacing; pick your own value
    else
        return 8.0f;
}

- (void)transalateResizingIntoConstraints
{
    for (UIView *subview in self.subviews) {
        subview.translatesAutoresizingMaskIntoConstraints = YES;
    }
}
#pragma mark - show image

- (void)showFlatImage:(UIImage*)image
{
    _btnPlay.hidden = YES;
    [self showImage:image withMaskName:imgMaskFlat];
}

- (void)showCurvedImage:(UIImage*)image
{
    _btnPlay.hidden = YES;
    [self showImage:image withMaskName:imgCurvedMask];
}

- (void)showImage:(UIImage*)image withMaskName:(NSString*)maskName
{
    UIImage *imgToShow;
    CGSize size = _imgProfilePic.frame.size;
    imgToShow = [image focusImageTopAndResizeTo:size];
    imgToShow = [imgToShow downsizeTo:10000];
    
    UIImage *mask = [UIImage imageNamed:maskName];
    imgToShow = [imgToShow maskImageWithMask:mask];
    _imgProfilePic.image = imgToShow;
    self.imageOrVideoPicked = YES;
}

- (void)showImageFromVideoUrlWithMaskName:(NSString *)maskName
{
    _btnPlay.hidden = NO;
    
    UIImage *image = [UIImage videoSnapshotFromUrl:_videoUrl];
    [self showImage:image withMaskName:maskName];
}

#pragma mark - text view delegate

- (void)textViewDidChange:(UITextView *)textView
{
    NSMutableAttributedString *underlinedString = [[NSMutableAttributedString alloc]initWithString:textView.text];
    NSRange range = NSMakeRange(0, textView.text.length);
    [underlinedString addAttribute:NSUnderlineStyleAttributeName
                      value:[NSNumber numberWithInt:NSUnderlineStyleNone]
                      range:range];
    [underlinedString addAttribute:NSFontAttributeName value:textView.font range:range];
    textView.attributedText = underlinedString;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
//    return YES;
    NSString *newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    NSDictionary *textAttributes = @{NSFontAttributeName : textView.font};
    
    CGFloat textWidth = CGRectGetWidth(UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset));
    textWidth -= 2.0f * textView.textContainer.lineFragmentPadding;
    CGRect boundingRect = [newText boundingRectWithSize:CGSizeMake(textWidth, 0)
                                                options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                             attributes:textAttributes
                                                context:nil];
    
    NSUInteger numberOfLines = CGRectGetHeight(boundingRect) / textView.font.lineHeight;

    if (textView.tag == 0)
        return newText.length <= 500 && numberOfLines <= 3;
     else
        return newText.length <= 300 && numberOfLines <= 2;
}

#pragma mark - getter setter

- (LetterModel*)letterObject
{
    LetterModel *letter = [[LetterModel alloc]init];
    letter.text = _txtDescription.text;
    letter.scrollTheme = self.tag;
    letter.imgData = nil;
    letter.imgData = UIImagePNGRepresentation(_imgProfilePic.image);
    letter.time = _lblTime.text;
    letter.isTimeVisible = !_lblTime.hidden;
    letter.videoUrl = _videoUrl;
    letter.mediaType = _mediaType;
    letter.imageOrVideoPicked = _imageOrVideoPicked;
    letter.filterType = _filterType;
    letter.isVolume = _btnVolume.selected;
   // _txtDescription.textContainer.maximumNumberOfLines = letter.scrollTheme == 0 ? 3 : 2;
    _txtDescription.textContainer.maximumNumberOfLines = _txtDescription.tag == 0 ? 3 : 2;

    if (_imgData) {
        letter.imgData = _imgData;
    }
    
    return letter;
}

- (void)setLetterObject:(LetterModel *)letterObject
{
    @autoreleasepool {
        
        NSDateFormatter *inputFormat = [[NSDateFormatter alloc]init];
        inputFormat.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc]init];
        outputFormatter.dateFormat = @"dd-MM-yy";
        
        NSDate *postedDateFromServer = [inputFormat dateFromString:letterObject.time];
        NSString *postedDate = [outputFormatter stringFromDate:postedDateFromServer];
        
        _lblTime.text = postedDate;
        _lblTime.hidden = !letterObject.isTimeVisible;
        _mediaType = letterObject.mediaType;
        if (_mediaType == MEDIA_TYPE_IMAGE) {
            if (letterObject.imgData) {
                UIImage *image = [UIImage imageWithData:letterObject.imgData];
                if (self.tag == SCROLL_THEME_FLAT) {
                    [self showFlatImage:image];
                } else {
                    [self showCurvedImage:image];
                }
                image = nil;
            }
            if (letterObject.imgLink) {
                [self loadImageFromLink:letterObject.imgLink];
            }
        }
        
        _videoUrl = letterObject.videoUrl;
        if (_mediaType == MEDIA_TYPE_VIDEO && _videoUrl) {
            if (letterObject.imgLink) {
                [self loadImageFromLink:letterObject.imgLink];
            } else {
                NSString *maskName = self.tag == SCROLL_THEME_FLAT ? imgMaskFlat : imgCurvedMask;
                [self showImageFromVideoUrlWithMaskName:maskName];
                self.btnPlay.hidden = NO;
            }
        }
        
        _txtDescription.text = letterObject.text;
        [self textViewDidChange:_txtDescription];
        
        _imageOrVideoPicked = letterObject.imageOrVideoPicked;
        _filterType = letterObject.filterType;
        
        if (_filterType != FILTER_NONE) {
            _imgData = letterObject.imgData;
        }
    }
}

- (void)loadImageFromLink:(NSString*)imgLink
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgLink]];
    NSString *placeholder = (self.tag == SCROLL_THEME_FLAT) ? @"profile_pic" : @"scroll_placeholder";
    UIImage *placholderImage = [UIImage imageNamed:placeholder];
    [self.imgProfilePic setImageWithURLRequest:request placeholderImage:placholderImage
       success:^(NSURLRequest *request, NSURLResponse *response, UIImage *image) {
           if (self.tag == SCROLL_THEME_FLAT) {
               [self showFlatImage:image];
           } else {
               [self showCurvedImage:image];
           }
           self.btnPlay.hidden = (self.mediaType == MEDIA_TYPE_IMAGE);
       } failure:nil];
}

#pragma mark - button click

- (IBAction)playClicked:(id)sender
{
    [self endEditing:YES];
    self.btnPlay.hidden = YES;
    asset = [AVAsset assetWithURL:_videoUrl];
    if (_filteredMovieUrl) {
        //_playerVc.player = [[AVPlayer alloc]initWithURL:_filteredMovieUrl];
    asset = [AVAsset assetWithURL:_filteredMovieUrl];
    }
    AVPlayerItem *playerItem = [[AVPlayerItem alloc] initWithAsset:asset];
    avPlayer = [AVPlayer playerWithPlayerItem:playerItem];
    
    AVPlayerLayer *avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:avPlayer];
    avPlayer.muted = _muteButtonClicked;
    avPlayerLayer.frame = self.viewVideoPlayArea.bounds;
    avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.viewVideoPlayArea.layer addSublayer:avPlayerLayer];
    if (_imageViewForVideoBlueBorder.hidden == YES) {
        [self.viewVideoPlayArea bringSubviewToFront:_imageViewForVideoWhiteBorder];
    }
    [self.viewVideoPlayArea bringSubviewToFront:_imageViewForVideoBlueBorder];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(playerItemDidReachEnd:) name:AVPlayerItemDidPlayToEndTimeNotification
     object:[avPlayer currentItem]];
    [avPlayer play];
    
}

#pragma mark - Notification playing video completed
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    self.btnPlay.hidden = NO;
    [self.viewVideoPlayArea bringSubviewToFront:_imgProfilePic];
}

#pragma mark - filter

- (void)applyFilter
{
    //filter captured image or image from video
    UIImage *image = [UIImage imageWithData:_imgData];
    NSLog(@"initial size: %lu", (unsigned long)_imgData.length);
    if (!image && !_videoUrl) {
        kAlert(nil, @"Please select an image or video to filter first");
        return;
    }
    
    //initialize filters
    GPUImageRGBFilter *filter = [self getRgbFilter];
    GPUImageGrayscaleFilter *gsFilter = [[GPUImageGrayscaleFilter alloc]init];
    if (_filterType == FILTER_NONE) {
        //[gsFilter removeOutputFramebuffer];
        [gsFilter removeTarget:filter];
        [gsFilter removeAllTargets];
        UIImage *image = [UIImage imageWithData:_imgData];
        _imgProfilePic.image = image;
       [self applyMask];
        return;
    }
    image = [gsFilter imageByFilteringImage:image];
    _imgProfilePic.image = [filter imageByFilteringImage:image];
    [self applyMask];
    
    NSData *newData = UIImagePNGRepresentation(_imgProfilePic.image);
    NSLog(@"final size: %lu", (unsigned long)newData.length);
    
    if (self.mediaType == MEDIA_TYPE_VIDEO) {
        //movie filters
        //separate filter objects for movie else it will crash
        if(self.filterType != FILTER_NONE){
            GPUImageGrayscaleFilter *gsMovieFilter = [[GPUImageGrayscaleFilter alloc]init];
            [gsMovieFilter setInputRotation:kGPUImageRotateRight atIndex:0];
            
            GPUImageRGBFilter *movieRgbFilter = [self getRgbFilter];
            [movieRgbFilter setInputRotation:kGPUImageRotateRight atIndex:0];
            
            [self showLoaderWithTitle:@"Processing Video"];
            [self filterMovieWithFilter:gsMovieFilter url:_videoUrl];
        }
        else
        {
            //filter captured image or image from video
            UIImage *image = [UIImage imageWithData:_imgData];
            NSLog(@"initial size: %lu", (unsigned long)_imgData.length);
            if (!image && !_videoUrl) {
                kAlert(nil, @"Please select an image or video to filter first");
                return;
            }
            
            //initialize filters
            GPUImageRGBFilter *filter = [self getRgbFilter];
            GPUImageGrayscaleFilter *gsFilter = [[GPUImageGrayscaleFilter alloc]init];
            if (_filterType == FILTER_NONE) {
                //[gsFilter removeOutputFramebuffer];
                [gsFilter removeTarget:filter];
                [gsFilter removeAllTargets];
                UIImage *image = [UIImage imageWithData:_imgData];
                _imgProfilePic.image = image;
                [self applyMask];
                return;
            }
            image = [gsFilter imageByFilteringImage:image];
            _imgProfilePic.image = [filter imageByFilteringImage:image];
            [self applyMask];
            
            NSData *newData = UIImagePNGRepresentation(_imgProfilePic.image);
            NSLog(@"final size: %lu", (unsigned long)newData.length);
        }
        
    }
}

- (void)filterMovieWithFilter:(GPUImageFilter*)filter url:(NSURL*)url
{
    _movieFile = [[GPUImageMovie alloc] initWithURL:url];
    _movieFile.runBenchmark = YES;
    _movieFile.playAtActualSpeed = NO;
    
    //filter video
    [_movieFile addTarget:filter];
    NSString *pathToMovie = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@.MOV", filter.class]];
    
    unlink([pathToMovie UTF8String]); // If a file already exists, AVAssetWriter won't let you record new frames, so delete the old movie
    
    NSURL *movieURL = [NSURL fileURLWithPath:pathToMovie];
    
    _movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:movieURL size:CGSizeMake(720.0, 540.0)];
    [filter addTarget:_movieWriter];
    
    // Configure this for video from the movie file, where we want to preserve all video frames and audio samples
    _movieWriter.shouldPassthroughAudio = YES;
    _movieFile.audioEncodingTarget = _movieWriter;
    [_movieFile enableSynchronizedEncodingUsingMovieWriter:_movieWriter];
    
    [_movieWriter startRecording];
    [_movieFile startProcessing];
    
    __block GPUImageOutput<GPUImageInput> *weakfilter = filter;
    __block GPUImageMovieWriter *weakMovieWriter = _movieWriter;
    __weak typeof(self) weakSelf = self;
    
    [_movieWriter setCompletionBlock:^{
        [weakfilter removeTarget:weakMovieWriter];
        [weakMovieWriter finishRecording];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            if (weakSelf.filterType == FILTER_GRAYSCALE ||
                [filter isKindOfClass:[GPUImageRGBFilter class]]) {
                _filteredMovieUrl = movieURL;
                [weakSelf hideLoader];
            } else {
                //sequenetial filter
                GPUImageRGBFilter *movieRgbFilter = [weakSelf getRgbFilter];    
                [weakSelf filterMovieWithFilter:movieRgbFilter url:movieURL];
            }
        });
    }];
}

- (GPUImageRGBFilter*)getRgbFilter
{
    GPUImageRGBFilter *filter = [[GPUImageRGBFilter alloc]init];
    
    switch (_filterType) {
            
        case FILTER_BROWN:
        {
            filter.red = 187.0/255.0;
            filter.green = 130.0/255.0;
            filter.blue = 102.0/255.0;
            break;
        }
            
        case FILTER_PINK:
        {
            filter.red = 251.0/255.0;
            filter.green = 211.0/255.0;
            filter.blue = 233.0/255.0;
            break;
        }
            
        case FILTER_BLUE:
        {
            filter.red = 215.0/255.0;
            filter.green = 222.0/255.0;
            filter.blue = 236.0/255.0;
            break;
        }
            
        case FILTER_VIOLET:
        {
            filter.red = 228.0/255.0;
            filter.green = 252.0/255.0;
            filter.blue = 221.0/255.0;
            break;
        }
            
        default:
            break;
    }
    return filter;
}

- (void)applyMask
{
    _btnPlay.hidden = (_mediaType == MEDIA_TYPE_IMAGE);
    
    if (self.letterObject.scrollTheme == SCROLL_THEME_CURVED) {
        [self showImage:_imgProfilePic.image withMaskName:imgCurvedMask];
    } else {
        [self showImage:_imgProfilePic.image withMaskName:imgMaskFlat];
    }
}

#pragma mark - loader

- (void)showLoaderWithTitle:(NSString *)title
{
    if (!_loader) {
        _loader = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication]keyWindow] animated:YES];
        _loader.labelText = title;
    }
}

- (void)hideLoader
{
    if (_loader) {
        [_loader removeFromSuperview];
        _loader = nil;
    }
}

@end
