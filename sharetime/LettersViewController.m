//
//  LettersViewController.m
//  sharetime
//
//  Created by iOS Developer on 03/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

enum {
    SECTION_NEW_LETTERS,
    SECTION_ALL_LETTERS
};

#import "LettersViewController.h"
#import "ScrollThemeView.h"
#import "FriendViewController.h"
#import "ProfileViewController.h"
#import "HomeViewController.h"

@interface LettersViewController ()<UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegate,  UICollectionViewDelegateFlowLayout> {
    NSMutableArray *_newLetters;
    NSMutableArray *_oldLetters;
    NSArray *_filteredNewLetters;
    NSArray *_filteredOldLetters;
    UIColor *oldColor;

}

@property (weak, nonatomic) IBOutlet UIView *rightBarButtonView;
@property (weak, nonatomic) IBOutlet UICollectionView *collLetters;
//@property (weak, nonatomic) IBOutlet UITextField *txtSearch;

@end

@implementation LettersViewController

#pragma mark - vc life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController addLeftBarButtonWithImageNamed:@"paper_blue"];
    //[self.navigationController addPlainTitle:@"Letters" color:[UIColor navigationBlueColor]];
    [self.navigationController addTitleColor:@"Letters"];
    
    //style search text field
    //[_txtSearch addLeftImageNamed:@"search"];
    //[_txtSearch addRightButtonWithImageName:@"search_close" target:self selector:@selector(cancelSearchClicked:) viewMode:UITextFieldViewModeWhileEditing];
    
    //collection view cell register
    [self.collLetters registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"identifier"];
    [self.collLetters registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    
    [self.view addHorizontalSwipeRecognizersWithTarget:self leftSwipeSelector:@selector(leftSwipeDetected:) rightSwipeSelector:@selector(rightSwipeDetected:)];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //[_txtSearch addBottomBorderWithWidth:1 color:[UIColor lightGrayColor]];
    
    [self sendDataToServerWithTask:TASK_LETTERS];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    oldColor = self.navigationController.navigationBar.barTintColor;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"#3a4347"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = oldColor;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - nav

- (IBAction)leftBarButtonClicked:(id)sender
{
    NSArray *viewControllers = self.navigationController.viewControllers;
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[HomeViewController class]]) {
            [self.navigationController popToViewController:vc animated:YES];
            break;
        }
    }
}

#pragma mark - gesture

- (IBAction)rightSwipeDetected:(id)sender
{
    ProfileViewController *profileVc = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
    profileVc.isMyProfile = YES;
    [self.navigationController pushViewController:profileVc animated:YES];
}

- (IBAction)leftSwipeDetected:(id)sender
{
    //[self.navigationController popViewControllerAnimated:YES];
    NSArray *viewControllers = self.navigationController.viewControllers;
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[HomeViewController class]]) {
            [self.navigationController popToViewController:vc animated:YES];
            break;
        }
    }
}

#pragma mark - button clicks

- (IBAction)scrollClicked:(id)sender
{
    
}

- (IBAction)groupClicked:(id)sender
{
    
}

#pragma mark - text field delegate
/*
- (IBAction)cancelSearchClicked:(id)sender
{
    _txtSearch.text = @"";
    [_txtSearch resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)searchTextChanged:(id)sender
{
    NSString *newText = [_txtSearch.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (newText.length) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"SELF.username beginswith[c] %@",
                                  newText];
        _filteredNewLetters = [_newLetters filteredArrayUsingPredicate:predicate];
        _filteredOldLetters= [_oldLetters filteredArrayUsingPredicate:predicate];
    } else {
        _filteredNewLetters = _newLetters;
        _filteredOldLetters = _oldLetters;
    }
    [self.collLetters reloadData];
}
*/

#pragma mark - collection view delegate flow layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int width = collectionView.frame.size.width/4;
    int height = width * 1.5 + 20;
    return CGSizeMake(width, height);
}

#pragma mark - collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == SECTION_NEW_LETTERS) {
        return _filteredNewLetters.count;
    } else {
        return _filteredOldLetters.count;
    }
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"identifier" forIndexPath:indexPath];
    for (UIView *subview in cell.subviews) {
        [subview removeFromSuperview];
    }
    
    int cellWidth = cell.frame.size.width;
    int leftMargin = 5;
    
    LetterModel *letter;
    if (indexPath.section == SECTION_NEW_LETTERS) {
        letter = _filteredNewLetters[indexPath.row];
        //reduced size
        cellWidth *= 0.7;
        leftMargin = cellWidth * 0.3;
    } else {
        letter = _filteredOldLetters[indexPath.row];
    }
    
    UIView *scrollContainerView = [[UIView alloc]initWithFrame:CGRectMake(leftMargin, 0, cellWidth - 2 * leftMargin, cellWidth * 1.5)];
    [cell addSubview:scrollContainerView];
    NSString *nibName = @"ScrollThemeView";
    if (letter.scrollTheme == SCROLL_THEME_FLAT) {
        nibName = @"FlatScrollView";
    }
    ScrollThemeView *scroll = [[[NSBundle mainBundle]loadNibNamed:nibName owner:self options:nil]lastObject];
    scroll.letterObject = letter;
    
    scroll.viewVideoPlayArea.backgroundColor = [UIColor clearColor];
    scroll.imageViewForVideoBlueBorder.hidden = YES;
    scroll.imageViewForVideoWhiteBorder.hidden = YES;
    
    [scrollContainerView addSubview:scroll];
    [scroll resizeViewConstrainedTo:scrollContainerView.frame.size animated:NO];
    [scroll transalateResizingIntoConstraints];
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, cellWidth * 1.5, cellWidth - 2 * leftMargin, 20)];
    lbl.adjustsFontSizeToFitWidth = YES;
    lbl.text =  letter.username;
    if (indexPath.section == SECTION_NEW_LETTERS) {
//        [cell addSubview:lbl];
    }
    
    [cell addBottomShadeWithWidth:1 color:[UIColor lightGrayColor]];
    return cell;
}

- (UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    NSArray *headers = @[@"NEW LETTERS", @"ALL LETTERS"];

    if (kind == UICollectionElementKindSectionHeader) {
        reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        
        for (UIView *subview in reusableview.subviews) {
            [subview removeFromSuperview];
        }
        
        
        UILabel *lbl = (UILabel*)[reusableview viewWithTag:1];
        CGSize size = reusableview.frame.size;
        lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, -10, 80, size.height/2)];
        lbl.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:10];
        lbl.center = CGPointMake(size.width/2, size.height/2);
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = headers[indexPath.section];
        
        NSArray *colors = @[[UIColor cellGreenColor], [UIColor navigationBlueColor]];
        lbl.backgroundColor = colors[indexPath.section];
        [reusableview addSubview:lbl];
        
        lbl.hidden = YES;
        
        if (indexPath.section == SECTION_NEW_LETTERS && _newLetters.count == 0) {
            lbl.hidden = YES;
            return reusableview;
        }
        if (_oldLetters.count == 0) {
            lbl.hidden = YES;
            return reusableview;
        }
    }
    
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section == SECTION_NEW_LETTERS && _newLetters.count == 0) {
        return CGSizeZero;
    } else {
        return CGSizeMake(150, 40);
    }
}

#pragma mark - collection view delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FriendViewController *friendVc = [[FriendViewController alloc]initWithNibName:@"FriendViewController" bundle:nil];
    if (indexPath.section == SECTION_NEW_LETTERS) {
        friendVc.letter = _filteredNewLetters[indexPath.row];
        friendVc.letters = [NSMutableArray arrayWithArray:_filteredNewLetters];
    } else {
        friendVc.letter = _filteredOldLetters[indexPath.row];
        friendVc.letters = [NSMutableArray arrayWithArray:_filteredOldLetters];
    }
    friendVc.selectedLetterIndex = indexPath.row;
    friendVc.isFromLetter = YES;
    [self.navigationController pushViewController:friendVc animated:YES];
}



#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@""];
    NSString *userId = self.appDelegate.user.Id;
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    
    switch (task) {
        case TASK_LETTERS:
        {
            [postDict setObject:userId forKey:kUserId];
            [self.requestManager callServiceWithRequestType:TASK_LETTERS method:METHOD_POST params:postDict urlEndPoint:@"letters"];
            break;
        }
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_LETTERS:
            {
                NSDictionary *lettersDict = [responseDict objectForKey:@"letters"];
                _newLetters = [[NSMutableArray alloc]init];
                NSArray *newLetters = [lettersDict objectForKey:@"newletters"];
                for (NSDictionary *letterDict in newLetters) {
                    LetterModel *letter = [[LetterModel alloc]initWithResponse:letterDict];
                    [_newLetters addObject:letter];
                }
                _oldLetters = [[NSMutableArray alloc]init];
                NSArray *oldLetters = [lettersDict objectForKey:@"oldletters"];
                for (NSDictionary *letterDict in oldLetters) {
                    LetterModel *letter = [[LetterModel alloc]initWithResponse:letterDict];
                    [_oldLetters addObject:letter];
                }
                NSArray *myLetters = [lettersDict objectForKey:@"myletters"];
                for (NSDictionary *letterDict in myLetters) {
                    LetterModel *letter = [[LetterModel alloc]initWithResponse:letterDict];
                    [_oldLetters addObject:letter];
                }
                
                _filteredNewLetters = _newLetters;
                _filteredOldLetters = _oldLetters;
                [_collLetters reloadData];
                break;
            }
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

@end
