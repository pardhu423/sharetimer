//
//  MyMobileViewController.m
//  sharetime
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#define strPrivacyPolicy @"Privacy Policy"
#define TEXT_COUNTRY 10

#import "MyMobileViewController.h"
#import "CountryPicker.h"
#import "PrivacyPolicyViewController.h"

@interface MyMobileViewController ()<CountryPickerDelegate, UITextFieldDelegate> {
    CountryPicker *_countryPicker;
}

@property (weak, nonatomic) IBOutlet UITextView *txtPrivacyPolicy;
@property (weak, nonatomic) IBOutlet UITextField *txtCountry;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;

@end

@implementation MyMobileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self colorTextInTextView];
    [_txtPrivacyPolicy addTapRecognizerWithTarget:self selector:@selector(privacyPolicyClicked:)];
    [self.navigationController addTitle:@"My Mobile #"];
    [self addCountryPicker];
    [self styleTextFields];
    
    UserModel *user = self.appDelegate.user;
    _txtCountry.text = user.country;
    _txtPhone.text = user.phone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)colorTextInTextView
{
    NSString *text = _txtPrivacyPolicy.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:text];
    
    //blue color for terms of use and privacy policy
    UIColor *blueColor = [UIColor colorWithHexString:@"#238dff"];
    NSRange rangePrivacy = [text rangeOfString:strPrivacyPolicy];
    [attributedString addAttribute:NSForegroundColorAttributeName value:blueColor range:rangePrivacy];
    
    //font
    NSRange completeRange = NSMakeRange(0, _txtPrivacyPolicy.text.length);
    [attributedString addAttribute:NSFontAttributeName value:_txtPrivacyPolicy.font range:completeRange];
    _txtPrivacyPolicy.attributedText = attributedString;
}

#pragma mark - text fields

- (void)addCountryPicker
{
    _countryPicker = [[CountryPicker alloc]init];
    _countryPicker.delegate = self;
    _txtCountry.inputView = _countryPicker;
}

- (void)styleTextFields
{
    [_txtCountry addLeftPadding:12];
    [_txtPhone addLeftPadding:12];
    [_txtCountry addToolbarWithTarget:self DoneSelector:@selector(dismissPicker:) nextSelector:@selector(nextClicked:)];
    [_txtPhone addToolbarWithTarget:self DoneSelector:@selector(dismissPicker:)];
}

#pragma mark - country picker delegate

- (void)countryPicker:(CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
{
    _txtCountry.text = name;
}

- (IBAction)dismissPicker:(UIButton*)btnDone
{
    if (btnDone.tag == TEXT_COUNTRY) {
        [self countryPicker:_countryPicker didSelectCountryWithName:_countryPicker.selectedCountryName code:_countryPicker.selectedCountryCode];
    }
    [self.view endEditing:YES];
}

- (IBAction)nextClicked:(id)sender
{
    [_txtPhone becomeFirstResponder];
}

#pragma mark - text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)phoneChanged:(id)sender
{

}

#pragma mark - gesture

- (IBAction)privacyPolicyClicked:(UITapGestureRecognizer*)recognizer
{
    //get view
    UITextView *textView = (UITextView *)recognizer.view;
    //get location
    CGPoint location = [recognizer locationInView:textView];
    UITextPosition *tapPosition = [textView closestPositionToPoint:location];
    NSString *text = _txtPrivacyPolicy.text;
    NSInteger index = [_txtPrivacyPolicy offsetFromPosition:_txtPrivacyPolicy.beginningOfDocument toPosition:tapPosition] - 1;
    
    NSRange rangePrivacy = [text rangeOfString:strPrivacyPolicy];
    if (NSLocationInRange(index, rangePrivacy)) {
//        kAlert(nil, @"privacy policy clicked");
        
        PrivacyPolicyViewController *blockListVc = [[PrivacyPolicyViewController alloc]initWithNibName:@"PrivacyPolicyViewController" bundle:nil];
        blockListVc.screenTitle = @"Privacy Policy";
        blockListVc.htmlFile = @"Privacy Policy";
        [self.navigationController pushViewController:blockListVc animated:YES];
    }
}

#pragma mark - button click

- (IBAction)verifyClicked:(id)sender
{
    NSString *country = _txtCountry.text;
    NSString *phone = _txtPhone.text;
    if (country.length == 0) {
        kAlert(nil, @"Please select a country");
        return;
    }
    if (phone.length == 0) {
        kAlert(nil, @"Please enter phone number");
        return;
    }
    if (![phone isValidForPattern:patternForPhoneNumber]) {
        kAlert(nil, @"Please enter a valid phone number");
        return;
    }
    [self sendDataToServerWithTask:TASK_UPDATE_PHONE];
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@"Updating..."];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_UPDATE_PHONE:
        {
            UserModel *user = self.appDelegate.user;
            NSString *phone = _txtPhone.text;
            [postDict setObject:user.Id forKey:kUserId];
            [postDict setObject:user.username forKey:kUsername];
            [postDict setObject:user.email forKey:kEmail];
            [postDict setObject:phone forKey:kPhone];
            [self.requestManager callServiceWithRequestType:TASK_UPDATE_PHONE method:METHOD_POST params:postDict urlEndPoint:@"users/updateUser"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_UPDATE_PHONE:
            {
                self.appDelegate.user.phone = _txtPhone.text;
                kAlert(nil, message);
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

@end
