//
//  BaseViewController.m
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#define RETRY 0

#import "BaseViewController.h"
#import "MBProgressHUD.h"

@interface BaseViewController ()<STAlertViewDelegate> {
    MBProgressHUD *_loader;
}

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController addLeftBarButtonWithImageNamed:@"back_arrow"];
    
    self.requestManager = [[AFConnectionManager alloc]init];
    self.requestManager.delegate = self;
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - nav

- (IBAction)leftBarButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBarButtonClicked:(id)sender
{
    
}

#pragma mark - loader

- (void)showLoaderWithTitle:(NSString *)title
{
    if (!_loader) {
        _loader = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}

- (void)hideLoader
{
    if (_loader) {
        [_loader removeFromSuperview];
        _loader = nil;
    }
}

#pragma mark - alert

- (void)showServiceFailAlertWithMessage:(NSString *)message tag:(NSInteger)tag
{
    STAlertview *alert = [[STAlertview alloc]initWithTitle:nil message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Retry"]];
    alert.tag = tag;
    [alert show];
}

#pragma mark - alert view delegate

- (void)stAlertView:(STAlertview *)stAlertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == RETRY) {
        [self sendDataToServerWithTask:stAlertView.tag];
    }
}

- (void)stAlertViewDidCancel
{
    
}

#pragma mark - silence warning

- (void)sendDataToServerWithTask:(NSInteger)task
{
    
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    
}

- (void)requestFinishedWithResponse:(id)response
{
    
}

@end
