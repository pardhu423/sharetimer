//
//  ProfileViewController.h
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "BaseViewController.h"

@interface ProfileViewController : BaseViewController

@property (nonatomic) BOOL isMyProfile;
@property (strong, nonatomic) UserModel *user;
@property (nonatomic) NSInteger galleryTask;

@end
