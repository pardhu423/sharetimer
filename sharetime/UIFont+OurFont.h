//
//  UIFont+OurFont.h
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (OurFont)

+ (UIFont*)appFontWithSize:(CGFloat)size;
+ (void)setAppFontForButton:(UIButton*)button;

@end
