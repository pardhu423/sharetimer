//
//  FriendsViewController.m
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

enum {
    SECTION_NEW_FRIENDS,
    SECTION_ACCEPTED_FRIENDS
};

enum {
    ALERT_ACCEPT_FRIEND_REQUEST,
    ALERT_CANCEL_ACCEPT,
    ALERT_BLOCK_FRIEND
};

#import "FriendsViewController.h"
#import "ProfileViewController.h"
#import "AddFriendsViewController.h"

@interface FriendsViewController ()<UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegateFlowLayout, STAlertViewDelegate> {
    NSMutableArray *_friends;
    NSMutableArray *_friendRequests;
    NSArray *_filteredFriends;
    NSArray *_filteredFriendRequests;
    NSInteger _selectedFriendIndex;
    NSIndexPath *_swipedFriendIndexPath;
    NSString *stringStatus;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collFriends;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;

@end

@implementation FriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //text search
    [_txtSearch addLeftImageNamed:@"search"];
    [_txtSearch addRightButtonWithImageName:@"search_close" target:self selector:@selector(cancelSearchClicked:) viewMode:UITextFieldViewModeWhileEditing];
    
    [self.collFriends registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"identifier"];
    [self.collFriends registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    [self styleNavBar];
    [self sendDataToServerWithTask:TASK_GET_FRIEND_LIST];
    
    [self.view addHorizontalSwipeRecognizersWithTarget:self leftSwipeSelector:@selector(leftSwipeDetectedOnMainView:) rightSwipeSelector:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_txtSearch addBottomBorderWithWidth:1 color:[UIColor lightGrayColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - gesture

- (IBAction)leftSwipeDetectedOnMainView:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - nav

- (void)styleNavBar
{
    [self.navigationController addTitle:@"Friends"];
    [self.navigationController addLeftBarButtonWithImageNamed:@"back_arrow"];
    [self.navigationController addRightBarButtonWithImageNamed:@"add_friends"];
}

- (IBAction)leftBarButtonClicked:(id)sender
{
//    NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
//    [dictParam setObject:self.appDelegate.user.Id forKey:@"userId"];
//    [self.requestManager callServiceWithRequestType:TASK_DELETE_FRIENDS method:METHOD_POST params:dictParam urlEndPoint:@"deletefriends"];
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)rightBarButtonClicked:(id)sender
{
    AddFriendsViewController *addFriendsVc = [[AddFriendsViewController alloc]initWithNibName:@"AddFriendsViewController" bundle:nil];
    [self.navigationController pushViewController:addFriendsVc animated:YES];
}

#pragma mark - text field delegate

- (IBAction)cancelSearchClicked:(id)sender
{
    _txtSearch.text = @"";
    [_txtSearch resignFirstResponder];
    [self searchTextChanged:_txtSearch];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)searchTextChanged:(id)sender
{
    NSString *newText = [_txtSearch.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (newText.length) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"SELF.username beginswith[c] %@",
                                  newText];
        _filteredFriends = [_friends filteredArrayUsingPredicate:predicate];
        _filteredFriendRequests = [_friendRequests filteredArrayUsingPredicate:predicate];
    } else {
        _filteredFriends = _friends;
        _filteredFriendRequests = _friendRequests;
    }
    [self.collFriends reloadData];
}

#pragma mark - collection view delegate flow layout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int width = collectionView.frame.size.width/4;
    int height = width + 25;
    return CGSizeMake(width, height);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section == SECTION_NEW_FRIENDS) {
        return CGSizeMake(collectionView.frame.size.width, 50);
    } else {
        return CGSizeZero;
    }
}

#pragma mark - collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (section) {
        case SECTION_NEW_FRIENDS:
            return _filteredFriendRequests.count;
        
        case SECTION_ACCEPTED_FRIENDS:
            return _filteredFriends.count;
    }
    return 0;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"identifier" forIndexPath:indexPath];
    for (UIView *subview in cell.subviews) {
        [subview removeFromSuperview];
    }
    
    int width = collectionView.frame.size.width/4;
    int cellMargin = 10;
    int startY = 5;
    int imgWidth = width - cellMargin;
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(cellMargin, startY, imgWidth, imgWidth)];
    [cell addSubview:img];
    
    UserModel *user;
    
    switch (indexPath.section) {
        case SECTION_NEW_FRIENDS:
        {
            user = _filteredFriendRequests[indexPath.row];
            img.backgroundColor = [UIColor cellGreenColor];
            break;
        }
            
        case SECTION_ACCEPTED_FRIENDS:
        {
            user = _filteredFriends[indexPath.row];
            img.backgroundColor = [UIColor cellBrownColor];
            break;
        }
            
        default:
            break;
    }
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(cellMargin, imgWidth + startY, imgWidth, 20)];
    lbl.adjustsFontSizeToFitWidth = YES;
    lbl.text = user.username;
    [cell addSubview:lbl];
    
    NSString *imgName = genders[user.gender];
    img.image = [UIImage imageNamed:imgName];
    
    UIImageView *imgTickOrCross = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    imgTickOrCross.center = img.center;
    [cell addSubview:imgTickOrCross];
    
    switch (user.confirmStatus) {
        case STATUS_ACCEPTED:
        {
            if (indexPath.section == SECTION_NEW_FRIENDS) {
                imgTickOrCross.image = [UIImage imageNamed:@"right"];
            }
            break;
        }
            
        case STATUS_BLOCKED:
        {
            imgTickOrCross.image = [UIImage imageNamed:@"block"];
            break;
        }
        case STATUS_REJECTED:
        {
            imgTickOrCross.image = [UIImage imageNamed:@"red_cross"];
            break;
        }
            
//        case STATUS_REJECTED:
//        {
//            imgTickOrCross.image = nil;
//            break;
//        }
        default:
            break;
    }
    
    [cell addBottomShadeWithWidth:1 color:[UIColor lightGrayColor]];
    [cell addHorizontalSwipeRecognizersWithTarget:self leftSwipeSelector:@selector(leftSwipeDetected:) rightSwipeSelector:@selector(rightSwipeDetected:)];
    return cell;
}

- (UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
    
    if (kind == UICollectionElementKindSectionHeader && indexPath.section == SECTION_NEW_FRIENDS) {
        
        UILabel *lbl = (UILabel*)[reusableview viewWithTag:1];
        if (!lbl) {
            CGSize size = reusableview.frame.size;
            lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height)];
            NSArray *titles = @[@"  NEW FRIENDS"];
            lbl.text = titles[indexPath.section];
            lbl.textColor = [UIColor cellGreenColor];
            lbl.tag = 1;
            [reusableview addSubview:lbl];
        }
    }
    
    return reusableview;
}

#pragma mark - collection view delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_NEW_FRIENDS) {
        UserModel *user = _filteredFriendRequests[indexPath.row];
        STAlertview *alert = [[STAlertview alloc]initWithTitle:nil message:@"Add to friend list?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Yes"]];
        alert.tag = ALERT_ACCEPT_FRIEND_REQUEST;
        _selectedFriendIndex = indexPath.row;
        _swipedFriendIndexPath = indexPath;
        
        
        [alert show];
        return;
    }
    
    //for accepted freinds go to their profile page
    ProfileViewController *profileVc = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
    profileVc.isMyProfile = NO;
    profileVc.user = _filteredFriends[indexPath.row];
    [self.navigationController pushViewController:profileVc animated:YES];
}

- (IBAction)leftSwipeDetected:(UITapGestureRecognizer*)tap
{
    UICollectionViewCell *cell = (UICollectionViewCell*)tap.view;
    NSIndexPath *path = [_collFriends indexPathForCell:cell];
    
    UserModel *user;
    if (path.section == SECTION_NEW_FRIENDS) {
        user = _filteredFriendRequests[path.row];
    } else {
        user = _filteredFriends[path.row];
    }
    
    _swipedFriendIndexPath = path;
    
    switch (user.confirmStatus) {
        case STATUS_REJECTED:
        {
            if (path.section == SECTION_ACCEPTED_FRIENDS) {
                stringStatus = @"0";
                [self setStatus:STATUS_ACCEPTED forUser:user];
            } else {
                [self setStatus:STATUS_PENDING forUser:user];
            }
            break;
        }
            
//        case STATUS_PENDING:
//            //fall through
//        {
//            stringStatus = @"0";
//            [self setStatus:STATUS_REJECTED forUser:user];
//            break;
//        }
            
        case STATUS_ACCEPTED:
        {
            stringStatus = @"1";
            [self setStatus:STATUS_REJECTED forUser:user];
            break;
        }
    }
}

- (IBAction)rightSwipeDetected:(UITapGestureRecognizer*)tap
{
    UICollectionViewCell *cell = (UICollectionViewCell*)tap.view;
    NSIndexPath *path = [_collFriends indexPathForCell:cell];
    
    UserModel *user;
    if (path.section == SECTION_NEW_FRIENDS) {
        user = _filteredFriendRequests[path.row];
    } else {
        user = _filteredFriends[path.row];
    }
    
    _swipedFriendIndexPath = path;
    
    switch (user.confirmStatus) {
        case STATUS_BLOCKED:
        {
            if (path.section == SECTION_ACCEPTED_FRIENDS) {
                [self setStatus:STATUS_ACCEPTED forUser:user];
            } else {
                [self setStatus:STATUS_PENDING forUser:user];
            }
            break;
        }
            
        case STATUS_PENDING:
            //fall through
            
        case STATUS_ACCEPTED:
        {
            [self setStatus:STATUS_BLOCKED forUser:user];
            break;
        }
    }
}

#pragma mark - table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 27;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"id"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"id"];
    } else {
        for (UIView *subview in cell.subviews) {
            [subview removeFromSuperview];
        }
    }
    char c = 96 + indexPath.row;
    if (c == 96) {
        c = '#';
    }
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    lbl.text = [NSString stringWithFormat:@"%c", c];
    lbl.textAlignment = NSTextAlignmentCenter;
    [cell addSubview:lbl];
    return cell;
}

#pragma mark - table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@""];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_GET_FRIEND_LIST:
        {
            NSString *userId = self.appDelegate.user.Id;
            [postDict setObject:userId forKey:kUserId];
            
            [self.requestManager callServiceWithRequestType:TASK_GET_FRIEND_LIST method:METHOD_POST params:postDict urlEndPoint:@"friendsnpeople"];
            break;
        }
            
        case TASK_ACCEPT_FRIEND_REQUEST:
        {
            UserModel *user = _friendRequests[_selectedFriendIndex];
            [postDict setObject:user.Id forKey:kFriendId];
            [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
            //accepted friend will have status 1
            [postDict setObject:@"1" forKey:@"action"];
            [self.requestManager callServiceWithRequestType:TASK_ACCEPT_FRIEND_REQUEST method:METHOD_POST params:postDict urlEndPoint:@"responseOnRequest"];
            break;
        }
            
        default:
            break;
    }
}

- (void)setStatus:(int)status forUser:(UserModel*)user
{

    if (status == 2 || ([stringStatus isEqual:@"0"] && status == 1)) {
        //STATUS CAN BE pending, accepted, rejected or blocked
        [self showLoaderWithTitle:@""];
        //action should be 2 always for this condition
        NSString *str = @"2";
        NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
        [postDict setObject:user.Id forKey:kFriendId];
        [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
        [postDict setObject:[NSString stringWithFormat:@"%@", str] forKey:@"action"];
//        [postDict setObject:[NSString stringWithFormat:@"%@", stringStatus] forKey:@"makedelete"];
        [self.requestManager callServiceWithRequestType:TASK_CHANGE_FRIEND_STATUS method:METHOD_POST params:postDict urlEndPoint:@"responseOnRequest"];
    } else {
        //STATUS CAN BE pending, accepted, rejected or blocked
        [self showLoaderWithTitle:@""];
        NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
        
//        UserModel *user = _friendRequests[_selectedFriendIndex];
        [postDict setObject:user.Id forKey:kFriendId];
        [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
        [postDict setObject:[NSString stringWithFormat:@"%d", status] forKey:@"action"];
        [self.requestManager callServiceWithRequestType:TASK_CHANGE_FRIEND_STATUS method:METHOD_POST params:postDict urlEndPoint:@"responseOnRequest"];
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    stringStatus = @"";
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_GET_FRIEND_LIST:
            {
                _friendRequests = [[NSMutableArray alloc]init];
                _friends = [[NSMutableArray alloc]init];
                NSDictionary *friendsDict = [responseDict objectForKey:@"friends"];

                NSArray *friends = [friendsDict objectForKey:@"friends"];
                for (NSDictionary *userDict in friends) {
                    UserModel *user = [[UserModel alloc]initWithResponse:userDict];
                    [_friends addObject:user];
                }
                
                NSArray *friendReuqests = [friendsDict objectForKey:@"pending"];
                for (NSDictionary *userDict in friendReuqests) {
                    UserModel *user = [[UserModel alloc]initWithResponse:userDict];
                    [_friendRequests addObject:user];
                }
                _filteredFriendRequests = _friendRequests;
                _filteredFriends = _friends;
                [self.collFriends reloadData];
                break;
            }
                
            case TASK_ACCEPT_FRIEND_REQUEST:
            {
                UserModel *user = _filteredFriendRequests[_selectedFriendIndex];
                user.confirmStatus = STATUS_ACCEPTED;
                [self.collFriends reloadData];
                kAlert(nil, message);
                break;
            }
                
            case TASK_CHANGE_FRIEND_STATUS:
            {
                UserModel *user;
                if (_swipedFriendIndexPath.section == SECTION_NEW_FRIENDS) {
                    user = _filteredFriendRequests[_swipedFriendIndexPath.row];
                } else {
                    user = _filteredFriends[_swipedFriendIndexPath.row];
                }
                NSDictionary *responceDict = [responseDict objectForKey:@"response"];
                user.confirmStatus = [[responceDict objectForKey:@"action"] integerValue];
                [self.collFriends reloadData];
                kAlert(nil, message);
                break;
            }
            case TASK_DELETE_FRIENDS:{
                [self.navigationController popViewControllerAnimated:YES];
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    if (tag == TASK_ADD_FRIEND) {
        kAlert(nil, error.localizedDescription);
        return;
    }
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

#pragma mark - alert view delegate

- (void)stAlertView:(STAlertview *)stAlertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (stAlertView.tag == ALERT_ACCEPT_FRIEND_REQUEST) {
        UserModel *user = _filteredFriendRequests[_selectedFriendIndex];
        if (user.confirmStatus == STATUS_PENDING) {
            [self setStatus:STATUS_ACCEPTED forUser:user];
        }
    }
}

- (void)stAlertViewDidCancel:(STAlertview *)stAlertView
{
    if (stAlertView.tag == ALERT_ACCEPT_FRIEND_REQUEST) {
        UserModel *user = _filteredFriendRequests[_selectedFriendIndex];
        if (user.confirmStatus == STATUS_ACCEPTED) {
            [self setStatus:STATUS_PENDING forUser:user];
        }
    }
}

@end
