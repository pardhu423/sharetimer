//
//  EmailViewController.m
//  sharetime
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "EmailViewController.h"

@interface EmailViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

@end

@implementation EmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_txtEmail addLeftPadding:12];
    _txtEmail.text = self.appDelegate.user.email;
    [self.navigationController addTitle:@"My Email"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - button click

- (IBAction)saveClicked:(id)sender
{
    NSString *email = _txtEmail.text;
    if (email.length == 0) {
        kAlert(nil, @"Please enter your email");
        return;
    }
    if (![email isValidForPattern:patternForEmail]) {
        kAlert(nil, @"Please enter a valid email");
        return;
    }
    [self sendDataToServerWithTask:TASK_UPDATE_EMAIL];
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@"Updating..."];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_UPDATE_EMAIL:
        {
            UserModel *user = self.appDelegate.user;
            NSString *email = _txtEmail.text;
            [postDict setObject:user.Id forKey:kUserId];
            [postDict setObject:user.username forKey:kUsername];
            [postDict setObject:email forKey:kEmail];
            [postDict setObject:user.phone forKey:kPhone];
            [self.requestManager callServiceWithRequestType:TASK_UPDATE_EMAIL method:METHOD_POST params:postDict urlEndPoint:@"users/updateUser"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_UPDATE_EMAIL:
            {
                self.appDelegate.user.email = _txtEmail.text;
                kAlert(nil, message);
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}


@end
