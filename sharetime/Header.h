//
//  Header.h
//  sharetime
//
//  Created by iOS Developer on 23/09/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#ifndef Header_h
#define Header_h

#define screenSize [[UIScreen mainScreen]bounds].size
#define heightSmallScreen 480
#define heightIphone5 568
#define heightIphone6 667
#define kAlert(title,msg) [[[STAlertview alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:nil otherButtonTitles:@[@"OK"]] show]

#import "UINavigationController+Utility.h"
#import "UIFont+OurFont.h"
#import "UITextField+Utility.h"
#import "UIColor+Addition.h"
#import "NSDate+Conversions.h"
#import "UIImage+ImageManipulations.h"
#import "UIView+ViewManipulations.h"
#import "NSString+Validations.h"
#import "NSDate+Conversions.h"
#import "UICollectionViewCell+Borders.h"

#import "STAlertview.h"

//user defaults
#define kDeviceToken @"device_token"
#define kDeviceUuid @"device_uuid"
#define kScreenshotDisplayDisabled @"screenshot_display_disabled"
#define kFiltersOff @"filters_off"
#define kMute @"mute"
#define kBackFacingFlash @"back_facing_flash"
#define kNoLoop @"no_loop"

//push
#define kDeviceId @"device_id"
#define kPushId @"push_id"
#define kDeviceName @"device_name"

#endif /* Header_h */
