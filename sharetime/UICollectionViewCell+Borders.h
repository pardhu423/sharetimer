//
//  UICollectionViewCell+Borders.h
//  sharetime
//
//  Created by iOS Developer on 03/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionViewCell (Borders)

- (void)addBottomShadeWithWidth:(CGFloat)width color:(UIColor*)color;

@end
