//
//  BaseModel.h
//  Paytm
//
//  Created by iOS Developer on 22/07/15.
//  Copyright (c) 2016 Henote Technologies. All rights reserved.
//

#define kId @"id"
#define kMessage @"message"
#define kStatus @"status"

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject

@property (strong, nonatomic) NSString *Id;

- (id)initWithResponse:(NSDictionary*)response;
- (NSString*)checkNil:(NSString*)string;

@end
