//
//  SettingsViewController.m
//  sharetime
//
//  Created by iOS Developer on 06/10/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

enum {
    SECTION_SETTINGS,
    SECTION_SUPPORT,
    SECTION_MANAGE,
    SECTION_CAN_MY_FRIEND,
    SECTION_MORE_INFORMATION,
    SECTION_CLEAR_MAILBOX,
    SECTION_LOGOUT
};

enum {
    ROW_USERNAME,
    ROW_CHANGE_USERNAME,
    ROW_EMAIL,
    ROW_MOBILE_NUMBER,
    ROW_NOTIFICATION_SOUND
};

enum {
    ROW_REPORT_PROBLEM,
    ROW_CHECK_SHARETIMER_BLOG,
    ROW_ABOUT_THIS_VERSION
};

enum {
    ROW_BLOCK_LIST,
    ROW_PRIVACY_POLICY,
    ROW_TERMS_OF_USE
};

#import "SettingsViewController.h"
#import "SettingsTableViewCell.h"
#import "MyMobileViewController.h"
#import "EmailViewController.h"
#import "ManageViewController.h"
#import "ChangeUsernameViewController.h"
#import "SupportViewController.h"
#import "BlockListViewController.h"
#import "LoginViewController.h"
#import "VersionViewController.h"
#import "CanMyFriendViewController.h"
#import "PrivacyPolicyViewController.h"

@interface SettingsViewController ()<UITableViewDelegate, UITableViewDataSource, STAlertViewDelegate> {
    NSArray *_titles;
}

@property (weak, nonatomic) IBOutlet UITableView *tblSettings;
@property (weak, nonatomic) IBOutlet UILabel *labelVersion;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _labelVersion.text = @"";
    [self.navigationController addTitle:@"Settings"];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithHexString:@"#3a4347"]];
    [[UINavigationBar appearance] setTranslucent:NO];

    [self refreshTable];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshTable
{
    UserModel *user = self.appDelegate.user;
    NSString *email = [NSString stringWithFormat:@"Email: %@", user.email];
    NSString *mobile = [NSString stringWithFormat:@"Mobile Number: %@", user.phone];
    NSArray *settings = @[user.username, @"Change your username here", email, mobile, @"Notification Sounds:"];
    NSArray *supports = @[@"Report a Problem", @"Check Sharetimer Blog", @"About this Version"];
    NSArray *manage = @[@"Additional Services"];
    NSArray *canMyFriend = @[@"Screenshot My Letters?"];
    NSArray *moreInfo = @[@"Block List", @"Privacy Policy", @"Terms of Use"];
    _titles = @[settings, supports, manage, canMyFriend, moreInfo, @[], @[]];
    [self.tblSettings reloadData];
}

#pragma mark - table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _titles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *list = _titles[section];
    return list.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"SettingsTableViewCell";
    if (indexPath.section == SECTION_SETTINGS) {
        if (indexPath.row == ROW_USERNAME) {
            identifier = @"SettingsTextCell";
        }
        if (indexPath.row == ROW_NOTIFICATION_SOUND) {
            identifier = @"SettingsSwitchCell";
        }
    }
    SettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:identifier owner:self options:nil] lastObject];
    }
    
    NSArray *sectionTitles = _titles[indexPath.section];
    NSString *title = sectionTitles[indexPath.row];
    cell.lbl.text = title;
    
    //grey some text
    if (indexPath.section == SECTION_SETTINGS) {
        NSMutableAttributedString *coloredText = [[NSMutableAttributedString alloc]initWithString:title];
        NSRange greyRange;
        if (indexPath.row == ROW_EMAIL) {
            NSInteger emailLength = @"Email:".length;
            greyRange = NSMakeRange(emailLength, title.length - emailLength);
        }
        if (indexPath.row == ROW_MOBILE_NUMBER) {
            NSInteger mobileLength = @"mobile number:".length;
            greyRange = NSMakeRange(mobileLength, title.length - mobileLength);
        }
        if (indexPath.row == ROW_EMAIL || indexPath.row == ROW_MOBILE_NUMBER) {
            [coloredText addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:greyRange];
            cell.lbl.attributedText = coloredText;
        }
    }
    
    return cell;
}

#pragma mark - table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case SECTION_SETTINGS:
        {
            switch (indexPath.row) {
                case ROW_CHANGE_USERNAME:
                {
                    ChangeUsernameViewController *usernameVc = [[ChangeUsernameViewController alloc]initWithNibName:@"ChangeUsernameViewcontroller" bundle:nil];
                    [self.navigationController pushViewController:usernameVc animated:YES];
                    break;
                }
                    
                case ROW_EMAIL:
                {
                    EmailViewController *emailVc = [[EmailViewController alloc]initWithNibName:@"EmailViewController" bundle:nil];
                    [self.navigationController pushViewController:emailVc animated:YES];
                    break;
                }
                    
                case ROW_MOBILE_NUMBER:
                {
                    MyMobileViewController *mobileVc = [[MyMobileViewController alloc]initWithNibName:@"MyMobileViewController" bundle:nil];
                    [self.navigationController pushViewController:mobileVc animated:YES];
                    break;
                }
                    
                default:
                    break;
            }
            break;
        }
            
        case SECTION_SUPPORT:
        {
            switch (indexPath.row) {
                case ROW_REPORT_PROBLEM:
                {
                    SupportViewController *supportVc = [[SupportViewController alloc]initWithNibName:@"SupportViewController" bundle:nil];
                    [self.navigationController pushViewController:supportVc animated:YES];
                    break;
                }
                    
                case ROW_ABOUT_THIS_VERSION:
                {
                    VersionViewController *versionVc = [[VersionViewController alloc]initWithNibName:@"VersionViewController" bundle:nil];
                    [self.navigationController pushViewController:versionVc animated:YES];
                    break;
                }
                    
                default:
                    break;
            }
            
            break;
        }
            
        case SECTION_MANAGE:
        {
            ManageViewController *manageVc = [[ManageViewController alloc]initWithNibName:@"ManageViewController" bundle:nil];
            [self.navigationController pushViewController:manageVc animated:YES];
            break;
        }
            
        case SECTION_CAN_MY_FRIEND:
        {
            CanMyFriendViewController *canMyFriendVc = [[CanMyFriendViewController alloc]initWithNibName:@"CanMyFriendViewController" bundle:nil];
            [self.navigationController pushViewController:canMyFriendVc animated:YES];
            break;
        }
            
        case SECTION_MORE_INFORMATION:
        {
            switch (indexPath.row) {
                case ROW_BLOCK_LIST:
                {
                    BlockListViewController *blockListVc = [[BlockListViewController alloc]initWithNibName:@"BlockListViewController" bundle:nil];
                    [self.navigationController pushViewController:blockListVc animated:YES];
                    break;
                }
                    
                case ROW_PRIVACY_POLICY:
                {
                    PrivacyPolicyViewController *blockListVc = [[PrivacyPolicyViewController alloc]initWithNibName:@"PrivacyPolicyViewController" bundle:nil];
                    blockListVc.screenTitle = @"Privacy Policy";
                    blockListVc.htmlFile = @"Privacy Policy";
                    [self.navigationController pushViewController:blockListVc animated:YES];
                    break;
                }
                    
                case ROW_TERMS_OF_USE:
                {
                    PrivacyPolicyViewController *blockListVc = [[PrivacyPolicyViewController alloc]initWithNibName:@"PrivacyPolicyViewController" bundle:nil];
                    blockListVc.screenTitle = @"Terms and conditions";
                    blockListVc.htmlFile = @"Terms and conds";
                    [self.navigationController pushViewController:blockListVc animated:YES];
                    break;
                }
                    
                default:
                    break;
            }
        }
            
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == SECTION_SETTINGS ? 1 : 44;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, screenSize.width, 44)];
    lbl.backgroundColor = [UIColor colorWithHexString:@"#3a4347"];
    NSArray *headers = @[@"", @"SUPPORT", @"MANAGE", @"Can My Friends...", @"MORE INFORMATION", @"Clear Mailbox", @"Log Out"];
    NSString *header = headers[section];
    lbl.text = header;
    
    if (section >= headers.count - 2) {
        lbl.textColor = [UIColor navigationBlueColor];
    } else {
        lbl.textColor = [UIColor whiteColor];
    }
    
    lbl.font = [UIFont appFontWithSize:34];
    lbl.textAlignment = NSTextAlignmentCenter;
    
    lbl.tag = section;
    [lbl addTapRecognizerWithTarget:self selector:@selector(headerTapped:)];
    return lbl;
}

#pragma mark - gesture

- (IBAction)headerTapped:(UITapGestureRecognizer*)tap
{
    UIView *headerView = tap.view;
    if (headerView.tag >= SECTION_CLEAR_MAILBOX) {
        STAlertview *alert = [[STAlertview alloc]initWithTitle:nil message:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Yes"]];
        alert.tag = headerView.tag;
        [alert show];
    }
}

#pragma mark - alert delegate

- (void)stAlertView:(STAlertview *)stAlertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (stAlertView.tag) {
        case SECTION_CLEAR_MAILBOX:
        {
            [self sendDataToServerWithTask:TASK_CLEAR_MAILBOX];
            break;
        }
            
        case SECTION_LOGOUT:
        {
            //self.appDelegate.user = nil;
            NSArray *viewControllers = self.navigationController.viewControllers;
            UIViewController *vc = viewControllers[1];
            if ([vc isKindOfClass:[LoginViewController class]]) {
                [self.navigationController popToViewController:vc animated:YES];
            } else {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            break;
        }
        default:
            break;
    }
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@""];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_CLEAR_MAILBOX:
        {
            [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
            [self.requestManager callServiceWithRequestType:TASK_CLEAR_MAILBOX method:METHOD_POST params:postDict urlEndPoint:@"clearMailbox"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_CLEAR_MAILBOX:
            {
                kAlert(nil, message);
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    if (tag == TASK_ADD_FRIEND) {
        kAlert(nil, error.localizedDescription);
        return;
    }
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}


@end
