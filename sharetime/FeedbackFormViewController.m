//
//  FeedbackFormViewController.m
//  sharetime
//
//  Created by iOS Developer on 11/12/15.
//  Copyright © 2016 Henote Technologies. All rights reserved.
//

#import "FeedbackFormViewController.h"

@interface FeedbackFormViewController ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *txtSubject;
@property (weak, nonatomic) IBOutlet UITextView *txtComments;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginTop;

@end

@implementation FeedbackFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController addTitle:@"Support"];
    UserModel *user = self.appDelegate.user;
    _lblUsername.text = user.username;
    _lblEmail.text = user.email;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - text field delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    CGFloat topMargin = 0;
    int height = screenSize.height;
    switch (height) {
        case heightSmallScreen:
            topMargin = textView == _txtComments ? -250 : -180;
            break;
            
        case heightIphone5:
            topMargin = textView == _txtComments ? -170 : -100;
            break;
        
        case heightIphone6:
            topMargin = textView == _txtComments ? -50 : 0;
            break;
        
        default:
            break;
    }
    [self setTopMargin:topMargin];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self setTopMargin:0];
}

- (void)setTopMargin:(CGFloat)topMargin
{
    _marginTop.constant = topMargin;
    [UIView animateWithDuration:2
                     animations:^{
                         [self.view setNeedsLayout];
                     }];
}

#pragma mark - button click

- (IBAction)sendClicked:(id)sender
{
    NSString *subject = [_txtSubject.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *comments = [_txtComments.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (!subject.length) {
        kAlert(nil, @"Please enter a subject");
        return;
    }
    if (!comments.length) {
        kAlert(nil, @"Please enter some more information");
        return;
    }
    [self sendDataToServerWithTask:TASK_FEEDBACK];
}

#pragma mark - send data to server

- (void)sendDataToServerWithTask:(NSInteger)task
{
    [self showLoaderWithTitle:@""];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    switch (task) {
        case TASK_FEEDBACK:
        {
            [postDict setObject:self.appDelegate.user.Id forKey:kUserId];
            
            NSString *title = _txtSubject.text;
            NSString *feedback = _txtComments.text;
            [postDict setObject:title forKey:@"title"];
            [postDict setValue:feedback forKey:@"feedback"];
            [self.requestManager callServiceWithRequestType:TASK_FEEDBACK method:METHOD_POST params:postDict urlEndPoint:@"savefeedback"];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - connection manager delegate

- (void)requestFinishedWithResponse:(id)response
{
    [self hideLoader];
    
    NSInteger requestType = [[response objectForKey:kRequestType]integerValue];
    NSDictionary *responseDict = [response objectForKey:kResponseObject];
    BOOL success = [[responseDict objectForKey:kStatus]boolValue];
    NSString *message = [responseDict objectForKey:kMessage];
    
    if (success) {
        switch (requestType) {
            case TASK_FEEDBACK:
            {
                kAlert(nil, message);
                break;
            }
                
            default:
                break;
        }
    } else {
        kAlert(nil, message);
    }
}

- (void)requestFailedWithError:(NSMutableDictionary *)errorDict
{
    [self hideLoader];
    NSError *error = [errorDict objectForKey:kError];
    NSInteger tag = [[errorDict objectForKey:kRequestType]integerValue];
    [self showServiceFailAlertWithMessage:error.localizedDescription tag:tag];
}

@end
